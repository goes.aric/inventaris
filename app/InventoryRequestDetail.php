<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryRequestDetail extends Model
{
    use Notifiable;

    protected $fillable = [
    	'inventory_request_id', 'inventory_id', 'item_code', 'item_name', 'qty', 'unit',
    ];

    protected $table = 'inventory_request_detail';

    public function header() {
    	return $this->belongsTo('App\InventoryRequest', 'inventory_request_id');
    }

    public function inventory() {
        return $this->belongsTo('App\Inventory', 'inventory_id');
    }
}
