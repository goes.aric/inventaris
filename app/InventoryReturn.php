<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryReturn extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'return_number', 'return_date', 'inventory_delivery_id', 'department_id', 'employee_id', 'total_item', 'status', 'note', 'user_id',
    ];

    protected $table = 'inventory_return';

    public function department() {
    	return $this->belongsTo('App\Department', 'department_id');
    }

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id');
    }    

    public function delivery() {
        return $this->belongsTo('App\InventoryDelivery', 'inventory_delivery_id');
    }

    public function detail() {
        return $this->hasMany('App\InventoryReturnDetail');
    }    

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
