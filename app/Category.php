<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'category_name', 'description', 'status', 'user_id',
    ];

    protected $table = 'category';

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
