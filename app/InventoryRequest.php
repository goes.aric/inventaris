<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryRequest extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'request_number', 'request_date', 'department_id', 'requested_by', 'reason', 'description', 'request_file', 'approved_by', 'approved_date', 'total_item', 'user_id',
    ];

    protected $table = 'inventory_request';

    public function department() {
    	return $this->belongsTo('App\Department', 'department_id');
    }

    public function detail() {
        return $this->hasMany('App\InventoryRequestDetail');
    }    

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
