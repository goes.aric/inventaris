<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceWork extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'maintenance_record_id', 'work_name', 'work_description', 'technician', 'price',
    ];

    protected $table = 'maintenance_works';

    public function maintenance() {
    	return $this->belongsTo('App\MaintenanceRecord', 'maintenance_record_id');
    }
}
