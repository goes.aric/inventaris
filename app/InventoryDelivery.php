<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryDelivery extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'delivery_number', 'delivery_date', 'inventory_request_id', 'department_id', 'employee_id', 'total_item', 'status', 'note', 'user_id',
    ];

    protected $table = 'inventory_delivery';

    public function department() {
    	return $this->belongsTo('App\Department', 'department_id');
    }

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id');
    }    

    public function request() {
        return $this->belongsTo('App\InventoryRequest', 'inventory_request_id');
    }

    public function detail() {
        return $this->hasMany('App\InventoryDeliveryDetail');
    }    

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
