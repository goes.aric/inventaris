<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryProcurementSubDetail extends Model
{
    use Notifiable;

    protected $fillable = [
    	'inv_procurement_detail_id', 'inventory_detail_id', 'item_code', 'detail_code', 'item_name', 'identification_number', 'decree_number', 'request_number', 'origin', 'acquisition_year', 'condition', 'price', 'information',
    ];

    protected $table = 'inv_procurement_sub_detail';

    public function detail() {
    	return $this->belongsTo('App\InventoryProcurementDetail', 'inv_procurement_detail_id');
    }

    public function inv_detail() {
        return $this->belongsTo('App\InventoryDetail', 'inventory_detail_id');
    }
}
