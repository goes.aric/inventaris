<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UsersGroupItems extends Model
{
    protected $fillable = [
        'privilege_item_name', 'key', 'group_key', 'users_group_id',
    ];

    protected $table = 'privilege_items';

    public function users_group() {
        return $this->belongsTo('App\UsersGroup', 'users_group_id');
    }
}
