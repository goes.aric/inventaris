<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'code', 'department_name', 'description', 'status', 'user_id',
    ];

    protected $table = 'department';

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
