<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'employee_number', 'employee_name', 'payroll_number', 'department_id', 'group', 'position', 'salary', 'employee_status', 'level_of_education', 'majoring_in_education', 'other', 'description', 'is_active', 'user_id',
    ];

    protected $table = 'employee';

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function department() {
    	return $this->belongsTo('App\Department', 'department_id');
    }    
}
