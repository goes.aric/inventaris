<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersGroup extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'group_name', 'group_description', 'user_id',
    ];

    protected $table = 'users_group';

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
