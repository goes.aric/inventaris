<?php

namespace App\Http\Controllers\InventoryRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Datatables;
use Validator;
use App\User;
use App\Department;
use App\Inventory;
use App\InventoryRequest;
use App\InventoryRequestDetail;
use App\StockIn;
use App\StockOut;
use Carbon\Carbon;
use Auth;
use DB;

class InventoryRequestController extends Controller
{
    /**
     * Populate inventory_request records
     */
    public function populateRecords(Request $req)
    {
        if ($req->request_start <> '') {
            $request = InventoryRequest::join('department', 'inventory_request.department_id', '=', 'department.id')->select(['inventory_request.*', 'department.department_name'])->whereBetween('inventory_request.request_date', [$req->request_start, $req->request_end])->where('inventory_request.department_id', 'LIKE', $req->filter_department)->where('inventory_request.status', 'LIKE', $req->filter_status)->orderBy('inventory_request.created_at', 'DESC');
        }else {
            $request = InventoryRequest::join('department', 'inventory_request.department_id', '=', 'department.id')->select(['inventory_request.*', 'department.department_name'])->where('inventory_request.department_id', 'LIKE', $req->filter_department)->where('inventory_request.status', 'LIKE', $req->filter_status)->orderBy('inventory_request.created_at', 'DESC'); 
        }

        return Datatables::of($request)
            ->addColumn('action', function($request) {
                if ($request->status == '1') {
                    return '<a href="'.route('request.show', $request->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                            <i class="icon-eye"></i>
                        </a>
                        <a href="'.route('request.edit', $request->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                            <i class="icon-note"></i>
                        </a>                  
                        <button data-remote="' . $request->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                            <i class="icon-trash"></i>
                        </button>';
                } else {
                    return '<a href="'.route('request.show', $request->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                            <i class="icon-eye"></i>
                        </a>';
                }
            })
            ->addColumn('checkbox_column', function($request) {
                return '<input type="checkbox" data-id="'.$request->id.'" name="data[]" value="'.$request->id.'" id="data['.$request->id.']">';
            })
            ->editColumn('reason', function($request) {
                $reason = "";
                if ($request->reason == '1') {
                    $reason = "<span class='badge badge-primary'>".__('Low on Item')."</span>";
                } elseif ($request->reason == '2') {
                    $reason = "<span class='badge badge-primary'>".__('Out of Stock')."</span>";
                } elseif ($request->reason == '3') {
                    $reason = "<span class='badge badge-primary'>".__('Need/Purpose for this Item')."</span>";                
                }
                
                return $reason;
            })            
            ->editColumn('status', function($request) {
                $status = "";
                if ($request->status == '1') {
                    $status = "<span class='badge badge-warning'>".__('Ordered')."</span>";
                } elseif ($request->status == '2') {
                    $status = "<span class='badge badge-info'>".__('Approved')."</span>";
                } elseif ($request->status == '3') {
                    $status = "<span class='badge badge-success'>".__('Delivered')."</span>";
                } elseif ($request->status == '4') {
                    $status = "<span class='badge badge-danger'>".__('Denied')."</span>";                  
                }
                
                return $status;
            })
            ->addColumn('details_url', function($items) {
                return route("request.getInventoryRequestDetailData", $items->id);
            })                                                         
            ->make(true);
    }

    /**
     * Populate inventory request detail records
     */
    public function populateDetailRecords($id)
    {
        $items = InventoryRequestDetail::where('inventory_request_id', '=', $id);

        return Datatables::of($items)
            ->addColumn('action', function($items) {
                if ($items->header->status == 2 || $items->header->status == 3 || $items->header->status == 4) {
                    return '-';
                } elseif ($items->header->status == 1) {
                    return '
                        <button data-remote="' . $items->id . '" class="btn btn-sm btn-danger btn-delete" alt="Delete" title="Delete">
                            <i class="icon-trash"></i>
                        </button>';                    
                }
            })                                 
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::where('status', '=', 1)->get();

        return view('pages.inv_request.index', compact('department'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::where('status', '=', 1)->get();

        return view('pages.inv_request.create', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'request_date'      => 'required',
            'department'        => 'required',
            'requested_by'      => 'required',
            'reason'            => 'required',
            'description'       => 'string|nullable',
            'request_file'      => 'required|file|mimes:pdf|max:3072'
        ]);      

        //Auto Generate Request Number
        $kode   = Carbon::now()->format('Ym');
        $query  = DB::select('SELECT ifnull(right(max(request_number), 6), 0) AS maxID FROM inventory_request WHERE RIGHT(LEFT(request_number, 8), 6) = :id', ['id' => $kode]);

        foreach ($query as $id) {
            $id     = (int)$id->maxID + 1;
        }
        $newID  = "IR".$kode.'-'.substr("0000000$id", -6);

        //Jika input file tidak kosong, upload file
        $new_name = "";
        if (!empty($request->file('request_file'))) {
            //Variabel file
            $file = $request->file('request_file');
            $new_name = $newID . '-' .$file->getClientOriginalName();

            if (Storage::exists($new_name)) {
                $new_name = $newID . '-' . rand() .$file->getClientOriginalName();
            }

            //Upload file              
            $path = storage_path('app/public/files/');
            $file->move($path, $new_name);      
        }        

        $create = new InventoryRequest;
        $create->request_number     = $newID;
        $create->request_date       = $request->request_date;
        $create->department_id      = $request->department;
        $create->requested_by       = $request->requested_by;
        $create->reason             = $request->reason;
        $create->description        = $request->description;
        $create->request_file       = $new_name;
        $create->status             = 1;
        $create->user_id            = $user_id;

        if ($create->save()) {
            $notification = [
                'msg'           => __('Record has been saved'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('request.edit', $create->id)->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to save record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('request.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::where('status', '=', 1)->get();
        $request = InventoryRequest::findOrFail($id);

        return view('pages.inv_request.show', compact('department', 'request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::where('status', '=', 1)->get();
        $request = InventoryRequest::findOrFail($id);

        return view('pages.inv_request.edit', compact('department', 'request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'request_date'      => 'required',
            'department'        => 'required',
            'requested_by'      => 'required',
            'reason'            => 'required',
            'description'       => 'string|nullable',
            'status'            => 'required',
            'approved_by'       => 'required_if:status,2',
            'approved_date'     => 'required_if:status,2',                               
        ]);

        if (!empty($request->file('request_file'))) {
            //Cek default di field
            if ($update->request_file != null) {
                //Hapus file sebelumnya
                $path = storage_path('app/public/files/').$update->request_file;
                if(file_exists($path)){
                    \File::delete($path);
                }
            } 

            //Variabel gambar profil baru
            $file = $request->file('request_file');
            $new_name = $update->order_no . '-' .$file->getClientOriginalName();

            if (Storage::exists($new_name)) {
                $new_name = $update->order_no . '-' . rand() .$file->getClientOriginalName();
            }            

            //Upload gambar profil baru              
            $path = storage_path('app/public/files/');
            $file->move($path, $new_name);

            //Update value field
            $update->request_file = $new_name;
        }              

        $update = InventoryRequest::findOrFail($id);
        $update->request_date       = $request->request_date;
        $update->department_id      = $request->department;
        $update->requested_by       = $request->requested_by;
        $update->reason             = $request->reason;
        $update->description        = $request->description;
        $update->status             = $request->status;
        if ($request->status == '2') {
            $update->approved_by    = $request->approved_by;
            $update->approved_date  = $request->approved_date;
        }
        $update->user_id            = $user_id;

        if ($update->update()) {
            $notification = [
                'msg'           => __('Record has been updated'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('request.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to update record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('request.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = InventoryRequest::find($id);

        try {
            if ($delete->status == '1') {
                if ($delete->delete()) {
                    return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
                }

                return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]); 
            } else {
                return response()->json(['type' => 'success', 'msg' => __('Cannot delete record, except the status is ordered')]);
            }           
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = InventoryRequest::where('status', '=', '1')->whereIn('id', $ids);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }           
    }

    // Inventory Request Detail //
    public function populateItem(Request $request)
    {
        $item = Inventory::whereNotIn('id', InventoryRequestDetail::select('inventory_id')->where('inventory_request_id', '=', $request->id))->get();

        $response = [
            'msg'           => 'List of Items',
            'item'          => $item
        ];

        return response()->json($response, 200);         
    }

    public function getDetailItem($id)
    {
        $item = Inventory::where('id', $id)->first();
        
        $response = [
            'item_code'         => $item->item_code,
            'item_name'         => $item->item_name,
            'unit'              => $item->unit
        ];

        return response()->json($response, 200);         
    }    

    public function storeRequestDetail(Request $request)
    {
        $item = Inventory::findOrFail($request->item_id);

        $rules = array(
            'request_id'    => 'required',
            'item_id'       => 'required',
            'qty'           => 'required|numeric|min:1'                 
        ); 
    
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json(['type' => 'error', 'msg' => $validator->errors()->all()]);
        }else{
            $now = Carbon::now();

            $create = new InventoryRequestDetail;
            $create->inventory_request_id   = $request->request_id;
            $create->inventory_id           = $item->id;
            $create->item_code              = $item->item_code;
            $create->item_name              = $item->item_name;
            $create->qty                    = $request->qty;
            $create->unit                   = $item->unit;
            $create->created_at             = $now;
            $create->updated_at             = $now;            

            if ($create->save()) {
                //Update Total
                $total = InventoryRequestDetail::where('inventory_request_id', '=', $request->request_id)->sum('qty');
                $update                = InventoryRequest::findOrFail($request->request_id);
                $update->total_item    = $total;

                if ($update->update()) {
                    return response()->json(['type' => 'success', 'msg' => __('Record has been saved'), 'value' => $total]);
                }

                return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'value' => '0']);
            }
        
            return response()->json(['type' => 'error', 'msg' => __('Failed to insert record')]);
        }
    }

    public function requestDetailDestroy($id)
    {
        $delete     = InventoryRequestDetail::find($id);
        $request_id = $delete->inventory_request_id; 

        if ($delete->delete()) {
            //Update Total
            $total = InventoryRequestDetail::where('inventory_request_id', '=', $request_id)->sum('qty');
            $update                = InventoryRequest::findOrFail($request_id);
            $update->total_item    = $total;

            if ($update->update()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted'), 'value' => $total]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'value' => $total]);
        }

        return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);
    }     
}
