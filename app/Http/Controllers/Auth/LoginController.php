<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Auth;
use Cookie;

class LoginController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if ($user) {
            return redirect()->route('dashboard');
        }

    	return view('pages.login');
    }

    public function login(Request $request)
    {
        //Validate
        $this->validate($request, [
            'username'      => 'required',
            'password'      => 'required'
        ]);

        //login variable
        $username   = $request->username;
        $password   = $request->password;

        //atempt login
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            if (Auth::attempt(['email' => $username, 'password' => $password])) {
                //Create log last login
                $user = Auth::user();

                //Check status user
                //Is Blocked or Grant User
                if ($user->is_block == '1') {
                    Auth::logout();
                    return redirect()->back()->withInput()->with('message', __('User is locked. Please contact your administrator!'));
                } else {
                    $update = User::findOrFail($user->id);
                    $update->last_visit = Carbon::now();
                    $update->update();

                    //redirect to dashboard
                    $notification = [
                        'msg'           => __('Welcome').' '.$user->name,
                        'alert-type'    => 'success'
                    ];

                    return redirect()->intended( route('dashboard') )->with($notification);                    
                }
            } else {
                return redirect()->back()->withInput()->with('message', __('Username or Password are invalid'));
            }
        }else{
            if (Auth::attempt(['username' => $username, 'password' => $password])) {                
                //Create log last login
                $user = Auth::user();

                //Check status user
                //Is Blocked or Grant User
                if ($user->is_block == '1') {
                    Auth::logout();
                    return redirect()->back()->withInput()->with('message', __('User is locked. Please contact your administrator!'));
                } else {
                    $update = User::findOrFail($user->id);
                    $update->last_visit = Carbon::now();
                    $update->update();

                    //redirect to dashboard
                    $notification = [
                        'msg'           => __('Welcome').' '.$user->name,
                        'alert-type'    => 'success'
                    ];

                    return redirect()->intended( route('dashboard') )->with($notification);                    
                }                
            } else {
                return redirect()->back()->withInput()->with('message', __('Username or Password are invalid'));
            }
        }            
    }

    public function logout()
    {
    	Auth::logout();
        
    	return redirect()->route('login');
    }
}
