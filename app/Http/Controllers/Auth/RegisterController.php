<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class RegisterController extends Controller
{
    public function index()
    {
    	$user = Auth::user();

    	if ($user) {
    		return redirect()->route('dashboard');
    	}
    	return view('pages.register');
    }

    public function store(Request $request)
    {
    	//Validate
    	$this->validate($request, [
    		'name'			=> 'required|string|max:255',
    		'username'		=> 'required|string|max:255|unique:users',
    		'email'			=> 'required|string|email|max:255|unique:users',
    		'password'		=> 'required|string|min:5|confirmed',
    	]);

    	//Insert record
    	$user = new User([
    		'name'		    => $request->name,
    		'username'	    => $request->username,
    		'email'		    => $request->email,
    		'password'	    => bcrypt($request->password),
            'is_block'      => 1
    	]);

    	if ($user->save()) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_block' => '0'])) {
                $notification = [
                    'message'       => __('User registered'),
                    'alert-type'    => 'success'
                ];

                return redirect()->route('dashboard')->with($notification); 
            }

            $notification = [
                'message'       => __('Please contact your administrator to activated your account'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('login')->with($notification);
    	}

    	$notification = [
    		'message'		=> __('Register user failed'),
    		'alert-type'	=> 'error'
    	];

    	return redirect()->back()->withInput()->with($notification);
    }
}
