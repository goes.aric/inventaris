<?php

namespace App\Http\Controllers\InventoryReturn;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Datatables;
use Validator;
use App\User;
use App\Department;
use App\Employee;
use App\Inventory;
use App\InventoryDetail;
use App\InventoryDelivery;
use App\InventoryDeliveryDetail;
use App\InventoryReturn;
use App\InventoryReturnDetail;
use App\StockIn;
use App\StockOut;
use Carbon\Carbon;
use Auth;
use DB;

class InventoryReturnController extends Controller
{
    /**
     * Populate inventory_return records
     */
    public function populateRecords(Request $req)
    {
        if ($req->return_start <> '') {
            $request = InventoryReturn::join('department', 'inventory_return.department_id', '=', 'department.id')->join('employee', 'inventory_return.employee_id', '=', 'employee.id')->select(['inventory_return.*', 'department.department_name', 'employee.employee_name'])->whereBetween('inventory_return.return_date', [$req->return_start, $req->return_end])->where('inventory_return.department_id', 'LIKE', $req->filter_department)->where('inventory_return.status', 'LIKE', $req->filter_status)->orderBy('inventory_return.created_at', 'DESC');
        }else {
            $request = InventoryReturn::join('department', 'inventory_return.department_id', '=', 'department.id')->join('employee', 'inventory_return.employee_id', '=', 'employee.id')->select(['inventory_return.*', 'department.department_name', 'employee.employee_name'])->where('inventory_return.department_id', 'LIKE', $req->filter_department)->where('inventory_return.status', 'LIKE', $req->filter_status)->orderBy('inventory_return.created_at', 'DESC'); 
        }

        return Datatables::of($request)
            ->addColumn('action', function($request) {
                if ($request->status == '1') {
                    return '<a href="'.route('return.show', $request->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                            <i class="icon-eye"></i>
                        </a>
                        <a href="'.route('return.edit', $request->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                            <i class="icon-note"></i>
                        </a>';
                } else {
                    return '<a href="'.route('return.show', $request->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                            <i class="icon-eye"></i>
                        </a>';
                }
            })
            ->addColumn('checkbox_column', function($request) {
                return '<input type="checkbox" data-id="'.$request->id.'" name="data[]" value="'.$request->id.'" id="data['.$request->id.']">';
            })          
            ->editColumn('status', function($request) {
                $status = "";
                if ($request->status == '1') {
                    $status = "<span class='badge badge-danger'>".__('Pending')."</span>";
                } elseif ($request->status == '2') {
                    $status = "<span class='badge badge-success'>".__('Valid')."</span>";
                }
                
                return $status;
            })
            ->addColumn('details_url', function($items) {
                return route("return.getInventoryReturnDetailData", $items->id);
            })                                                         
            ->make(true);
    }

    /**
     * Populate inventory request detail records
     */
    public function populateDetailRecords($id)
    {
        $items = InventoryReturnDetail::where('inventory_return_id', '=', $id);

        return Datatables::of($items)
            ->addColumn('unit', function($items) {
                return $items->inventory_detail->inventory->unit;
            })
            ->addColumn('action', function($items) {
                if ($items->header->status == 2 || $items->header->status == 3) {
                    return '-';
                } elseif ($items->header->status == 1) {
                    return '
                        <button data-remote="' . $items->id . '" class="btn btn-sm btn-danger btn-delete" alt="Delete" title="Delete">
                            <i class="icon-trash"></i>
                        </button>';                    
                }
            })                                 
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::where('status', '=', 1)->get();

        return view('pages.inv_return.index', compact('department'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::where('status', '=', 1)->get();

        return view('pages.inv_return.create', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'return_date'           => 'required',
            'inventory_delivery'    => 'required',
            'department'            => 'required',
            'employee'              => 'required',
            'note'                  => 'string|nullable'
        ]);      

        //Auto Generate Request Number
        $kode   = Carbon::now()->format('Ym');
        $query  = DB::select('SELECT ifnull(right(max(return_number), 6), 0) AS maxID FROM inventory_return WHERE RIGHT(LEFT(return_number, 8), 6) = :id', ['id' => $kode]);

        foreach ($query as $id) {
            $id     = (int)$id->maxID + 1;
        }
        $newID  = "ID".$kode.'-'.substr("0000000$id", -6);

        $create = new InventoryReturn;
        $create->return_number          = $newID;
        $create->return_date            = $request->return_date;
        $create->inventory_delivery_id  = $request->inventory_delivery;
        $create->department_id          = $request->department;
        $create->employee_id            = $request->employee;
        $create->note                   = $request->note;
        $create->status                 = 1;
        $create->user_id                = $user_id;

        if ($create->save()) {
            $notification = [
                'msg'           => __('Record has been saved'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('return.edit', $create->id)->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to save record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('return.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::where('status', '=', 1)->get();
        $return = InventoryReturn::findOrFail($id);

        return view('pages.inv_return.show', compact('department', 'return'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::where('status', '=', 1)->get();
        $return = InventoryReturn::findOrFail($id);

        return view('pages.inv_return.edit', compact('department', 'return'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;
        $now        = Carbon::now();

        //Validate Input
        $this->validate($request, [
            'return_date'           => 'required',
            'inventory_delivery'    => 'required',
            'department'            => 'required',
            'employee'              => 'required',
            'total_item'            => 'required',
            'note'                  => 'string|nullable',
            'status'                => 'required'                             
        ]);

        $update = InventoryReturn::findOrFail($id);
        $update->return_date            = $request->return_date;
        $update->inventory_delivery_id  = $request->inventory_delivery;
        $update->department_id          = $request->department;
        $update->employee_id            = $request->employee;
        $update->total_item             = $request->total_item;
        $update->note                   = $request->note;
        $update->status                 = $request->status;
        $update->user_id                = $user_id;

        $detail = InventoryReturnDetail::where('inventory_return_id', '=', $id)->count();
        if ($request->status == '2' && $detail == 0) {
            $notification = [
                'msg'           => __('Please complete transaction'),
                'alert-type'    => 'error'
            ];

            return redirect()->back()->with($notification);
        }        

        if ($update->update()) {
            //Update Stock if status is valid
            if ($request->status == '2') {
                $items = InventoryReturnDetail::join('inventory_detail', 'inventory_return_detail.inventory_detail_id', '=', 'inventory_detail.id')->join('inventory', 'inventory_detail.inventory_id', '=', 'inventory.id')->where('inventory.type', '<>', 3)->where('inventory_return_id', '=', $id)->get();

                if ($items->count() > 0) {
                    foreach ($items as $data) {
                        $insert[] = [
                            'reff_no'               => $update->return_number,
                            'inventory_id'          => $data->inventory_id,
                            'item_code'             => $data->item_code,
                            'item_name'             => $data->item_name,
                            'qty'                   => $data->qty,
                            'description'           => $update->note,
                            'created_at'            => $now,
                            'updated_at'            => $now                                     
                        ];
                    }

                    $insertStock = StockIn::insert($insert);
                    if ($insertStock) {
                        $notification = [
                            'msg'           => __('Record has been updated'),
                            'alert-type'    => 'success'
                        ];

                        return redirect()->route('return.index')->with($notification);
                    }
                    
                    $notification = [
                        'msg'           => __('Record has been updated but failed to out stock'),
                        'alert-type'    => 'success'
                    ];

                    return redirect()->route('return.index')->with($notification);
                } else {
                    $myItem = InventoryReturnDetail::join('inventory_detail', 'inventory_return_detail.inventory_detail_id', '=', 'inventory_detail.id')->join('inventory', 'inventory_detail.inventory_id', '=', 'inventory.id')->select('inventory_detail.id')->where('inventory.type', '=', 3)->where('inventory_return_id', '=', $id)->get();

                    foreach ($myItem as $item) {
                        $ids[] = $item->id;
                    }

                    $updateStatus = DB::table('inventory_detail')->whereIn('id', $ids)->update(['status' => '0']);

                    if ($updateStatus) {
                        $notification = [
                            'msg'           => __('Record has been updated'),
                            'alert-type'    => 'success'
                        ];

                        return redirect()->route('delivery.index')->with($notification);
                    }

                    $notification = [
                        'msg'           => __('Record has been updated but failed to update inventory status'),
                        'alert-type'    => 'success'
                    ];

                    return redirect()->route('delivery.index')->with($notification);
                }
            } else {
                $notification = [
                    'msg'           => __('Record has been updated'),
                    'alert-type'    => 'success'
                ];

                return redirect()->route('return.index')->with($notification);
            }
        }

        $notification = [
            'msg'           => __('Failed to update record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('return.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = InventoryReturn::find($id);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = InventoryReturn::whereIn('id', $ids);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }           
    }

    // Inventory Delivery Detail //
    public function populateItem(Request $request)
    {
        $item = InventoryDetail::whereIn('id', InventoryDeliveryDetail::select('inventory_detail_id')->where('inventory_delivery_id', '=', $request->inv_delivery)->get())->whereNotIn('id', InventoryReturnDetail::select('inventory_detail_id')->where('inventory_return_id', '=', $request->id))->get();

        $response = [
            'msg'           => 'List of Items',
            'item'          => $item
        ];

        return response()->json($response, 200);       
    }

    public function getDetailItem($id)
    {
        $item = InventoryDetail::join('inventory_delivery_detail', 'inventory_detail.id', '=', 'inventory_delivery_detail.inventory_detail_id')->select('inventory_detail.*', 'inventory_delivery_detail.qty')->with('inventory')->where('inventory_detail.id', $id)->first();

        return response()->json($item, 200);         
    }    

    public function storeReturnDetail(Request $request)
    {
        $item = InventoryDetail::findOrFail($request->item_id);

        $rules = array(
            'delivery_id'   => 'required',
            'item_id'       => 'required',
            'qty'           => 'required|numeric|min:1'                 
        ); 
    
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json(['type' => 'error', 'msg' => $validator->errors()->all()]);
        }else{
            $create = new InventoryReturnDetail;
            $create->inventory_return_id    = $request->delivery_id;
            $create->inventory_detail_id    = $item->id;
            $create->item_code              = $item->item_code;
            $create->detail_code            = $item->detail_code;
            $create->item_name              = $item->item_name;
            $create->identification_number  = $item->identification_number;
            $create->decree_number          = $item->decree_number;
            $create->request_number         = $item->request_number;
            $create->origin                 = $item->origin;
            $create->acquisition_year       = $item->acquisition_year;
            $create->condition              = $item->condition;
            $create->qty                    = $request->qty;           

            if ($create->save()) {
                //Update Total
                $total = InventoryReturnDetail::where('inventory_return_id', '=', $request->delivery_id)->sum('qty');
                $update                = InventoryReturn::findOrFail($request->delivery_id);
                $update->total_item    = $total;

                if ($update->update()) {
                    return response()->json(['type' => 'success', 'msg' => __('Record has been saved'), 'value' => $total]);
                }

                return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'value' => '0']);
            }
        
            return response()->json(['type' => 'error', 'msg' => __('Failed to insert record')]);
        }
    }

    public function returnDetailDestroy($id)
    {
        $delete         = InventoryReturnDetail::find($id);
        $return_id      = $delete->inventory_return_id; 

        if ($delete->delete()) {
            //Update Total
            $total = InventoryReturnDetail::where('inventory_return_id', '=', $return_id)->sum('qty');
            $update                = InventoryReturn::findOrFail($return_id);
            $update->total_item    = $total;

            if ($update->update()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted'), 'value' => $total]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'value' => $total]);
        }

        return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);
    }

    // Populate Inventory Request
    public function populateInventoryDelivery($id)
    {
        $response = InventoryDelivery::with('employee')->where('department_id', $id)->where('status', '=', 2)->get();

        return response()->json($response, 200);         
    }

    // Populate Employee
    public function populateEmployee($id)
    {
        $response = Employee::where('department_id', $id)->where('is_active', '=', 1)->get();

        return response()->json($response, 200);         
    }             
}
