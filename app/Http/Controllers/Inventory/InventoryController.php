<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\User;
use App\Category;
use App\Inventory;
use App\InventoryDetail;
use App\StockIn;
use App\StockOut;
use App\Department;
use App\InventoryDelivery;
use App\MaintenanceRecord;
use App\MaintenanceWork;
use App\MaintenancePart;
use Auth;

class InventoryController extends Controller
{
    /**
     * Populate inventory records
     */
    public function populateRecords(Request $request)
    {
        $inventory = Inventory::join('category', 'inventory.category_id', '=', 'category.id')->select(['inventory.*', 'category.category_name'])->where('inventory.type', 'LIKE', $request->type)->where('inventory.category_id', 'LIKE', $request->category);

        return Datatables::of($inventory)
            ->editColumn('type', function($inventory) {
                $type = "";
                if ($inventory->type == '1') {
                    $type = "Office Supplies";
                } elseif ($inventory->type == '2') {
                    $type = "Materials";
                } else {
                    $type = "Provisional Equity";
                }
                
                return $type;
            })
            ->addColumn('action', function($inventory) {
                return '<a href="'.route('inventory.show', $inventory->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                        <i class="icon-eye"></i>
                    </a>
                    <a href="'.route('inventory.edit', $inventory->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                        <i class="icon-note"></i>
                    </a>                  
                    <button data-remote="' . $inventory->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                        <i class="icon-trash"></i>
                    </button>';
            })
            ->addColumn('checkbox_column', function($inventory) {
                return '<input type="checkbox" data-id="'.$inventory->id.'" name="data[]" value="'.$inventory->id.'" id="data['.$inventory->id.']">';
            })
            ->addColumn('details_url', function($items) {
                return route("inventory.getInventoryDetailData", $items->id);
            })            
            ->addColumn('qty', function($inventory) {
                return '10';
            })                                             
            ->make(true);
    }

    /**
     * Populate inventory detail records
     */
    public function populateDetailRecords($id)
    {
        $items = InventoryDetail::where('inventory_id', '=', $id);

        return Datatables::of($items)
            ->editColumn('condition', function($items) {
                $condition = "";
                if ($items->condition == 1) {
                    $condition = "<span class='badge badge-success'>".__('Good')."</span>";
                } elseif ($items->condition == 2) {
                    $condition = "<span class='badge badge-warning'>".__('Lightly Damaged')."</span>";
                } else {
                    $condition = "<span class='badge badge-danger'>".__('Heavily Damaged')."</span>"; 
                }

                return $condition;
            })        
            ->editColumn('status', function($items) {
                if ($items->inventory->type <> 3) {
                    return "-";
                } else {
                    if ($items->status == 0) {
                        $status = "<span class='badge badge-success'>".__('Free')."</span>";
                    } else {
                        $status = "<span class='badge badge-danger'>".__('Used')."</span>";
                    }

                    return $status;
                }
            })
            ->addColumn('action', function($items) {
                if ($items->inventory->type <> 3) {
                    return "-";
                } else {
                    if ($items->status == '1') {
                        return '
                            <button data-remote="' . $items->inventory_delivery_id . '" class="btn btn-sm btn-secondary btn-show" alt="'. __('Show') .'" title="'. __('Show') .'">
                                <i class="icon-eye"></i>
                            </button>
                            <button data-remote="' . $items->id . '" class="btn btn-sm btn-primary btn-record" alt="'. __('Maintenance') .'" title="'. __('Show Maintenance Records') .'">
                                <i class="cil-clipboard"></i>
                            </button>';
                    }

                    return "-";
                }                                    
            })                                 
            ->make(true);
    }

    /**
     * Populate inventory detail records
     */
    public function populateMaintenanceRecords($id)
    {
        $items = MaintenanceRecord::where('inventory_detail_id', '=', $id);

        return Datatables::of($items)
            ->editColumn('cost', function($items) {
                return number_format($items->cost, 2);                                  
            })        
            ->addColumn('action', function($items) {
                return '
                    <button data-remote="' . $items->id . '" class="btn btn-sm btn-secondary btn-maintenance-detail" alt="'. __('Show') .'" title="'. __('Show Maintenance Details') .'">
                        <i class="icon-eye"></i>
                    </button>';                                   
            })                                 
            ->make(true);
    }

    /**
     * Populate inventory detail records
     */
    public function populateMaintenanceWorkRecords($id)
    {
        $items = MaintenanceWork::where('maintenance_record_id', '=', $id);

        return Datatables::of($items)
            ->editColumn('price', function($items) {
                return number_format($items->price, 2);                                  
            })                                 
            ->make(true);        
    }

    /**
     * Populate inventory detail records
     */
    public function populateMaintenancePartRecords($id)
    {
        $items = MaintenancePart::where('maintenance_record_id', '=', $id);

        return Datatables::of($items)
            ->editColumn('price', function($items) {
                return number_format($items->price, 2);                                  
            })                                 
            ->make(true);  
    }                

    public function populateFilteredRecords()
    {
        $inventory = Inventory::join('category', 'inventory.category_id', '=', 'category.id')->select(['inventory.*', 'category.category_name']);

        return Datatables::of($inventory)
            ->editColumn('type', function($inventory) {
                $type = "";
                if ($inventory->type == '1') {
                    $type = "Office Supplies";
                } elseif ($inventory->type == '2') {
                    $type = "Materials";
                } else {
                    $type = "Provisional Equity";
                }
                
                return $type;
            })
            ->addColumn('action', function($inventory) {
                return '<a href="'.route('inventory.show', $inventory->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                        <i class="icon-eye"></i>
                    </a>
                    <a href="'.route('inventory.edit', $inventory->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                        <i class="icon-note"></i>
                    </a>                  
                    <button data-remote="' . $inventory->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                        <i class="icon-trash"></i>
                    </button>';
            })
            ->addColumn('checkbox_column', function($inventory) {
                return '<input type="checkbox" data-id="'.$inventory->id.'" name="data[]" value="'.$inventory->id.'" id="data['.$inventory->id.']">';
            })
            ->addColumn('qty', function($inventory) {
                return '10';
            })                                             
            ->make(true);
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::where('status', '=', 1)->get();

        return view('pages.inventory.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::where('status', '=', 1)->get();

        return view('pages.inventory.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'type'                  => 'required',
            'category'              => 'required',
            'item_code'             => 'required|unique:inventory,item_code|max:150',
            'item_name'             => 'required|max:255',
            'brand'                 => 'string|nullable|max:255',
            'type_of_brand'         => 'string|nullable|max:255',
            'materials'             => 'string|nullable|max:255',
            'size'                  => 'string|nullable|max:255',
            'unit'                  => 'required',
            'specification'         => 'string|nullable',
            'information'           => 'string|nullable',
        ]);      

        $create = new Inventory;
        $create->type                   = $request->type;
        $create->category_id            = $request->category;
        $create->item_code              = $request->item_code;
        $create->item_name              = $request->item_name;
        $create->brand                  = $request->brand;
        $create->type_of_brand          = $request->type_of_brand;
        $create->materials              = $request->materials;
        $create->size                   = $request->size;
        $create->unit                   = $request->unit;
        $create->specification          = $request->specification;
        $create->information            = $request->information;
        $create->user_id                = $user_id;

        if ($create->save()) {
            $notification = [
                'msg'           => __('Record has been saved'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('inventory.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to save record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('inventory.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::where('status', '=', 1)->get();
        $inventory = Inventory::findOrFail($id);

        return view('pages.inventory.show', compact('category', 'inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('status', '=', 1)->get();
        $inventory = Inventory::findOrFail($id);

        return view('pages.inventory.edit', compact('category', 'inventory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'type'                  => 'required',            
            'category'              => 'required',
            'item_code'             => 'required|max:150|unique:inventory,item_code,'.$id.'',
            'item_name'             => 'required|max:255',
            'brand'                 => 'string|nullable|max:255',
            'type_of_brand'         => 'string|nullable|max:255',
            'materials'             => 'string|nullable|max:255',
            'size'                  => 'string|nullable|max:255',
            'unit'                  => 'required',
            'specification'         => 'string|nullable',
            'information'           => 'string|nullable',                                 
        ]);      

        $update = Inventory::findOrFail($id);
        $update->type                   = $request->type;
        $update->category_id            = $request->category;
        $update->item_code              = $request->item_code;
        $update->item_name              = $request->item_name;
        $update->brand                  = $request->brand;
        $update->type_of_brand          = $request->type_of_brand;
        $update->materials              = $request->materials;
        $update->size                   = $request->size;
        $update->unit                   = $request->unit;
        $update->specification          = $request->specification;
        $update->information            = $request->information;
        $update->user_id                = $user_id;

        if ($update->update()) {
            $notification = [
                'msg'           => __('Record has been updated'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('inventory.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to update record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('inventory.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Inventory::find($id);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = Inventory::whereIn('id', $ids);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }           
    }

    public function inventoryUserDetails($id)
    {
        $inventory_user = InventoryDelivery::with('department')->with('employee')->where('id', '=', $id)->first();

        return response()->json($inventory_user, 200);
    } 
}
