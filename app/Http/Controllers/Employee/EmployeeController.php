<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\User;
use App\Department;
use App\Employee;
use Auth;

class EmployeeController extends Controller
{
    /**
     * Populate employee records
     */
    public function populateRecords()
    {
        $employee = Employee::join('department', 'employee.department_id', '=', 'department.id')->select(['employee.*', 'department.department_name']);

        return Datatables::of($employee)
            ->addColumn('action', function($employee) {
                return '<a href="'.route('employee.show', $employee->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                        <i class="icon-eye"></i>
                    </a>
                    <a href="'.route('employee.edit', $employee->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                        <i class="icon-note"></i>
                    </a>                  
                    <button data-remote="' . $employee->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                        <i class="icon-trash"></i>
                    </button>';
            })
            ->addColumn('checkbox_column', function($employee) {
                return '<input type="checkbox" data-id="'.$employee->id.'" name="data[]" value="'.$employee->id.'" id="data['.$employee->id.']">';
            })
            ->editColumn('employee_status', function($employee) {
                $status = "";
                if ($employee->employee_status == '1') {
                    $status = "<span class='badge badge-info'>".__('Honorarium')."</span>";
                }else{
                    $status = "<span class='badge badge-success'>".__('Permanent')."</span>";
                }
                return $status;               
            })            
            ->editColumn('is_active', function($employee) {
                $status = "";
                if ($employee->is_active == '1') {
                    $status = "<span class='badge badge-success'>".__('Active')."</span>";
                }else{
                    $status = "<span class='badge badge-danger'>".__('Inactive')."</span>";
                }
                return $status;               
            })                                 
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.employee.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::where('status', '=', 1)->get();

        return view('pages.employee.create', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'employee_number'       => 'required|unique:employee,employee_number|max:150',
            'employee_name'         => 'required|max:255',
            'payroll_number'        => 'required|unique:employee,payroll_number|max:150',
            'department'            => 'required',
            'group'                 => 'required|max:255',
            'position'              => 'required|max:255',
            'salary'                => 'required|numeric',
            'employee_status'       => 'required',
            'level_of_education'    => 'required|max:255',
            'majoring_in_education' => 'required|max:255',
            'other'                 => 'string|nullable|max:255',
            'description'           => 'string|nullable',
            'is_active'             => 'required',
        ]);      

        $create = new Employee;
        $create->employee_number        = $request->employee_number;
        $create->employee_name          = $request->employee_name;
        $create->payroll_number         = $request->payroll_number;
        $create->department_id          = $request->department;
        $create->group                  = $request->group;
        $create->position               = $request->position;
        $create->salary                 = $request->salary;
        $create->employee_status        = $request->employee_status;
        $create->level_of_education     = $request->level_of_education;
        $create->majoring_in_education  = $request->majoring_in_education;
        $create->other                  = $request->other;
        $create->description            = $request->description;
        $create->is_active              = $request->is_active;
        $create->user_id                = $user_id;

        if ($create->save()) {
            $notification = [
                'msg'           => __('Record has been saved'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('employee.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to save record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('employee.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::where('status', '=', 1)->get();
        $employee = Employee::findOrFail($id);

        return view('pages.employee.show', compact('department', 'employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::where('status', '=', 1)->get();
        $employee = Employee::findOrFail($id);

        return view('pages.employee.edit', compact('department', 'employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'employee_number'       => 'required|max:150|unique:employee,employee_number,'.$id.'',
            'employee_name'         => 'required|max:255',
            'payroll_number'        => 'required|max:150|unique:employee,payroll_number,'.$id.'',
            'department'            => 'required',
            'group'                 => 'required|max:255',
            'position'              => 'required|max:255',
            'salary'                => 'required|numeric',
            'employee_status'       => 'required',
            'level_of_education'    => 'required|max:255',
            'majoring_in_education' => 'required|max:255',
            'other'                 => 'string|nullable|max:255',
            'description'           => 'string|nullable',
            'is_active'             => 'required',                      
        ]);      

        $update = Employee::findOrFail($id);
        $update->employee_number        = $request->employee_number;
        $update->employee_name          = $request->employee_name;
        $update->payroll_number         = $request->payroll_number;
        $update->department_id          = $request->department;
        $update->group                  = $request->group;
        $update->position               = $request->position;
        $update->salary                 = $request->salary;
        $update->employee_status        = $request->employee_status;
        $update->level_of_education     = $request->level_of_education;
        $update->majoring_in_education  = $request->majoring_in_education;
        $update->other                  = $request->other;
        $update->description            = $request->description;
        $update->is_active              = $request->is_active;
        $update->user_id                = $user_id;

        if ($update->update()) {
            $notification = [
                'msg'           => __('Record has been updated'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('employee.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to update record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('employee.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Employee::find($id);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = Employee::whereIn('id', $ids);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }           
    } 
}
