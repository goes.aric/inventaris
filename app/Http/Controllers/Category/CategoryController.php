<?php

namespace App\Http\Controllers\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\User;
use App\Category;
use Auth;

class CategoryController extends Controller
{
    /**
     * Populate category records
     */
    public function populateRecords()
    {
        $category = Category::select(['id', 'category_name', 'description', 'status', 'created_at', 'updated_at']);

        return Datatables::of($category)
            ->addColumn('action', function($category) {
                return '<a href="'.route('category.show', $category->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                        <i class="icon-eye"></i>
                    </a>
                    <a href="'.route('category.edit', $category->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                        <i class="icon-note"></i>
                    </a>                  
                    <button data-remote="' . $category->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                        <i class="icon-trash"></i>
                    </button>';
                })
            ->addColumn('checkbox_column', function($category) {
                return '<input type="checkbox" data-id="'.$category->id.'" name="data[]" value="'.$category->id.'" id="data['.$category->id.']">';
                })
            ->editColumn('status', function($category) {
                $status = "";
                if ($category->status == '1') {
                    $status = "<span class='badge badge-success'>".__('Active')."</span>";
                }else{
                    $status = "<span class='badge badge-danger'>".__('Inactive')."</span>";
                }
                return $status;               
                })                    
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'category_name'     => 'required|unique:category,category_name|max:255',
            'description'       => 'string|nullable',
            'status'            => 'required',
        ]);      

        $create = new Category;
        $create->category_name      = $request->category_name;
        $create->description        = $request->description;
        $create->status             = $request->status;
        $create->user_id            = $user_id;

        if ($create->save()) {
            $notification = [
                'msg'           => __('Record has been saved'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('category.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to save record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('category.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);

        return view('pages.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('pages.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'category_name'     => 'required|max:255|unique:category,category_name,'.$id.'',
            'description'       => 'string|nullable',
            'status'            => 'required',            
        ]);      

        $update = Category::findOrFail($id);
        $update->category_name      = $request->category_name;
        $update->description        = $request->description;
        $update->status             = $request->status;
        $update->user_id            = $user_id;

        if ($update->update()) {
            $notification = [
                'msg'           => __('Record has been updated'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('category.index')->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to update record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('category.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Category::find($id);

        try {
            if ($delete->delete()) {

                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = Category::whereIn('id', $ids);

        try {
            if ($delete->delete()) {

                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }           
    } 
}
