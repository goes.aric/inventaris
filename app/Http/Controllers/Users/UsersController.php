<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Datatables;
use App\User;
use App\UsersGroup;
use App\UsersGroupItems;
use App\UsersRoleGroups;
use Auth;

class UsersController extends Controller
{
    /**
     * Populate user records
     */
    public function populateRecords()
    {
        $user = User::select(['users.id', 'users.name', 'users.email', 'users.is_block', 'users.created_at', 'users.updated_at', 'users.last_visit']);

        return Datatables::of($user)
            ->addColumn('action', function($user) {
                return '<a href="'.route('users.show', $user->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                        <i class="icon-eye"></i>
                    </a>
                    <a href="'.route('users.edit', $user->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                        <i class="icon-note"></i>
                    </a>
                    <button data-remote="' . $user->id . '" class="btn btn-sm btn-warning btn-reset" alt="'.__('Reset Password').'" title="'.__('Reset Password User').'">
                        <i class="icon-refresh"></i>
                    </button>                    
                    <button data-remote="' . $user->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                        <i class="icon-trash"></i>
                    </button>';
                })
            ->addColumn('checkbox_column', function($user) {
                return '<input type="checkbox" data-id="'.$user->id.'" name="data[]" value="'.$user->id.'" id="data['.$user->id.']">';
                })
            ->editColumn('email', function($user) {
                return '<a href="mailto:'.$user->email.'" alt="'.$user->email.'" title="'.$user->email.'">'.$user->email.'</a>';
                })
            ->editColumn('is_block', function($user) {
                $status = "";
                if ($user->is_block == '0') {
                    $status = "<span class='badge badge-success'>".__('Active')."</span>";
                }else{
                    $status = "<span class='badge badge-danger'>".__('Blocked')."</span>";
                }
                return $status;               
                })         
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = UsersGroup::get();
        return view('pages.users.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate Input
        $this->validate($request, [
            'name'          => 'required|max:255',
            'username'      => 'required|max:150|unique:users',
            'email'         => 'required|email|unique:users|max:255',
            'password'      => 'required|string|min:5|confirmed',
            'status'        => 'required',
        ]);      

        if (empty($request->group)) {
            $notification = array(
                'msg'           => __('Please select at least one of users group'),
                'alert-type'    => 'error'
            );

            return redirect()->back()->withInput()->with($notification);
        }else{
            $create = new User;
            $create->name           = $request->name;
            $create->username       = $request->username;
            $create->email          = $request->email;
            $create->password       = bcrypt($request->password);
            $create->is_block       = $request->status;

            if ($create->save()) {
                $now = date("Y-m-d H:i:s");

                //Insert all new group
                $group_items = [];
                foreach ($request->group as $groups) {
                    $group_items[] = [
                        'user_id'               => $create->id,
                        'users_group_id'        => $groups,
                        'created_at'            => $now,
                        'updated_at'            => $now                
                    ];
                }
                UsersRoleGroups::insert($group_items);

                $notification = [
                    'msg'           => __('Record has been saved'),
                    'alert-type'    => 'success'
                ];

                return redirect()->route('users.index')->with($notification);
            }

            $notification = [
                'msg'           => __('Failed to save record'),
                'alert-type'    => 'error'
            ];

            return redirect()->route('users.index')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = UsersGroup::get();
        $user = User::findOrFail($id);

        return view('pages.users.show', compact('user', 'group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = UsersGroup::get();
        $user = User::findOrFail($id);           

        return view('pages.users.edit', compact('user', 'group'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate Input
        $this->validate($request, [
            'name'          => 'required|max:255',
            'email'         => 'required|email|max:255|unique:users,email,'.$id.'',
            'username'      => 'required|unique:users,username,'.$id.'',
            'status'        => 'required',
        ]);      

        if (empty($request->group)) {
            $notification = array(
                'msg'           => __('Please select at least one of users group'),
                'alert-type'    => 'error'
            );

            return redirect()->back()->withInput()->with($notification);
        }else{
            $update = User::findOrFail($id);
            $update->name           = $request->name;
            $update->email          = $request->email;
            $update->username       = $request->username;
            $update->is_block       = $request->status;

            if ($update->update()) {
                $now = date("Y-m-d H:i:s");

                //First delete all old permissions
                UsersRoleGroups::where('user_id', $id)->delete();            

                //Then insert all new group
                $group_items = [];
                foreach ($request->group as $groups) {
                    $group_items[] = [
                        'user_id'               => $id,
                        'users_group_id'        => $groups,
                        'created_at'            => $now,
                        'updated_at'            => $now                
                    ];
                }

                UsersRoleGroups::insert($group_items);

                $notification = [
                    'msg'           => __('Record has been updated'),
                    'alert-type'    => 'success'
                ];

                return redirect()->route('users.index')->with($notification);
            }

            $notification = [
                'msg'           => __('Failed to update record'),
                'alert-type'    => 'error'
            ];

            return redirect()->route('users.index')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = User::find($id);

        try {
            if ($delete->delete()) {

                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = User::whereIn('id', $ids);

        try {
            if ($delete->delete()) {

                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }  
    }

    public function profile()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('pages.users.profile', compact('user'));
    }    

    public function password()
    {
        return view('pages.users.password');        
    }

    public function update_password(Request $request)
    {
        $user = Auth::user();

        //Validate Input
        $this->validate($request, [
            'old_password'  => 'required|string|min:5',
            'password'      => 'required|string|min:5|confirmed',
        ]);

        $update = User::findOrFail($user->id);

        //Check input old password with current password
        if (Hash::check($request->old_password, $update->password)){
            if (Hash::check($request->password, $update->password)){
                //Denied change password if old password is same with new password
                $notification = [
                    'msg'           => __("New password can't same as old password"),
                    'alert-type'    => 'warning'
                ];

                return back()->withInput()->with($notification);                
            }else{
                //Change password
                $update->password   = bcrypt($request->password);

                if ($update->update()) {

                    $notification = [
                        'msg'           => __('Password has been updated'),
                        'alert-type'    => 'success'
                    ];

                    return redirect()->route('dashboard')->with($notification);
                }
            }           
        }else{

            $notification = [
                'msg'           => __("Old password doesn't match, can't continue update password"),
                'alert-type'    => 'error'
            ];

            return back()->withInput()->with($notification); 
        }
    }

    public function resetPassword($id)
    {
        $user = User::findOrFail($id);
        $user->password = bcrypt('12345');

        if ($user->update()) {

            $notification = [
                'msg'   => __('Password has been reseted'),
                'type'  => 'success'
            ];

            return response()->json($notification);
        }

        $notification = [
            'msg'   => __('Failed to reset password'),
            'type'  => 'error'
        ];

        return response()->json($notification);        
    }

    public function getUserGroups($id)
    {
        $data = [];
        $group_items = UsersRoleGroups::join('users_group', 'users_role_groups.users_group_id', '=', 'users_group.id')
                        ->select('users_role_groups.users_group_id', 'users_group.group_name')
                        ->where('users_role_groups.user_id', $id)->get();

        foreach ($group_items as $items) {
            $data[] = [
                'id'            => $items->users_group_id,
                'group_name'    => $items->group_name             
            ]; 
        }

        //dd($data);
        return response()->json($data, 200);
    }        
}
