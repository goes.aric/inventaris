<?php

namespace App\Http\Controllers\InventoryProcurement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Datatables;
use Validator;
use App\User;
use App\Inventory;
use App\InventoryDetail;
use App\InventoryProcurement;
use App\InventoryProcurementDetail;
use App\InventoryProcurementSubDetail;
use App\StockIn;
use App\StockOut;
use Carbon\Carbon;
use Auth;
use DB;

class InventoryProcurementController extends Controller
{
    /**
     * Populate inv_procurement records
     */
    public function populateRecords(Request $req)
    {
        if ($req->procurement_start <> '') {
            $procurement = InventoryProcurement::select(['inv_procurement.*'])->whereBetween('inv_procurement.procurement_date', [$req->procurement_start, $req->procurement_end])->where('inv_procurement.status', 'LIKE', $req->filter_status)->orderBy('inv_procurement.created_at', 'DESC');
        }else {
            $procurement = InventoryProcurement::select(['inv_procurement.*'])->where('inv_procurement.status', 'LIKE', $req->filter_status)->orderBy('inv_procurement.created_at', 'DESC'); 
        }

        return Datatables::of($procurement)
            ->addColumn('action', function($procurement) {
                if ($procurement->status == '2') {
                    return '<a href="'.route('procurement.show', $procurement->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                            <i class="icon-eye"></i>
                        </a>';
                } else {
                    return '<a href="'.route('procurement.show', $procurement->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                            <i class="icon-eye"></i>
                        </a>
                        <a href="'.route('procurement.edit', $procurement->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                            <i class="icon-note"></i>
                        </a>';
                }
            })
            ->addColumn('checkbox_column', function($procurement) {
                return '<input type="checkbox" data-id="'.$procurement->id.'" name="data[]" value="'.$procurement->id.'" id="data['.$procurement->id.']">';
            })           
            ->editColumn('status', function($procurement) {
                $status = "";
                if ($procurement->status == '1') {
                    $status = "<span class='badge badge-success'>".__('Valid')."</span>";
                } elseif ($procurement->status == '2') {
                    $status = "<span class='badge badge-danger'>".__('Pending')."</span>";                  
                }
                
                return $status;
            })
            ->addColumn('details_url', function($items) {
                return route("procurement.getInventoryProcurementDetailData", $items->id);
            })                                                         
            ->make(true);
    }

    /**
     * Populate inventory procurement detail records
     */
    public function populateDetailRecords($id)
    {
        $items = InventoryProcurementDetail::where('inv_procurement_id', '=', $id);

        return Datatables::of($items)
            ->addColumn('action', function($items) {
                if ($items->header->status == 2) {
                    return '-';
                } elseif ($items->header->status == 1) {
                    return '
                        <button data-remote="' . $items->id . '" class="btn btn-sm btn-primary btn-view" alt="View" title="View">
                            <i class="icon-eye"></i>
                        </button>                    
                        <button data-remote="' . $items->id . '" class="btn btn-sm btn-danger btn-delete" alt="Delete" title="Delete">
                            <i class="icon-trash"></i>
                        </button>';                    
                }
            }) 
            ->addColumn('view', function($items) {
                if ($items->inventory->type == '3') {
                    return '
                        <button data-remote="' . $items->id . '" class="btn btn-sm btn-primary btn-view" alt="View" title="View">
                            <i class="icon-eye"></i>
                        </button>'; 
                } else {
                    return '-';
                }                   
            })                                             
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.inv_procurement.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.inv_procurement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'procurement_date'  => 'required',
            'decree_number'     => 'required',
            'reff_number'       => 'required',
            'origin'            => 'required',
            'information'       => 'string|nullable',
            'procurement_file'  => 'required|file|mimes:pdf|max:3072'
        ]);      

        //Auto Generate Request Number
        $kode   = Carbon::now()->format('Ym');
        $query  = DB::select('SELECT ifnull(right(max(procurement_number), 6), 0) AS maxID FROM inv_procurement WHERE RIGHT(LEFT(procurement_number, 8), 6) = :id', ['id' => $kode]);

        foreach ($query as $id) {
            $id     = (int)$id->maxID + 1;
        }
        $newID  = "IP".$kode.'-'.substr("0000000$id", -6);

        //Jika input file tidak kosong, upload file
        $new_name = "";
        if (!empty($request->file('procurement_file'))) {
            //Variabel file
            $file = $request->file('procurement_file');
            $new_name = $newID . '-' .$file->getClientOriginalName();

            if (Storage::exists($new_name)) {
                $new_name = $newID . '-' . rand() .$file->getClientOriginalName();
            }

            //Upload file              
            $path = storage_path('app/public/files/');
            $file->move($path, $new_name);      
        }        

        $create = new InventoryProcurement;
        $create->procurement_number = $newID;
        $create->procurement_date   = $request->procurement_date;
        $create->decree_number      = $request->decree_number;
        $create->reff_number        = $request->reff_number;
        $create->origin             = $request->origin;
        $create->information        = $request->information;
        $create->procurement_file   = $new_name;
        $create->status             = 1;
        $create->user_id            = $user_id;

        if ($create->save()) {
            $notification = [
                'msg'           => __('Record has been saved'),
                'alert-type'    => 'success'
            ];

            return redirect()->route('procurement.edit', $create->id)->with($notification);
        }

        $notification = [
            'msg'           => __('Failed to save record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('procurement.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $procurement = InventoryProcurement::findOrFail($id);

        return view('pages.inv_procurement.show', compact('procurement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $procurement = InventoryProcurement::findOrFail($id);

        return view('pages.inv_procurement.edit', compact('procurement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;
        $now        = Carbon::now();

        //Validate Input
        $this->validate($request, [
            'procurement_date'  => 'required',
            'decree_number'     => 'required',
            'reff_number'       => 'required',
            'origin'            => 'required',
            'information'       => 'string|nullable',
            'status'            => 'required',                               
        ]);

        if (!empty($request->file('procurement_file'))) {
            //Cek default di field
            if ($update->procurement_file != null) {
                //Hapus file sebelumnya
                $path = storage_path('app/public/files/').$update->procurement_file;
                if(file_exists($path)){
                    \File::delete($path);
                }
            } 

            //Variabel gambar profil baru
            $file = $request->file('procurement_file');
            $new_name = $update->order_no . '-' .$file->getClientOriginalName();

            if (Storage::exists($new_name)) {
                $new_name = $update->order_no . '-' . rand() .$file->getClientOriginalName();
            }            

            //Upload gambar profil baru              
            $path = storage_path('app/public/files/');
            $file->move($path, $new_name);

            //Update value field
            $update->procurement_file = $new_name;
        }              

        $update = InventoryProcurement::findOrFail($id);
        $update->procurement_date   = $request->procurement_date;
        $update->decree_number      = $request->decree_number;
        $update->reff_number        = $request->reff_number;
        $update->origin             = $request->origin;
        $update->information        = $request->information;
        $update->status             = $request->status;
        $update->user_id            = $user_id;

        $detail = InventoryProcurementDetail::where('inv_procurement_id', '=', $id)->count();
        if ($request->status == '2' && $detail == 0) {
            $notification = [
                'msg'           => __('Please complete transaction'),
                'alert-type'    => 'error'
            ];

            return redirect()->back()->with($notification);
        }

        if ($update->update()) {
            //Update Stock if status is valid
            if ($request->status == '2') {
                $items = InventoryProcurementDetail::where('inv_procurement_id', '=', $id)->get();
                foreach ($items as $data) {
                    $insert[] = [
                        'reff_no'               => $update->procurement_number,
                        'inventory_id'          => $data->inventory_id,
                        'item_code'             => $data->item_code,
                        'item_name'             => $data->item_name,
                        'qty'                   => $data->qty,
                        'description'           => $update->information,
                        'created_at'            => $now,
                        'updated_at'            => $now                                     
                    ];
                }

                $insertStock = StockIn::insert($insert);
                if ($insertStock) {
                    $notification = [
                        'msg'           => __('Record has been updated'),
                        'alert-type'    => 'success'
                    ];

                    return redirect()->route('procurement.index')->with($notification);
                }
                
                $notification = [
                    'msg'           => __('Record has been updated but failed to insert stock'),
                    'alert-type'    => 'success'
                ];

                return redirect()->route('procurement.index')->with($notification);
            } else {
                $notification = [
                    'msg'           => __('Record has been updated'),
                    'alert-type'    => 'success'
                ];

                return redirect()->route('procurement.index')->with($notification);
            }
        }

        $notification = [
            'msg'           => __('Failed to update record'),
            'alert-type'    => 'error'
        ];

        return redirect()->route('procurement.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = InventoryProcurement::find($id);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids = $request->data;
        $delete = InventoryProcurement::whereIn('id', $ids);

        try {
            if ($delete->delete()) {
                return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }           
    }

    // Inventory Request Detail //
    public function populateItem(Request $request)
    {
        $item = Inventory::whereNotIn('id', InventoryProcurementDetail::select('inventory_id')->where('inv_procurement_id', '=', $request->id))->get();

        $response = [
            'msg'           => 'List of Items',
            'item'          => $item
        ];

        return response()->json($response, 200);         
    }

    public function getDetailItem($id)
    {
        $item = Inventory::where('id', $id)->first();
        
        $response = [
            'type'              => $item->type,
            'item_code'         => $item->item_code,
            'item_name'         => $item->item_name,
            'unit'              => $item->unit
        ];

        return response()->json($response, 200);         
    }    

    public function storeProcurementDetail(Request $request)
    {
        //dd($request->all());

        $item = Inventory::findOrFail($request->item_id);

        $rules = array(
            'procurement_id'    => 'required',
            'decree_number'     => 'required',
            'request_number'    => 'required',
            'origin'            => 'required',
            'date'              => 'required|date',
            'item_id'           => 'required',
            'type'              => 'required',
            'qty'               => 'required|numeric|min:1',
            'price'             => 'required|numeric|min:1',
            'sub_total'         => 'required|numeric|min:1'                
        ); 
    
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return response()->json(['type' => 'error', 'msg' => $validator->errors()->all()]);
        }else{
            $now = Carbon::now();
            $year = date('Y', strtotime($request->date));

            if ($request->type == 3) {
                for ($i=0; $i < $request->qty; $i++) {
                    $detail_code            = 'detail_code'.$i;
                    $identification_number  = 'identification_number'.$i;
                    $condition              = 'condition'.$i;
                    $description            = 'description'.$i;

                    if ($request->$identification_number == '') {
                        return response()->json(['type' => 'error', 'msg' => __('All identification number is required')]);
                    } else {
                        $collection_id[] = $request->$detail_code;

                        $insert[] = [
                            'inventory_id'          => $request->item_id,
                            'item_code'             => $request->item_code,
                            'detail_code'           => $request->$detail_code,
                            'item_name'             => $request->item_name,
                            'identification_number' => $request->$identification_number,
                            'decree_number'         => $request->decree_number,
                            'request_number'        => $request->request_number,
                            'origin'                => $request->origin,
                            'acquisition_year'      => $year, 
                            'condition'             => $request->$condition,
                            'price'                 => $request->price, 
                            'information'           => $request->$description,
                            'created_at'            => $now,
                            'updated_at'            => $now            
                        ];                                                
                    }
                }

                $create = new InventoryProcurementDetail;
                $create->inv_procurement_id = $request->procurement_id;
                $create->inventory_id       = $request->item_id;
                $create->item_code          = $request->item_code;
                $create->item_name          = $request->item_name;
                $create->qty                = $request->qty;
                $create->unit               = $request->unit;
                $create->price              = $request->price;
                $create->sub_total          = $request->sub_total;           

                if ($create->save()) {
                    $insertData = InventoryDetail::insert($insert);

                    if ($insertData) {
                        $collection = InventoryDetail::whereIn('detail_code', $collection_id)->get();

                        foreach ($collection as $items) {
                            $sub[] = [
                                'inv_procurement_detail_id' => $create->id,
                                'inventory_detail_id'       => $items->id,
                                'item_code'                 => $items->item_code,
                                'detail_code'               => $items->detail_code,
                                'item_name'                 => $items->item_name,
                                'identification_number'     => $items->identification_number,
                                'decree_number'             => $items->decree_number,
                                'request_number'            => $items->request_number,
                                'origin'                    => $items->origin,
                                'acquisition_year'          => $items->acquisition_year,
                                'condition'                 => $items->condition,
                                'price'                     => $items->price, 
                                'information'               => $items->information,
                                'created_at'                => $now,
                                'updated_at'                => $now                                              
                            ];
                        }

                        $insertSub = InventoryProcurementSubDetail::insert($sub);

                        //Update Total
                        $total = InventoryProcurementDetail::where('inv_procurement_id', '=', $request->procurement_id)->sum('qty');
                        $grand_total = InventoryProcurementDetail::where('inv_procurement_id', '=', $request->procurement_id)->sum('sub_total');
                        $update                = InventoryProcurement::findOrFail($request->procurement_id);
                        $update->total_item    = $total;
                        $update->total         = $grand_total;                  

                        if ($update->update()) {
                            return response()->json(['type' => 'success', 'msg' => __('Record has been saved'), 'total' => $total, 'grand_total' => $grand_total]);
                        }

                        return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'total' => $total, 'grand_total' => $grand_total]);
                    } else {
                        return response()->json(['type' => 'error', 'msg' => __('Error while insert inventory details')]);
                    }
                } else {
                    return response()->json(['type' => 'error', 'msg' => __('Failed to insert record')]);
                }                
            } else {
                $insert[] = [
                    'inventory_id'          => $request->item_id,
                    'item_code'             => $request->item_code,
                    'detail_code'           => $request->detail_code,
                    'item_name'             => $request->item_name,
                    'identification_number' => $request->identification_number,
                    'decree_number'         => $request->decree_number,
                    'request_number'        => $request->request_number,
                    'origin'                => $request->origin,
                    'acquisition_year'      => $year, 
                    'condition'             => $request->condition,
                    'price'                 => $request->price, 
                    'information'           => $request->description,
                    'created_at'            => $now,
                    'updated_at'            => $now                                 
                ];

                $create = new InventoryProcurementDetail;
                $create->inv_procurement_id = $request->procurement_id;
                $create->inventory_id       = $request->item_id;
                $create->item_code          = $request->item_code;
                $create->item_name          = $request->item_name;
                $create->qty                = $request->qty;
                $create->unit               = $request->unit;
                $create->price              = $request->price;
                $create->sub_total          = $request->sub_total;           

                if ($create->save()) {
                    $checked = InventoryDetail::where('detail_code', '=', $request->detail_code)->where('inventory_id', '=', $request->item_id)->first();

                    if ($checked) {
                        $collection = InventoryDetail::whereIn('detail_code', [$request->detail_code])->get();

                        foreach ($collection as $items) {
                            $sub[] = [
                                'inv_procurement_detail_id' => $create->id,
                                'inventory_detail_id'       => $items->id,
                                'item_code'                 => $items->item_code,
                                'detail_code'               => $items->detail_code,
                                'item_name'                 => $items->item_name,
                                'identification_number'     => $items->identification_number,
                                'decree_number'             => $items->decree_number,
                                'request_number'            => $items->request_number,
                                'origin'                    => $items->origin,
                                'acquisition_year'          => $items->acquisition_year,
                                'condition'                 => $items->condition,
                                'price'                     => $items->price, 
                                'information'               => $items->information,
                                'created_at'                => $now,
                                'updated_at'                => $now                                             
                            ];
                        }

                        $insertSub = InventoryProcurementSubDetail::insert($sub);

                        //Update Total
                        $total = InventoryProcurementDetail::where('inv_procurement_id', '=', $request->procurement_id)->sum('qty');
                        $grand_total = InventoryProcurementDetail::where('inv_procurement_id', '=', $request->procurement_id)->sum('sub_total');
                        $update                = InventoryProcurement::findOrFail($request->procurement_id);
                        $update->total_item    = $total;
                        $update->total         = $grand_total;                  

                        if ($update->update()) {
                            return response()->json(['type' => 'success', 'msg' => __('Record has been saved'), 'total' => $total, 'grand_total' => $grand_total]);
                        }

                        return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'total' => $total, 'grand_total' => $grand_total]);                        
                        
                    } else {
                        $insertData = InventoryDetail::insert($insert);

                        if ($insertData) {
                            $collection = InventoryDetail::whereIn('detail_code', [$request->detail_code])->get();

                            foreach ($collection as $items) {
                                $sub[] = [
                                    'inv_procurement_detail_id' => $create->id,
                                    'inventory_detail_id'       => $items->id,
                                    'item_code'                 => $items->item_code,
                                    'detail_code'               => $items->detail_code,
                                    'item_name'                 => $items->item_name,
                                    'identification_number'     => $items->identification_number,
                                    'decree_number'             => $items->decree_number,
                                    'request_number'            => $items->request_number,
                                    'origin'                    => $items->origin,
                                    'acquisition_year'          => $items->acquisition_year,
                                    'condition'                 => $items->condition,
                                    'price'                     => $items->price, 
                                    'information'               => $items->information,
                                    'created_at'                => $now,
                                    'updated_at'                => $now                                                 
                                ];
                            }

                            $insertSub = InventoryProcurementSubDetail::insert($sub);

                            //Update Total
                            $total = InventoryProcurementDetail::where('inv_procurement_id', '=', $request->procurement_id)->sum('qty');
                            $grand_total = InventoryProcurementDetail::where('inv_procurement_id', '=', $request->procurement_id)->sum('sub_total');
                            $update                = InventoryProcurement::findOrFail($request->procurement_id);
                            $update->total_item    = $total;
                            $update->total         = $grand_total;                  

                            if ($update->update()) {
                                return response()->json(['type' => 'success', 'msg' => __('Record has been saved'), 'total' => $total, 'grand_total' => $grand_total]);
                            }

                            return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'total' => $total, 'grand_total' => $grand_total]);
                        } else {
                            return response()->json(['type' => 'error', 'msg' => __('Error while insert inventory details')]);
                        }
                    }
                } else {
                    return response()->json(['type' => 'error', 'msg' => __('Failed to insert record')]);
                }                
            }
        }
    }

    public function getProcurementDetailItem($id)
    {
        $item = InventoryProcurementDetail::with('inventory')->where('id', $id)->first();

        return response()->json($item, 200);         
    }    

    public function getSubDetailItem($id)
    {
        $item = InventoryProcurementSubDetail::where('inv_procurement_detail_id', '=', $id)->get();

        return response()->json($item, 200);
    }

    public function procurementDetailDestroy($id)
    {
        //Find all inventory detail that make with this record
        //after that remove all inventory detail that references by this record
        $inv_detail = InventoryDetail::whereIn('id', InventoryProcurementSubDetail::select('inventory_detail_id')->where('inv_procurement_detail_id', '=', $id)->get())->delete();

        $delete         = InventoryProcurementDetail::find($id);
        $procurement_id = $delete->inv_procurement_id; 

        if ($delete->delete()) {
            //Update Total
            $total = InventoryProcurementDetail::where('inv_procurement_id', '=', $procurement_id)->sum('qty');
            $grand_total = InventoryProcurementDetail::where('inv_procurement_id', '=', $procurement_id)->sum('sub_total');
            $update                = InventoryProcurement::findOrFail($procurement_id);
            $update->total_item    = $total;
            $update->total         = $grand_total;         

            if ($update->update()) {
                return response()->json(['type' => 'success', 'msg' => __('Record has been deleted'), 'total' => $total, 'grand_total' => $grand_total]);
            }

            return response()->json(['type' => 'error', 'msg' => __('Total not updated'), 'total' => $total, 'grand_total' => $grand_total]);
        }

        return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);
    }

    public function autoCode(Request $request)
    {
        $item = Inventory::find($request->item);
        $query  = DB::select('SELECT ifnull(right(max(detail_code), 4), 0) AS maxID FROM inventory_detail WHERE inventory_id = :id', ['id' => $request->item]);

        $id = 0;
        foreach ($query as $id) {
            $id     = (int)$id->maxID;
        }

        $code = [];
        if ($item->type == 3) {
            for ($i=0; $i < $request->qty; $i++) {
                $id = $id + 1; 
                $code[] = [
                    'type'  => $item->type,
                    'code'  => $request->code.'-'.substr("0000000$id", -4)
                ];
            }
        } else {
            $code[] = [
                'type'  => $item->type,
                'code'  => $request->code.'-XXXX'
            ];            
        }

        return $code;
    }     
}
