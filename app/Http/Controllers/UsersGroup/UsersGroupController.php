<?php

namespace App\Http\Controllers\UsersGroup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UsersGroup;
use App\UsersGroupItems;
use Datatables;
use Auth;
use DB;

class UsersGroupController extends Controller
{
    /**
     * Populate group records
     */
    public function populateRecords()
    {
        $group = UsersGroup::select(['id', 'group_name', 'group_description', 'created_at', 'updated_at']);

        return Datatables::of($group)
            ->addColumn('action', function($group) {
                return '<a href="'.route('users-group.show', $group->id).'" class="btn btn-sm btn-secondary" alt="'.__('Show').'" title="'.__('Show').'">
                        <i class="icon-eye"></i>
                    </a>
                    <a href="'.route('users-group.edit', $group->id).'" class="btn btn-sm btn-primary" alt="'.__('Edit').'" title="'.__('Edit').'">
                        <i class="icon-note"></i>
                    </a>
                    <button data-remote="' . $group->id . '" class="btn btn-sm btn-danger btn-delete" alt="'.__('Delete').'" title="'.__('Delete').'">
                        <i class="icon-trash"></i>
                    </button>';
                })
            ->addColumn('checkbox_column', function($group) {
                return '<input type="checkbox" data-id="'.$group->id.'" name="data[]" value="'.$group->id.'" id="data['.$group->id.']">';
                })
            ->make(true); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.users_group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'data'  => $this->appPermissions()
        ];                
        return view('pages.users_group.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'group_name'            => 'required|max:255|unique:users_group',
            'group_description'     => 'required',
        ]);      

        if (empty($request->permissions)) {
            $notification = array(
                'msg'           => __('Please select the privilege items'),
                'alert-type'    => 'error'
            );

            return redirect()->back()->withInput()->with($notification);
        }else{
            try {
                $insert = DB::transaction(function() use($request, $user_id) {
                    $user       = Auth::user();
                    $user_id    = $user->id;    
                                
                    $users_groups = new UsersGroup([
                        'group_name'            => $request->group_name,
                        'group_description'     => $request->group_description,
                        'user_id'               => $user_id                    
                    ]);

                    //check if success
                    if ($users_groups->save()) { 
                        $now = date("Y-m-d H:i:s");
                        $permission_items = [];
                        foreach ($request->permissions as $permissions) {
                            $item = $this->getPermissions($permissions);
                            $permission_items[] = [
                                'privilege_item_name'   => $item['itemName'],
                                'key'                   => $item['itemKey'],
                                'group_key'             => $item['groupKey'],
                                'master_group'          => $item['masterGroup'],
                                'index_url'             => $item['indexUrl'],
                                'users_group_id'        => $users_groups->id,
                                'created_at'            => $now,
                                'updated_at'            => $now                
                            ];
                        }

                        UsersGroupItems::insert($permission_items);
                    }

                    return true;
                }, 5);

                if ($insert === true) {
                    $notification = [
                        'msg'           => __('Record has been saved'),
                        'alert-type'    => 'success'
                    ];

                    return redirect()->route('users-group.index')->with($notification); 
                }else{  
                    $notification = [
                        'msg'           => __('Failed to save record'),
                        'alert-type'    => 'error'
                    ];

                    return redirect()->route('users-group.index')->with($notification);                                
                }
            } catch (Exception $e) {
                $notification = [
                    'msg'           => __('Failed to save record. Error message:').' '.$e->getMessage(),
                    'alert-type'    => 'error'
                ];

                return redirect()->route('users-group.index')->with($notification);             
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'data'  => $this->appPermissions()
        ];        
        $group = UsersGroup::findOrFail($id);

        return view('pages.users_group.show', compact('group', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data'  => $this->appPermissions()
        ];
        $group = UsersGroup::findOrFail($id);

        return view('pages.users_group.edit', compact('group', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user       = Auth::user();
        $user_id    = $user->id;

        //Validate Input
        $this->validate($request, [
            'group_name'            => 'required|max:255|unique:users_group,group_name,'.$id.'',
            'group_description'     => 'required|max:255',
        ]); 

        if (empty($request->permissions)) {
            $notification = array(
                'msg'           => __('Please select the privilege items'),
                'alert-type'    => 'error'
            );

            return redirect()->back()->withInput()->with($notification);
        }else{
            try {
                $update = DB::transaction(function() use($request, $id, $user_id) {
                    $users_groups = UsersGroup::findOrFail($id);
                    $users_groups->group_name           = $request->group_name;
                    $users_groups->group_description    = $request->group_description;
                    $users_groups->user_id              = $user_id;               

                    if ($users_groups->update()) {
                        $now = date("Y-m-d H:i:s");

                        //First delete all old permissions
                        UsersGroupItems::where('users_group_id', $users_groups->id)->delete();

                        //Then insert all new permissions
                        $permission_items = [];
                        foreach ($request->permissions as $permissions) {
                            $item = $this->getPermissions($permissions);
                            $permission_items[] = [
                                'privilege_item_name'   => $item['itemName'],
                                'key'                   => $item['itemKey'],
                                'group_key'             => $item['groupKey'],
                                'master_group'          => $item['masterGroup'],
                                'index_url'             => $item['indexUrl'],
                                'users_group_id'        => $users_groups->id,
                                'created_at'            => $now,
                                'updated_at'            => $now                
                            ];
                        }

                        UsersGroupItems::insert($permission_items);
                    }

                    return true;
                }, 5);

                if ($update === true) {
                    $notification = [
                        'msg'           => __('Record has been updated'),
                        'alert-type'    => 'success'
                    ];

                    return redirect()->route('users-group.index')->with($notification);
                }else{
                    $notification = [
                        'msg'           => __('Failed to update record'),
                        'alert-type'    => 'error'
                    ];

                    return redirect()->route('users-group.index')->with($notification);                    
                }
            } catch (Exception $e) {
                $notification = [
                    'msg'           => __('Failed to update record. Error message:').' '.$e->getMessage(),
                    'alert-type'    => 'error'
                ];

                return redirect()->route('users-group.index')->with($notification);              
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = UsersGroup::find($id);

        try {
            if ($delete->delete()) {
                $delete_items = UsersGroupItems::whereIn('users_group_id', $id);
                if ($delete_items->delete()) {
                    return response()->json(['type' => 'success', 'msg' => __('Record has been deleted')]);
                }
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        $ids    = $request->data;
        $delete = UsersGroup::whereIn('id', $ids);

        try {
            if ($delete->delete()) {
                $delete_items = UsersGroupItems::whereIn('users_group_id', $ids)->delete();
                if ($delete_items) {
                    return response()->json(['type' => 'success', 'msg' => __('Selected record has been deleted')]);
                }
            }

            return response()->json(['type' => 'error', 'msg' => __('Failed to delete selected record')]);             
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                return response()->json(['type' => 'error', 'msg' => __('Failed to delete active records. Contact your administrator for more information.')]);
            }
            
            return response()->json(['type' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function getPermissions($key)
    {
        $permission_items = [            
            [
                'itemName'      => 'View Users Group',
                'itemKey'       => 'viewUsersGroup',
                'groupKey'      => 'Users Group',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users-group.index',
                'description'   => 'View of Users Group List'                  
            ],                
            [
                'itemName'      => 'Create New Users Group',
                'itemKey'       => 'createUsersGroup',
                'groupKey'      => 'Users Group',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users-group.index',
                'description'   => 'Create New Users Group Record'                      
            ],
            [
                'itemName'      => 'Edit Users Group',
                'itemKey'       => 'editUsersGroup',
                'groupKey'      => 'Users Group',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users-group.index',
                'description'   => 'Edit Selected Users Group Record'                     
            ],
            [
                'itemName'      => 'Delete Users Group',
                'itemKey'       => 'deleteUsersGroup',
                'groupKey'      => 'Users Group',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users-group.index',
                'description'   => 'Delete Selected Users Group Record'                     
            ],            
            [
                'itemName'      => 'View User',
                'itemKey'       => 'viewUsers',
                'groupKey'      => 'Users',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users.index',
                'description'   => 'View of User List'                  
            ],                
            [
                'itemName'      => 'Create New User',
                'itemKey'       => 'createUsers',
                'groupKey'      => 'Users',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users.index',
                'description'   => 'Create New User Record'                      
            ],
            [
                'itemName'      => 'Edit User',
                'itemKey'       => 'editUsers',
                'groupKey'      => 'Users',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users.index',
                'description'   => 'Edit Selected User Record'                     
            ],
            [
                'itemName'      => 'Delete User',
                'itemKey'       => 'deleteUsers',
                'groupKey'      => 'Users',
                'masterGroup'   => 'Settings',
                'indexUrl'      => 'users.index',
                'description'   => 'Delete Selected User Record'                     
            ],                                           
        ];

        $data = [];
        foreach ($permission_items as $item) {
            if ($item['itemKey'] == $key) {
                $data = [
                    'itemKey'       => $item['itemKey'],
                    'itemName'      => $item['itemName'],
                    'groupKey'      => $item['groupKey'],
                    'indexUrl'      => $item['indexUrl'],
                    'masterGroup'   => $item['masterGroup'],
                ];
            }
        }

        return $data;
    }  

    public function appPermissions()
    {
        $groups = [
            'groupName'     => 'Users Group',
            'description'   => 'Settings users group on this system',
            'key'           => 'Users Group',
            'items'         => [
                [
                    'itemName'      => 'View Users Group',
                    'itemKey'       => 'viewUsersGroup',
                    'groupKey'      => 'Users Group',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users-group.index',
                    'description'   => 'View of Users Group List'                  
                ],                
                [
                    'itemName'      => 'Create New Users Group',
                    'itemKey'       => 'createUsersGroup',
                    'groupKey'      => 'Users Group',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users-group.index',
                    'description'   => 'Create New Users Group Record'                      
                ],
                [
                    'itemName'      => 'Edit Users Group',
                    'itemKey'       => 'editUsersGroup',
                    'groupKey'      => 'Users Group',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users-group.index',
                    'description'   => 'Edit Selected Users Group Record'                     
                ],
                [
                    'itemName'      => 'Delete Users Group',
                    'itemKey'       => 'deleteUsersGroup',
                    'groupKey'      => 'Users Group',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users-group.index',
                    'description'   => 'Delete Selected Users Group Record'                     
                ],                              
            ]
        ];

        $users = [
            'groupName'     => 'Users',
            'description'   => 'Settings users on this system',
            'key'           => 'Users',
            'items'         => [
                [
                    'itemName'      => 'View User',
                    'itemKey'       => 'viewUsers',
                    'groupKey'      => 'Users',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users.index',
                    'description'   => 'View of User List'                  
                ],                
                [
                    'itemName'      => 'Create New User',
                    'itemKey'       => 'createUsers',
                    'groupKey'      => 'Users',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users.index',
                    'description'   => 'Create New User Record'                      
                ],
                [
                    'itemName'      => 'Edit User',
                    'itemKey'       => 'editUsers',
                    'groupKey'      => 'Users',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users.index',
                    'description'   => 'Edit Selected User Record'                     
                ],
                [
                    'itemName'      => 'Delete User',
                    'itemKey'       => 'deleteUsers',
                    'groupKey'      => 'Users',
                    'masterGroup'   => 'Settings',
                    'indexUrl'      => 'users.index',
                    'description'   => 'Delete Selected User Record'                     
                ],                               
            ]
        ];      

        $policies = array($groups, $users);

        return $policies;        
    }

    public function getUserPermissions($id)
    {
        $data = [];
        $permission_items = UsersGroupItems::where('users_group_id', $id)->get();

        foreach ($permission_items as $items) {
            $data[] = [
                'itemName'      => $items->privilege_item_name,
                'itemKey'       => $items->key,
                'groupKey'      => $items->group_key,
                'masterGroup'   => $items->master_group,
                'indexUrl'      => $items->index_url               
            ]; 
        }

        //dd($data);
        return response()->json($data, 200);
    }        
}
