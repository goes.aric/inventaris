<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceRecord extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'inventory_detail_id', 'date', 'reff_number', 'problem', 'work_description', 'cost', 'user_id',
    ];

    protected $table = 'maintenance_records';

    public function inventory_detail() {
    	return $this->belongsTo('App\InventoryDetail', 'inventory_detail_id');
    }

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function parts() {
        return $this->hasMany('App\MaintenancePart');
    }

    public function works() {
        return $this->hasMany('App\MaintenanceWork');
    }
}
