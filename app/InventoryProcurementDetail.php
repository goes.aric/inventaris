<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryProcurementDetail extends Model
{
    use Notifiable;

    protected $fillable = [
    	'inv_procurement_id', 'inventory_id', 'item_code', 'item_name', 'qty', 'unit', 'price', 'sub_total',
    ];

    protected $table = 'inv_procurement_detail';

    public function header() {
    	return $this->belongsTo('App\InventoryProcurement', 'inv_procurement_id');
    }

    public function inventory() {
        return $this->belongsTo('App\Inventory', 'inventory_id');
    }
}
