<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryDetail extends Model
{
    use Notifiable;

    protected $fillable = [
    	'inventory_id', 'item_code', 'detail_code', 'item_name', 'identification_number', 'decree_number', 'request_number', 'origin', 'acquisition_year', 'condition', 'price', 'status', 'information', 'inventory_delivery_id',
    ];

    protected $table = 'inventory_detail';

    public function inventory() {
    	return $this->belongsTo('App\Inventory', 'inventory_id');
    }

    public function user() {
    	return $this->belongsTo('App\InventoryDelivery', 'inventory_delivery_id', 'id');
    }
}
