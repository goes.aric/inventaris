<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockIn extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'reff_no', 'inventory_id', 'item_code', 'item_name', 'qty', 'description',
    ];

    protected $table = 'stock_in';

    public function inventory() {
    	return $this->belongsTo('App\Inventory', 'inventory_id');
    }
}
