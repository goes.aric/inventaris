<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'type', 'category_id', 'item_code', 'item_name', 'brand', 'type_of_brand', 'materials', 'size', 'unit', 'specification', 'information', 'user_id',
    ];

    protected $table = 'inventory';

    public function category() {
    	return $this->belongsTo('App\Category', 'category_id');
    }

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
