<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryReturnDetail extends Model
{
    use Notifiable;

    protected $fillable = [
        'inventory_return_id', 'inventory_detail_id', 'item_code', 'detail_code', 'item_name', 'identification_number', 'decree_number', 'request_number', 'origin', 'acquisition_year', 'condition', 'qty',
    ];

    protected $table = 'inventory_return_detail';

    public function header() {
    	return $this->belongsTo('App\InventoryReturn', 'inventory_return_id');
    }

    public function inventory_detail() {
        return $this->belongsTo('App\InventoryDetail', 'inventory_detail_id');
    }
}
