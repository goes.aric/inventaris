<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenancePart extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'maintenance_record_id', 'part_name', 'part_number', 'serial_number', 'price',
    ];

    protected $table = 'maintenance_parts';

    public function maintenance() {
    	return $this->belongsTo('App\MaintenanceRecord', 'maintenance_record_id');
    }
}
