<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryProcurement extends Model
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'procurement_number', 'procurement_date', 'decree_number', 'reff_number', 'origin', 'procurement_file', 'information', 'total_item', 'total', 'status', 'user_id',
    ];

    protected $table = 'inv_procurement';

    public function detail() {
        return $this->hasMany('App\InventoryProcurementDetail');
    }    

    public function user() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
