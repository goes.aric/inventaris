<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Users;

class UsersRoleGroups extends Model
{
	use Notifiable;
	
    protected $fillable = [
    	'user_id', 'users_group_id',
    ];

    /* Related with table name */
    public $table = 'users_role_groups';

    /* Enable or Disable timestamps */
    public $timestamps = true; 
}
