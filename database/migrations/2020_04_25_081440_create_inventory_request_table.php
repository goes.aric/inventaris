<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_request', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_number', 150);
            $table->datetime('request_date');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('department')->onDelete('cascade');
            $table->string('requested_by', 255);
            $table->integer('reason');
            $table->text('description')->nullable();
            $table->string('request_file', 255);            
            $table->string('approved_by', 255)->nullable();
            $table->datetime('approved_date')->nullable();
            $table->integer('total_item')->default(0);
            $table->integer('status');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_request');
    }
}
