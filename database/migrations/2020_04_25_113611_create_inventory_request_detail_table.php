<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryRequestDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_request_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_request_id')->unsigned();
            $table->foreign('inventory_request_id')->references('id')->on('inventory_request')->onDelete('cascade');
            $table->integer('inventory_id')->unsigned();
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade');
            $table->string('item_code', 150);
            $table->string('item_name', 255);
            $table->decimal('qty', 10, 2);
            $table->string('unit', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_request_detail');
    }
}
