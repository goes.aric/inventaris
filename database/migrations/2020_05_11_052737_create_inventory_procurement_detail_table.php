<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryProcurementDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_procurement_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inv_procurement_id')->unsigned();
            $table->foreign('inv_procurement_id')->references('id')->on('inv_procurement')->onDelete('cascade');
            $table->integer('inventory_id')->unsigned();
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade');
            $table->string('item_code', 150);
            $table->string('item_name', 255);
            $table->decimal('qty', 10, 2);
            $table->string('unit', 150);
            $table->decimal('price', 10, 2);
            $table->decimal('sub_total', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_procurement_detail');
    }
}
