<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_detail_id')->unsigned();
            $table->foreign('inventory_detail_id')->references('id')->on('inventory_detail')->onDelete('cascade');
            $table->date('date');
            $table->string('reff_number', 150);
            $table->text('problem');
            $table->text('work_description');
            $table->decimal('cost', 15, 2)->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenance_records');
    }
}
