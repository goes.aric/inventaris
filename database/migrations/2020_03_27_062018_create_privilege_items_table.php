<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivilegeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privilege_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('privilege_item_name', 255);
            $table->string('key', 255);
            $table->string('group_key', 255);
            $table->string('master_group', 255);
            $table->string('index_url', 255)->nullable();
            $table->integer('users_group_id')->unsigned();
            $table->foreign('users_group_id')->references('id')->on('users_group')->onDelete('cascade');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privilege_items');
    }
}
