<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryProcurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_procurement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('procurement_number', 150);
            $table->datetime('procurement_date');
            $table->string('decree_number', 150);
            $table->string('reff_number', 255);
            $table->string('origin', 255);
            $table->string('procurement_file', 255);
            $table->text('information')->nullable();
            $table->integer('total_item')->default(0);
            $table->decimal('total', 15,2)->default(0);
            $table->integer('status');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_procurement');
    }
}
