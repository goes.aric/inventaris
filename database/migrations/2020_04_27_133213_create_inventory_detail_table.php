<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id')->unsigned();
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade');
            $table->string('item_code', 150);
            $table->string('detail_code', 150);
            $table->string('item_name', 255);
            $table->string('identification_number', 255)->nullable();
            $table->string('decree_number', 150);
            $table->string('request_number', 150)->nullable();
            $table->string('origin', 255);
            $table->string('acquisition_year', 6)->nullable();
            $table->integer('condition');
            $table->decimal('price', 10, 2);
            $table->text('information')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_detail');
    }
}
