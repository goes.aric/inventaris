<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryReturnDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_return_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_return_id')->unsigned();
            $table->foreign('inventory_return_id')->references('id')->on('inventory_return')->onDelete('cascade');
            $table->integer('inventory_detail_id')->unsigned();
            $table->foreign('inventory_detail_id')->references('id')->on('inventory_detail')->onDelete('cascade');            
            $table->string('item_code', 150);
            $table->string('detail_code', 150);
            $table->string('item_name', 255);
            $table->string('identification_number', 255)->nullable();
            $table->string('decree_number', 150);
            $table->string('request_number', 150)->nullable();   
            $table->string('origin', 255);
            $table->string('acquisition_year', 6);
            $table->integer('condition');
            $table->decimal('qty', 10, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_return_detail');
    }
}
