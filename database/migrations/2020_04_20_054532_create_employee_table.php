<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_number', 150);
            $table->string('employee_name', 255);
            $table->string('payroll_number', 150);
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('department')->onDelete('cascade');
            $table->string('group', 255);
            $table->string('position', 255);
            $table->decimal('salary', 10, 2);
            $table->boolean('employee_status');
            $table->string('level_of_education', 255);
            $table->string('majoring_in_education', 255);
            $table->string('other', 255)->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_active');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
