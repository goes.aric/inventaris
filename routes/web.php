<?php

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
	Route::prefix('/')->group(function(){
		//Login
		Route::get('/login', 'Auth\LoginController@index')->name('login');
		Route::post('/login', 'Auth\LoginController@login')->name('login.attempt');

		//Register
		Route::get('/register', 'Auth\RegisterController@index')->name('register');
		Route::post('/register', 'Auth\RegisterController@store')->name('register.store');	

		Route::middleware(['auth'])->group(function(){
			//Dashboard	
			Route::get('/', 'DashboardController@index')->name('dashboard');
			Route::get('dashboard', 'DashboardController@index')->name('dashboard');

			//Category
			Route::get('/category/json', 'Category\CategoryController@populateRecords')->name('category.getCategoryData');
			Route::delete('/category', 'Category\CategoryController@delete')->name('category.delete');		
			Route::resource('category', 'Category\CategoryController');

			//Inventory
			Route::get('/inventory/detail/json/{id}', 'Inventory\InventoryController@populateDetailRecords')->name('inventory.getInventoryDetailData');
			Route::get('/inventory/user/json/{id}', 'Inventory\InventoryController@inventoryUserDetails')->name('inventory.inventoryUserDetails');
			Route::get('/inventory/maintenance/json/{id}', 'Inventory\InventoryController@populateMaintenanceRecords')->name('inventory.maintenanceRecords');
			Route::get('/inventory/maintenance/work/json/{id}', 'Inventory\InventoryController@populateMaintenanceWorkRecords')->name('inventory.maintenanceWorkRecords');
			Route::get('/inventory/maintenance/part/json/{id}', 'Inventory\InventoryController@populateMaintenancePartRecords')->name('inventory.maintenancePartRecords');
			Route::get('/inventory/json', 'Inventory\InventoryController@populateRecords')->name('inventory.getInventoryData');
			Route::delete('/inventory', 'Inventory\InventoryController@delete')->name('inventory.delete');		
			Route::resource('inventory', 'Inventory\InventoryController');			

			//Department
			Route::get('/department/json', 'Department\DepartmentController@populateRecords')->name('department.getDepartmentData');
			Route::delete('/department', 'Department\DepartmentController@delete')->name('department.delete');		
			Route::resource('department', 'Department\DepartmentController');

			//Employee
			Route::get('/employee/json', 'Employee\EmployeeController@populateRecords')->name('employee.getEmployeeData');
			Route::delete('/employee', 'Employee\EmployeeController@delete')->name('employee.delete');		
			Route::resource('employee', 'Employee\EmployeeController');

			//Inventory Request
			Route::get('/request/json', 'InventoryRequest\InventoryRequestController@populateRecords')->name('request.getInventoryRequestData');
			Route::get('/request/detail/json/{id}', 'InventoryRequest\InventoryRequestController@populateDetailRecords')->name('request.getInventoryRequestDetailData');
			Route::delete('/request', 'InventoryRequest\InventoryRequestController@delete')->name('request.delete');		
			Route::resource('request', 'InventoryRequest\InventoryRequestController');

			//Inventory Request Detail
			Route::get('/request/item/json', 'InventoryRequest\InventoryRequestController@populateItem')->name('request.populateItem');
			Route::get('/request/item/{id}', 'InventoryRequest\InventoryRequestController@getDetailItem')->name('request.autoFillItem');
			Route::post('/request/item', 'InventoryRequest\InventoryRequestController@storeRequestDetail')->name('request.storeRequestDetail');
			Route::delete('/request/item/{item}', 'InventoryRequest\InventoryRequestController@requestDetailDestroy')->name('request.requestDetailDestroy');

			//Inventory Procurement
			Route::get('/procurement/json', 'InventoryProcurement\InventoryProcurementController@populateRecords')->name('procurement.getInventoryProcurementData');
			Route::get('/procurement/detail/json/{id}', 'InventoryProcurement\InventoryProcurementController@populateDetailRecords')->name('procurement.getInventoryProcurementDetailData');
			Route::delete('/procurement', 'InventoryProcurement\InventoryProcurementController@delete')->name('procurement.delete');
			Route::resource('procurement', 'InventoryProcurement\InventoryProcurementController');

			//Inventory Procurement Detail
			Route::get('/generate', 'InventoryProcurement\InventoryProcurementController@autoCode')->name('procurement.autoCode');
			Route::get('/procurement/item/json', 'InventoryProcurement\InventoryProcurementController@populateItem')->name('procurement.populateItem');
			Route::get('/procurement/item/{id}', 'InventoryProcurement\InventoryProcurementController@getDetailItem')->name('procurement.autoFillItem');
			Route::post('/procurement/item', 'InventoryProcurement\InventoryProcurementController@storeProcurementDetail')->name('procurement.storeProcurementDetail');
			Route::get('/procurement/show/detail/{id}', 'InventoryProcurement\InventoryProcurementController@getProcurementDetailItem')->name('procurement.getProcurementDetailItem');
			Route::get('/procurement/show/subdetail/{id}', 'InventoryProcurement\InventoryProcurementController@getSubDetailItem')->name('procurement.getSubDetailItem');
			Route::delete('/procurement/item/{item}', 'InventoryProcurement\InventoryProcurementController@procurementDetailDestroy')->name('procurement.procurementDetailDestroy');

			//Inventory Delivery
			Route::get('/delivery/request/{id}', 'InventoryDelivery\InventoryDeliveryController@populateInventoryRequest')->name('delivery.getInventoryRequest');
			Route::get('/delivery/employee/{id}', 'InventoryDelivery\InventoryDeliveryController@populateEmployee')->name('delivery.getEmployee');
			Route::get('/delivery/json', 'InventoryDelivery\InventoryDeliveryController@populateRecords')->name('delivery.getInventoryDeliveryData');
			Route::get('/delivery/detail/json/{id}', 'InventoryDelivery\InventoryDeliveryController@populateDetailRecords')->name('delivery.getInventoryDeliveryDetailData');
			Route::delete('/delivery', 'InventoryDelivery\InventoryDeliveryController@delete')->name('delivery.delete');		
			Route::resource('delivery', 'InventoryDelivery\InventoryDeliveryController');

			//Inventory Delivery Detail
			Route::get('/delivery/item/json', 'InventoryDelivery\InventoryDeliveryController@populateItem')->name('delivery.populateItem');
			Route::get('/delivery/item/{id}', 'InventoryDelivery\InventoryDeliveryController@getDetailItem')->name('delivery.autoFillItem');
			Route::post('/delivery/item', 'InventoryDelivery\InventoryDeliveryController@storeDeliveryDetail')->name('delivery.storeDeliveryDetail');
			Route::delete('/delivery/item/{item}', 'InventoryDelivery\InventoryDeliveryController@deliveryDetailDestroy')->name('delivery.deliveryDetailDestroy');

			//Inventory Return
			Route::get('/return/delivery/{id}', 'InventoryReturn\InventoryReturnController@populateInventoryDelivery')->name('return.getInventoryDelivery');
			Route::get('/return/employee/{id}', 'InventoryReturn\InventoryReturnController@populateEmployee')->name('return.getEmployee');
			Route::get('/return/json', 'InventoryReturn\InventoryReturnController@populateRecords')->name('return.getInventoryReturnData');
			Route::get('/return/detail/json/{id}', 'InventoryReturn\InventoryReturnController@populateDetailRecords')->name('return.getInventoryReturnDetailData');
			Route::delete('/return', 'InventoryReturn\InventoryReturnController@delete')->name('return.delete');		
			Route::resource('return', 'InventoryReturn\InventoryReturnController');

			//Inventory Return Detail
			Route::get('/return/item/json', 'InventoryReturn\InventoryReturnController@populateItem')->name('return.populateItem');
			Route::get('/return/item/{id}', 'InventoryReturn\InventoryReturnController@getDetailItem')->name('return.autoFillItem');
			Route::post('/return/item', 'InventoryReturn\InventoryReturnController@storeReturnDetail')->name('return.storeReturnDetail');
			Route::delete('/return/item/{item}', 'InventoryReturn\InventoryReturnController@returnDetailDestroy')->name('return.returnDetailDestroy');						

			//Users Group
			Route::get('/users-group/permissions/{id}', 'UsersGroup\UsersGroupController@getUserPermissions')->name('users-group.getUserPermissions');
			Route::get('/users-group/json', 'UsersGroup\UsersGroupController@populateRecords')->name('users-group.getUsersGroupData');
			Route::delete('/users-group', 'UsersGroup\UsersGroupController@delete')->name('users-group.delete');		
			Route::resource('users-group', 'UsersGroup\UsersGroupController');

			//Users
			Route::get('/users/groups/{id}', 'Users\UsersController@getUserGroups')->name('users.getUserGroups');
			Route::get('/users/json', 'Users\UsersController@populateRecords')->name('users.getUserData');
			Route::get('/users/password', 'Users\UsersController@password')->name('users.password');
			Route::put('/users/password', 'Users\UsersController@update_password')->name('users.password.update');
			Route::get('/profile', 'Users\UsersController@profile')->name('users.profile');			
			Route::put('/users/password/{id}', 'Users\UsersController@resetPassword')->name('users.reset');
			Route::delete('/users', 'Users\UsersController@delete')->name('users.delete');				
			Route::resource('users', 'Users\UsersController');

			//Logout
			Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
		});	
	});
});
