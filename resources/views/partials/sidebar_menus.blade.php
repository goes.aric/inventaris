<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <h4 alt="Timor Leste" class="c-sidebar-brand-full" height="46" width="118">
            <img class="mt-1 ml-2" height="45" width="45" src="{{ asset('css/images/tl-education.png') }}">
            Education Ministry
        </h4>
        <span alt="Timor Leste" class="c-sidebar-brand-minimized" height="46" width="46">
            <img height="46" width="46" src="{{ asset('css/images/tl-education.png') }}">
        </span>
    </div>
    <ul class="c-sidebar-nav" data-drodpown-accordion="true">
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('dashboard') }}">
                <i class="c-sidebar-nav-icon cil-speedometer"></i> {{ __('Dashboard') }}
            </a>
        </li>
        <li class="c-sidebar-nav-title">
            {{ __('Master') }}
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('category.index') }}">
                <i class="c-sidebar-nav-icon cil-sitemap"></i> {{ __('Categories') }}
            </a>
        </li>        
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('inventory.index') }}">
                <i class="c-sidebar-nav-icon cil-3d"></i> {{ __('Inventory') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('department.index') }}">
                <i class="c-sidebar-nav-icon cil-puzzle"></i> {{ __('Department') }}
            </a>
        </li>                
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('employee.index') }}">
                <i class="c-sidebar-nav-icon cil-address-book"></i> {{ __('Employee') }}
            </a>
        </li>              
        <li class="c-sidebar-nav-title">
            {{ __('Inventory Flow') }}
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('request.index') }}">
                <i class="c-sidebar-nav-icon cil-envelope-closed"></i> {{ __('Inventory Request') }}
            </a>
        </li>        
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('procurement.index') }}">
                <i class="c-sidebar-nav-icon cil-copy"></i> {{ __('Inventory Procurement') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('delivery.index') }}">
                <i class="c-sidebar-nav-icon cil-truck"></i> {{ __('Delivery of Inventory') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('return.index') }}">
                <i class="c-sidebar-nav-icon cil-book"></i> {{ __('Inventory Return') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="#">
                <i class="c-sidebar-nav-icon cil-clipboard"></i> {{ __('Lost or Damaged Inventory') }}
            </a>
        </li>                
        <li class="c-sidebar-nav-title">
            {{ __('Reports') }}
        </li>
        <li class="c-sidebar-nav-dropdown">
            <a class="c-sidebar-nav-dropdown-toggle" href="#">
                <i class="c-sidebar-nav-icon cil-print"></i> Procurement Report
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="login.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-account-logout">
                            </use>
                        </svg>
                        Login
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="register.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-account-logout">
                            </use>
                        </svg>
                        Register
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="404.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug">
                            </use>
                        </svg>
                        Error 404
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="500.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug">
                            </use>
                        </svg>
                        Error 500
                    </a>
                </li>
            </ul>
        </li>
        <li class="c-sidebar-nav-dropdown">
            <a class="c-sidebar-nav-dropdown-toggle" href="#">
                <i class="c-sidebar-nav-icon cil-chart-line"></i> Inventory Movement
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="500.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug">
                            </use>
                        </svg>
                        Error 500
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="500.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug">
                            </use>
                        </svg>
                        Error 500
                    </a>
                </li>                
            </ul>
        </li>
        <li class="c-sidebar-nav-dropdown">
            <a class="c-sidebar-nav-dropdown-toggle" href="#">
                <i class="c-sidebar-nav-icon cil-leaf"></i> Others
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="500.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug">
                            </use>
                        </svg>
                        Error 500
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="500.html" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-bug">
                            </use>
                        </svg>
                        Error 500
                    </a>
                </li>                
            </ul>
        </li>
        <li class="c-sidebar-nav-title">
            {{ __('Settings') }}
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('users-group.index') }}">
                <i class="c-sidebar-nav-icon cil-people"></i> {{ __('Users Group') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('users.index') }}">
                <i class="c-sidebar-nav-icon cil-contact"></i> {{ __('Users') }}
            </a>
        </li>
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" data-class="c-sidebar-unfoldable" data-target="_parent" type="button">
    </button>
</div>