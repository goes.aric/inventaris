<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" data-class="c-sidebar-show" data-target="#sidebar" type="button">
     <i class="cil-hamburger-menu"></i> 
</button>
<a class="c-header-brand d-lg-none" href="#">
    <img height="46" width="46" src="{{ asset('css/images/tl-education.png') }}">
</a>
<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" data-class="c-sidebar-lg-show" data-target="#sidebar" responsive="true" type="button">
     <i class="cil-hamburger-menu"></i> 
</button>
<ul class="c-header-nav d-md-down-none">
    <li class="c-header-nav-item px-3">
        <a class="c-header-nav-link" href="{{ route('dashboard') }}">
            {{ __('Dashboard') }}
        </a>
    </li>
    <li class="c-header-nav-item px-3">
        <a class="c-header-nav-link" href="{{ route('users.index') }}">
            {{ __('Users') }}
        </a>
    </li>
    <li class="c-header-nav-item px-3">
        <a class="c-header-nav-link" href="{{ route('users-group.index') }}">
            {{ __('Users Group') }}
        </a>
    </li>
</ul>
<ul class="c-header-nav mfs-auto">
    <li class="c-header-nav-item dropdown d-md-down-none mx-2">
        <a aria-expanded="false" aria-haspopup="true" class="c-header-nav-link" data-toggle="dropdown" href="#" role="button">
            <i class="cil-bell"></i>
            <span class="badge badge-pill badge-danger">
                5
            </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
            <div class="dropdown-header bg-light">
                <strong>
                    You have 5 notifications
                </strong>
            </div>
            <a class="dropdown-item" href="#">
                <svg class="c-icon mfe-2 text-success">
                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-user-follow">
                    </use>
                </svg>
                New user registered
            </a>
            <a class="dropdown-item" href="#">
                <svg class="c-icon mfe-2 text-danger">
                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-user-unfollow">
                    </use>
                </svg>
                User deleted
            </a>
            <a class="dropdown-item" href="#">
                <svg class="c-icon mfe-2 text-info">
                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-chart">
                    </use>
                </svg>
                Sales report is ready
            </a>
            <a class="dropdown-item" href="#">
                <svg class="c-icon mfe-2 text-success">
                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-basket">
                    </use>
                </svg>
                New client
            </a>
            <a class="dropdown-item" href="#">
                <svg class="c-icon mfe-2 text-warning">
                    <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-speedometer">
                    </use>
                </svg>
                Server overloaded
            </a>
        </div>
    </li>
    <li class="c-header-nav-item dropdown d-md-down-none mx-2">
        <a aria-expanded="false" aria-haspopup="true" class="c-header-nav-link" data-toggle="dropdown" href="#" role="button">
            <i class="cil-list-rich"></i>
            <span class="badge badge-pill badge-warning">
                15
            </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
            <div class="dropdown-header bg-light">
                <strong>
                    You have 5 pending tasks
                </strong>
            </div>
            <a class="dropdown-item d-block" href="#">
                <div class="small mb-1">
                    Upgrade NPM & Bower
                    <span class="float-right">
                        <strong>
                            0%
                        </strong>
                    </span>
                </div>
                <span class="progress progress-xs">
                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" class="progress-bar bg-info" role="progressbar" style="width: 0%">
                    </div>
                </span>
            </a>
            <a class="dropdown-item d-block" href="#">
                <div class="small mb-1">
                    ReactJS Version
                    <span class="float-right">
                        <strong>
                            25%
                        </strong>
                    </span>
                </div>
                <span class="progress progress-xs">
                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" class="progress-bar bg-danger" role="progressbar" style="width: 25%">
                    </div>
                </span>
            </a>
            <a class="dropdown-item d-block" href="#">
                <div class="small mb-1">
                    VueJS Version
                    <span class="float-right">
                        <strong>
                            50%
                        </strong>
                    </span>
                </div>
                <span class="progress progress-xs">
                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" class="progress-bar bg-warning" role="progressbar" style="width: 50%">
                    </div>
                </span>
            </a>
            <a class="dropdown-item d-block" href="#">
                <div class="small mb-1">
                    Add new layouts
                    <span class="float-right">
                        <strong>
                            75%
                        </strong>
                    </span>
                </div>
                <span class="progress progress-xs">
                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" class="progress-bar bg-info" role="progressbar" style="width: 75%">
                    </div>
                </span>
            </a>
            <a class="dropdown-item d-block" href="#">
                <div class="small mb-1">
                    Angular 8 Version
                    <span class="float-right">
                        <strong>
                            100%
                        </strong>
                    </span>
                </div>
                <span class="progress progress-xs">
                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" class="progress-bar bg-success" role="progressbar" style="width: 100%">
                    </div>
                </span>
            </a>
            <a class="dropdown-item text-center border-top" href="#">
                <strong>
                    View all tasks
                </strong>
            </a>
        </div>
    </li>
    <li class="c-header-nav-item dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="c-header-nav-link mfe-md-3" data-toggle="dropdown" href="#" role="button">
            <div class="c-avatar">
                <img alt="user@email.com" class="c-avatar-img" src="{{ asset('assets/img/avatars/user-circle.png') }}"/>
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0 mfe-md-3">
            <div class="dropdown-header bg-light py-2">
                <strong>
                    {{ __('Account') }}
                </strong>
            </div>
            <a class="dropdown-item" href="#">
                <span class="c-icon mfe-2">
                    <i class="cil-bell"></i>
                </span>
                Updates
                <span class="badge badge-info mfs-auto">
                    42
                </span>
            </a>
            <a class="dropdown-item" href="#">
                <span class="c-icon mfe-2">
                    <i class="cil-task"></i>
                </span>                    
                Tasks
                <span class="badge badge-danger mfs-auto">
                    42
                </span>
            </a>
            <div class="dropdown-header bg-light py-2">
                <strong>
                    {{ __('Settings') }}
                </strong>
            </div>
            <a class="dropdown-item" href="{{ route('users.profile') }}">
                <span class="c-icon mfe-2">
                    <i class="cil-user"></i>
                </span>                    
                {{ __('Profile') }}
            </a>
            <a class="dropdown-item" href="{{ route('users.password') }}">
                <span class="c-icon mfe-2">
                    <i class="cil-fingerprint"></i>
                </span>                    
                {{ __('Change Password') }}
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}">
                <span class="c-icon mfe-2">
                    <i class="cil-account-logout"></i>
                </span>                    
                {{ __('Logout') }}
            </a>
        </div>
    </li>
</ul>