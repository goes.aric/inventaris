<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('css/images/favicon.ico') }}" type="image/x-icon" />
    <!-- CSFR token for ajax call -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <title>@yield('title')</title>
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')"/>
    <meta name="copyright" content="@yield('copyright')"/>
    <meta name="author" content="@yield('author')"/>
    <meta name="Distribution" content="Global"/>
    <meta name="Rating" content="General"/>
    <meta name="robots" content="index,follow"/>        
    <!-- Styles --> 
    <link rel="stylesheet" type="text/css" href="{{ asset('css/coreui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/pace-progress/css/pace.custom.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/@coreui/icons/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.arikbali.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-select/dist/css/bootstrap-select4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">   
    <link rel="stylesheet" type="text/css" href="{{ asset('css/additional.css') }}">
    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>       
  </head>
  <body class="c-app c-legacy-theme">
    <!-- notification -->
    @include('pages/notification')    

    @include('partials/sidebar_menus')

    <div class="c-wrapper">
        @section('content')
        @show
        
        @include('partials/footer')
    </div>    

    <!-- Popper -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Momment JS -->
    <script type="text/javascript" src="{{ asset('bower_components/moment/moment.js') }}"></script>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('js/coreui.min.js') }}"></script>
    <!-- Pace Progress plugins-->
    <script src="{{ asset('js/pace.min.js') }}"></script>
    <!-- Date Picker -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>            
    <!-- Datatables -->
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- Bootstrap Select 1.12 -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('.selectpicker').selectpicker();
        });
    </script>
  </body>
</html>