<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('css/images/favicon.ico') }}" type="image/x-icon" />
    <title>@yield('title')</title>
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')"/>
    <meta name="copyright" content="@yield('copyright')"/>
    <meta name="author" content="@yield('author')"/>
    <meta name="Distribution" content="Global"/>
    <meta name="Rating" content="General"/>
    <meta name="robots" content="index,follow"/>        
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/coreui.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/pace-progress/css/pace.custom.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/@coreui/icons/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/additional.css') }}">       
  </head>
  <body class="c-app flex-row align-items-center">
    <!-- notification -->
    @include('pages/notification')      

    @section('content')
    @show

    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('js/coreui.min.js') }}"></script>
    <!-- Pace Progress plugins-->
    <script src="{{ asset('js/pace.min.js') }}"></script>    
  </body>
</html>