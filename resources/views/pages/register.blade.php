@extends('layouts.login')

@section('title', __('Register User'))
@section('meta_description', __('Register User Page'))
@section('meta_keywords', __('Register'))
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mx-4">
                <form action="{{ route('register.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="card-body p-4">
                        <h1>
                            {{ __('Register') }}
                        </h1>
                        <p class="text-muted">
                            {{ __('Create your account') }}
                        </p>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="cil-user-follow">
                                    </i>
                                </span>
                            </div>
                            <input class="form-control" type="text" placeholder="{{ __('Full Name') }}" id="name" name="name" value="{{ old('name') }}"/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="cil-user">
                                    </i>
                                </span>
                            </div>
                            <input class="form-control" type="text" placeholder="{{ __('Username') }}" id="username" name="username" value="{{ old('username') }}"/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="cil-envelope-open">
                                    </i>
                                </span>
                            </div>
                            <input class="form-control" type="text" placeholder="{{ __('Email') }}" id="email" name="email" value="{{ old('email') }}"/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="cil-lock-locked">
                                    </i>
                                </span>
                            </div>
                            <input class="form-control" type="password" placeholder="{{ __('Password') }}" id="password" name="password"/>
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="cil-lock-locked">
                                    </i>
                                </span>
                            </div>
                            <input class="form-control" type="password" placeholder="{{ __('Repeat password') }}" id="password_confirmation" name="password_confirmation"/>
                        </div>
                        <button class="btn btn-block btn-success" type="submit">
                            {{ __('Create Account') }}
                        </button>
                        <a class="btn btn-block btn-primary" href="{{ route('login') }}" alt="{{ __('Sign In') }}"  title="{{ __('Sign In') }}">
                            {{ __('Sign In') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
