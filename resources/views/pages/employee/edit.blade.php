@extends('layouts.app')

@section('title', __('Update Employee'))
@section('meta_description', __('Employee Page'))
@section('meta_keywords', __('Employee'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('employee.index') }}" alt="{{ __('Employee') }}" title="{{ __('Employee List') }}">
                    {{ __('Employee') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Update') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Employee') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Update Employee') }}
                                </div>
                            </div>
                        </div><hr>                        
                        <form id="myForm" novalidate="novalidate" method="POST" action="{{ route('employee.update', $employee->id) }}">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="employee_number">{{ __('Employee Number') }}</label>
                                    <input class="form-control" id="employee_number" type="text" name="employee_number" placeholder="{{ __('Employee Number') }}" value="{{ $employee->employee_number }}">
                                </div>                         
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="employee_name">{{ __('Full Name') }}</label>
                                    <input class="form-control" id="employee_name" type="text" name="employee_name" placeholder="{{ __('Full Name') }}" value="{{ $employee->employee_name }}">
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="payroll_number">{{ __('Payroll Number') }}</label>
                                    <input class="form-control" id="payroll_number" type="text" name="payroll_number" placeholder="{{ __('Payroll Number') }}" value="{{ $employee->payroll_number }}">
                                </div>                         
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="department">{{ __('Department') }}</label>
                                    <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                        @foreach ($department as $item)
                                            <option value="{{ $item->id }}" {{ $employee->department_id == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                        @endforeach
                                    </select>
                                </div>                                 
                            </div>                                                        
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="group">{{ __('Group') }}</label>
                                    <input class="form-control" id="group" type="text" name="group" placeholder="{{ __('Group') }}" value="{{ $employee->group }}">
                                </div>                         
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="position">{{ __('Position') }}</label>
                                    <input class="form-control" id="position" type="text" name="position" placeholder="{{ __('Position') }}" value="{{ $employee->position }}">
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="salary">{{ __('Salary') }}</label>
                                    <input class="form-control" id="salary" type="text" name="salary" placeholder="{{ __('Salary') }}" value="{{ $employee->salary }}">
                                </div>                         
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="employee_status">{{ __('Employee Status') }}</label>
                                    <select id="employee_status" name="employee_status" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" data-live-search="true" single>
                                        <option value="0" {{ $employee->employee_status == '0' ? 'selected' : '' }}>{{ __('Permanent') }}</option>
                                        <option value="1" {{ $employee->employee_status == '1' ? 'selected' : '' }}>{{ __('Honorarium') }}</option>
                                    </select>
                                </div>                                
                            </div>                            
                            <div class="form-group">
                                <label class="col-form-label" for="level_of_education">{{ __('Level of Education') }}</label>
                                <input class="form-control" id="level_of_education" type="text" name="level_of_education" placeholder="{{ __('Level of Education') }}" value="{{ $employee->level_of_education }}">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="majoring_in_education">{{ __('Majoring in Education') }}</label>
                                <input class="form-control" id="majoring_in_education" type="text" name="majoring_in_education" placeholder="{{ __('Majoring in Education') }}" value="{{ $employee->majoring_in_education }}">
                            </div> 
                            <div class="form-group">
                                <label class="col-form-label" for="other">{{ __('Other Note') }}</label>
                                <input class="form-control" id="other" type="text" name="other" placeholder="{{ __('Other Note') }}" value="{{ $employee->other }}">
                            </div>                                                        
                            <div class="form-group">
                                <label class="col-form-label" for="description">{{ __('Description') }}</label>
                                <textarea class="form-control" id="description" type="description" name="description" placeholder="{{ __('Description') }}" rows="5">{{ $employee->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="is_active">{{ __('Status') }}</label>
                                <div class="form-check">
                                    <input class="form-check-input" id="active" type="radio" value="1" name="is_active" {{ $employee->is_active == '1' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active">{{ __('Active') }}</label>
                                </div>                                  
                                <div class="form-check">
                                    <input class="form-check-input" id="inactive" type="radio" value="0" name="is_active" {{ $employee->is_active == '0' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inactive">{{ __('Inactive') }}</label>
                                </div>  
                            </div>    
                            <hr>                                
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Update & Close') }}" title="{{ __('Update & Close') }}">
                                    <i class="cil-save"></i> {{ __('Update & Close') }}
                                </button>
                                <a href="{{ route('employee.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
                                    <i class="icon-action-undo"></i> {{ __('Cancel') }}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection