@extends('layouts.app')

@section('title', __('Update Inventory Delivery'))
@section('meta_description', __('Inventory Delivery Page'))
@section('meta_keywords', __('Inventory Delivery'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('delivery.index') }}" alt="{{ __('Inventory Delivery') }}" title="{{ __('Inventory Delivery List') }}">
                    {{ __('Inventory Delivery') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Update') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Delivery') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Update Inventory Delivery') }}
                                </div>
                            </div>                           
                        </div><hr>                        
                        <form id="myForm" novalidate="novalidate" method="POST" action="{{ route('delivery.update', $delivery->id) }}" enctype="multipart/form-data">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <!-- Primary Field -->
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="delivery_number">{{ __('Delivery Number') }}</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="delivery_number" type="text" name="delivery_number" placeholder="{{ __('Delivery Number') }}" value="{{ $delivery->delivery_number }}" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="delivery_date">{{ __('Delivery Date') }}</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input class="form-control no-border-right" id="delivery_date" type="text" name="delivery_date" placeholder="{{ __('Delivery Date') }}" value="{{ $delivery->delivery_date }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text bg-transparent">
                                                        <i class="cil-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="department">{{ __('Department') }}</label>
                                        <div class="col-md-8">
                                            <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                                @foreach ($department as $item)
                                                    <option value="{{ $item->id }}" {{ $delivery->department_id == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="employee">{{ __('Received By') }}</label>
                                        <div class="col-md-8">
                                            <select id="employee" name="employee" class="form-control selectpicker show-tick" title="{{ __('Select Employee') }}" data-live-search="true" single>
                                                <option value="{{ $delivery->employee_id }}" selected="selected">{{ $delivery->employee->employee_number }} - {{ $delivery->employee->employee_name }}</option>
                                            </select>
                                        </div>
                                    </div>                                                                                     
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="inventory_request">{{ __('Inventory Request') }}</label>
                                        <div class="col-md-8">
                                            <select id="inventory_request" name="inventory_request" class="form-control selectpicker show-tick" title="{{ __('Select Inventory Request') }}" data-live-search="true" single>
                                                <option value="{{ $delivery->inventory_request_id }}" selected="selected">{{ $delivery->request->request_number }} - {{ $delivery->request->requested_by }}</option>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="status">{{ __('') }}Status</label>
                                        <div class="col-md-8">
                                            <select id="status" name="status" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" single>
                                                <option value="1" {{ $delivery->status == '1' ? 'selected' : '' }}>{{ __('Pending') }}</option>
                                                <option value="2" {{ $delivery->status == '2' ? 'selected' : '' }}>{{ __('Valid') }}</option>
                                            </select>
                                        </div>
                                    </div>              
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <h4 class="card-title mb-0">
                                        {{ __('Items') }}
                                    </h4>
                                    <div class="small text-muted">
                                        {{ __('The items of delivery') }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                    @if ($delivery->status == '1')
                                        <button type="button" id="btnAddItem" name="btnAddItem" alt="{{ __('Add Item') }}" class="btn btn-success float-right" title="{{ __('Add Item') }}">
                                            <i class="icon-plus">
                                            </i>
                                            <span class="d-none d-sm-inline">
                                                {{ __('Add Item') }}
                                            </span>
                                        </button>
                                    @endif                                
                                </div>                                                               
                            </div><hr>
                            <div class="form-group">                               
                                <div id="table" class="table-responsive">
                                    <table cellspacing="0" class="table table-bordered datatable dataTable" id="item-table" style="width: 100%" width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    {{ __('Item Code') }}
                                                </th>
                                                <th>
                                                    {{ __('Detail Code') }}
                                                </th>    
                                                <th>
                                                    {{ __('Item Name') }}
                                                </th>
                                                <th>
                                                    {{ __('Identification Number') }}
                                                </th>
                                                <th>
                                                    {{ __('Unit') }}
                                                </th> 
                                                <th>
                                                    {{ __('Qty') }}
                                                </th>                                                
                                                <th class="text-center">
                                                    {{ __('Action') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="note" name="note" placeholder="{{ __('Note') }}" rows="5">{{ $delivery->note }}</textarea>
                                    </div>                                      
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">    
                                        <div class="col-md-6">
                                            <label class="col-form-label float-right" for="total_item">{{ __('Total Items') }}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control text-right" type="text" id="total_item" name="total_item" placeholder="{{ __('Total Items') }}" value="{{ $delivery->total_item }}" readonly="readonly">
                                        </div>      
                                    </div>                                      
                                </div>
                            </div>    
                            <hr>                                
                            <div class="form-group">
                                @if ($delivery->status == '1')
                                    <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Update & Close') }}" title="{{ __('Update & Close') }}">
                                        <i class="cil-save"></i> {{ __('Update & Close') }}
                                    </button>
                                @endif
                                <a href="{{ route('delivery.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
                                    <i class="icon-action-undo"></i> {{ __('Cancel') }}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Add Item -->
<div class="modal fade" id="itemDialog" tabindex="-1" role="dialog" aria-labelledby="itemDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemDialogLabel">{{ __('Add Item') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-9">
                        <label class="col-form-label" for="item_id">{{ __('Items') }}</label>
                        <select id="item_id" name="item_id" class="form-control selectpicker show-tick" data-live-search="true" title="{{ __('Select Items') }}">
                        </select>                    
                    </div>
                    <div class="form-group col-md-3">
                        <label class="col-form-label" for="request_qty">{{ __('Request Amount') }}</label>
                        <input class="form-control" id="request_qty" type="text" name="request_qty" placeholder="{{ __('Request Amount') }}" readonly="readonly">
                    </div>                                        
                </div>
                <h5><b>{{ __('Details') }}</b></h5>
                <p class="text-muted">{{ __('Here detail about item') }}</p>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="col-form-label" for="item_code">{{ __('Item Code') }}</label>
                        <input class="form-control" id="item_code" type="text" name="item_code" placeholder="{{ __('Item Code') }}" readonly="readonly">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="col-form-label" for="detail_code">{{ __('Detail Code') }}</label>
                        <input class="form-control" id="detail_code" type="text" name="detail_code" placeholder="{{ __('Detail Code') }}" readonly="readonly">
                    </div>                    
                    <div class="form-group col-md-6">
                        <label class="col-form-label" for="item_name">{{ __('Item Name') }}</label>
                        <input class="form-control" id="item_name" type="text" name="item_name" placeholder="{{ __('Item Name') }}" readonly="readonly">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="col-form-label" for="identification_number">{{ __('Identification Number') }}</label>
                        <input class="form-control" id="identification_number" type="text" name="identification_number" placeholder="{{ __('Identification Number') }}" readonly="readonly">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="col-form-label" for="unit">{{ __('Unit') }}</label>
                        <input class="form-control text-right" id="unit" type="text" name="unit" placeholder="{{ __('Unit') }}" readonly="readonly">
                    </div>                                         
                    <div class="form-group col-md-3">
                        <label class="col-form-label" for="qty">{{ __('Qty') }}</label>
                        <input class="form-control text-right" id="qty" type="number" name="qty" min="1">
                    </div>                   
                </div>                               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                <button type="button" class="btn btn-primary" id="btnInsertItem">{{ __('Insert') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        function populateSelect() {
            var id = $('#department option:selected').val();
            var requestUrl = '{{ route('delivery.getInventoryRequest', ':id') }}';
            requestUrl = requestUrl.replace(':id', id);
            var employeeUrl = '{{ route('delivery.getEmployee', ':id') }}';
            employeeUrl = employeeUrl.replace(':id', id);

            $.ajax({
                url: requestUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $("#inventory_request").find("option").remove();
                    var item = '';

                    for (var i = 0; i < data.length; i++) {
                        var date = new Date(data[i].request_date);
                        var tanggal = date.toLocaleDateString('fr-ca');

                        item += "<option value='"+data[i].id+"'>"+tanggal+" | "+data[i].request_number+" - "+data[i].requested_by+"</option>";
                    }

                    $("#inventory_request").append(item);
                    $('.selectpicker').selectpicker('refresh'); 
                }
            });

            $.ajax({
                url: employeeUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $("#employee").find("option").remove();
                    var item = '';

                    for (var i = 0; i < data.length; i++) {
                        item += "<option value='"+data[i].id+"'>"+data[i].employee_number+" - "+data[i].employee_name+"</option>";
                    }

                    $("#employee").append(item);
                    $('.selectpicker').selectpicker('refresh'); 
                }
            });
        }

        $('#delivery_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });           

        $('#item-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("delivery.getInventoryDeliveryDetailData", $delivery->id) }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [               
                {data: 'item_code', name: 'item_code'},
                {data: 'detail_code', name: 'detail_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'identification_number', name: 'identification_number'},
                {data: 'unit', name: 'unit'},
                {data: 'qty', name: 'qty'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 1, "asc"]]
        });

        $('#item-table').on('click', '.btn-delete[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("delivery.deliveryDetailDestroy", ":id") }}';
            url = url.replace(':id', id);

            // confirm then
            if (confirm('{{ __('Are you sure want to delete it ?') }}')) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: 'json',
                    data: {method: '_DELETE', submit: true,},
                    success: function(data){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };                      
                        if (data.type == 'error') {
                            $('#total_item').val(data.value);
                            toastr.error(data.msg);
                        }else {
                            $('#total_item').val(data.value);
                            toastr.success(data.msg);
                        }
                    }
                }).always(function (data) {
                    $('#item-table').DataTable().draw(false);
                });
            }else{
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.info('{{ __('You have canceled it!') }}');
            }
        });

        function clearItem(){
            $('#item_id').val('');
            $('#item_code').val('');
            $('#detail_code').val('');
            $('#item_name').val('');
            $('#identification_number').val('');
            $('#unit').val('');
            $('#qty').val('1');
            $('.selectpicker').selectpicker('refresh');         
        }

        function populateItem(){
            var myUrl = '{{ route('delivery.populateItem') }}';

            $.ajax({
                type: 'GET',
                url: myUrl,             
                data: {
                    'id'            : {{ $delivery->id }},
                    'inv_request'   : $('#inventory_request option:selected').val()
                },
                dataType: 'json',
                success: function(data){
                    $("#item_id").find("option").remove();

                    var data_item = data.item;
                    var item = '';
                    for (var i in data_item)
                    {
                        item        = item + "<option value='"+data_item[i].id+"'>"+data_item[i].detail_code+" - "+data_item[i].item_name+"</option>";
                    }

                    $("#item_id").append(item); 
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center"
                    };           

                    toastr.error(errorThrown);                      
                }
            });            
        }

        $('#btnAddItem').click(function(){
            clearItem();
            populateItem();
            $('#itemDialog').modal('show');
        });        

        //Event on Material ID Change
        $('#item_id').change(function(){
            var type = 'GET';
            var id = $('#item_id option:selected').val();
            var myUrl = '{{ route("delivery.autoFillItem", ":id") }}';
            myUrl = myUrl.replace(':id', id);

            $.ajax({
                type: type,
                url: myUrl,             
                data: {},
                dataType: 'json',
                success: function(data){
                    $('#request_qty').val(data.qty);

                    $('#item_code').val(data.item_code);
                    $('#detail_code').val(data.detail_code);
                    $('#item_name').val(data.item_name);
                    $('#identification_number').val(data.identification_number);
                    $('#unit').val(data.inventory.unit);
                    $('#qty').focus();

                    if (data.inventory.type == 3) {
                        $('#qty').prop('readonly', true);
                    } else {
                        $('#qty').prop('readonly', false);
                        $('#qty').val(data.qty);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#request_qty').val('');
                    $('#item_code').val('');
                    $('#detail_code').val('');
                    $('#item_name').val('');
                    $('#identification_number').val('');
                    $('#unit').val('');
                    $('#qty').val('');                       
                }
            });
        });        

        //Save Material
        $('#btnInsertItem').click(function(e){          
            e.preventDefault();

            var request = $('#request_qty').val();
            var qty = $('#qty').val();

            if (qty > request) {
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };           

                toastr.error('{{ __("You can't input quantity more than request") }}');
            } else {
                var formData = {
                    '_token'        : $('input[name=_token]').val(),
                    'delivery_id'   : '{{ $delivery->id }}',
                    'item_id'       : $('#item_id option:selected').val(),
                    'qty'           : $('#qty').val(),           
                }

                var type = 'POST';
                var myUrl = '{{ route('delivery.storeDeliveryDetail') }}';

                $.ajax({
                    type: type,
                    url: myUrl,             
                    data: formData,
                    dataType: 'json',
                    success: function(data){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };           
                        
                        if (data.type == 'error') {
                            $('#total_item').val(data.value);
                            toastr.error(data.msg);
                        }else {
                            $('#total_item').val(data.value);
                            toastr.success(data.msg);
                            $('#itemDialog').modal('hide');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };           

                        toastr.error(errorThrown);                      
                    }
                }).always(function (data) {
                    $('#item-table').DataTable().draw(false);
                });                
            }
        });                
    });
</script>
@endsection