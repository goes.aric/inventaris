@extends('layouts.app')

@section('title', __('Show Inventory Delivery'))
@section('meta_description', __('Inventory Delivery Page'))
@section('meta_keywords', __('Inventory Delivery'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('delivery.index') }}" alt="{{ __('Inventory Delivery') }}" title="{{ __('Inventory Delivery List') }}">
                    {{ __('Inventory Delivery') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Delivery') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Inventory Delivery') }}
                                </div>
                            </div>                           
                        </div><hr>                        
                        <div class="row">
                            <!-- Primary Field -->
                            <div class="col-md-5">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="delivery_number">{{ __('Delivery Number') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="delivery_number" type="text" name="delivery_number" placeholder="{{ __('Delivery Number') }}" value="{{ $delivery->delivery_number }}" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="delivery_date">{{ __('Delivery Date') }}</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input class="form-control no-border-right" id="delivery_date" type="text" name="delivery_date" placeholder="{{ __('Delivery Date') }}" value="{{ $delivery->delivery_date }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-transparent">
                                                    <i class="cil-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="department">{{ __('Department') }}</label>
                                    <div class="col-md-8">
                                        <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                            @foreach ($department as $item)
                                                <option value="{{ $item->id }}" {{ $delivery->department_id == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="employee">{{ __('Received By') }}</label>
                                    <div class="col-md-8">
                                        <select id="employee" name="employee" class="form-control selectpicker show-tick" title="{{ __('Select Employee') }}" data-live-search="true" single>
                                            <option value="{{ $delivery->employee_id }}" selected="selected">{{ $delivery->employee->employee_number }} - {{ $delivery->employee->employee_name }}</option>
                                        </select>
                                    </div>
                                </div>                                                                                     
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="inventory_request">{{ __('Inventory Request') }}</label>
                                    <div class="col-md-8">
                                        <select id="inventory_request" name="inventory_request" class="form-control selectpicker show-tick" title="{{ __('Select Inventory Request') }}" data-live-search="true" single>
                                            <option value="{{ $delivery->inventory_request_id }}" selected="selected">{{ $delivery->request->request_number }} - {{ $delivery->request->requested_by }}</option>
                                        </select>
                                    </div>
                                </div>                                    
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="status">{{ __('Status') }}</label>
                                    <div class="col-md-8">
                                        <select id="status" name="status" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" single>
                                            <option value="1" {{ $delivery->status == '1' ? 'selected' : '' }}>{{ __('Pending') }}</option>
                                            <option value="2" {{ $delivery->status == '2' ? 'selected' : '' }}>{{ __('Delivered') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="approved" class="{{ $delivery->approved_by == '' ? 'd-none' : '' }}">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="approved_by">{{ __('Approved By') }}</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="approved_by" type="text" name="approved_by" placeholder="{{ __('Approved By') }}" value="{{ $delivery->approved_by }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="approved_date">{{ __('Approved Date') }}</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input class="form-control no-border-right" id="approved_date" type="text" name="approved_date" placeholder="{{ __('Approved Date') }}" value="{{ $delivery->approved_date }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text bg-transparent">
                                                        <i class="cil-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                         
                                </div>               
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <h4 class="card-title mb-0">
                                    {{ __('Items') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('The items of delivery') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <button type="button" id="btnAddItem" name="btnAddItem" alt="{{ __('Add Item') }}" class="btn btn-success float-right {{ $delivery->status == '1' ? '' : 'd-none' }}" title="{{ __('Add Item') }}">
                                    <i class="icon-plus">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Add Item') }}
                                    </span>
                                </button>                                
                            </div>                                                               
                        </div><hr>
                        <div class="form-group">                               
                            <div id="table" class="table-responsive">
                                <table cellspacing="0" class="table table-bordered datatable dataTable" id="item-table" style="width: 100%" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                {{ __('Item Code') }}
                                            </th>
                                            <th>
                                                {{ __('Detail Code') }}
                                            </th>    
                                            <th>
                                                {{ __('Item Name') }}
                                            </th>
                                            <th>
                                                {{ __('Identification Number') }}
                                            </th>
                                            <th>
                                                {{ __('Unit') }}
                                            </th> 
                                            <th>
                                                {{ __('Qty') }}
                                            </th>                                                
                                            <th class="text-center">
                                                {{ __('Action') }}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" id="note" name="note" placeholder="{{ __('Note') }}" rows="5">{{ $delivery->note }}</textarea>
                                </div>                                      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">    
                                    <div class="col-md-6">
                                        <label class="col-form-label float-right" for="total_item">{{ __('Total Items') }}</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control text-right" type="text" id="total_item" name="total_item" placeholder="{{ __('Total Items') }}" value="{{ $delivery->total_item }}" readonly="readonly">
                                    </div>      
                                </div>                                      
                            </div>
                        </div>    
                        <hr>                                
                        <div class="form-group">
                            <a href="{{ route('delivery.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
                                <i class="icon-action-undo"></i> {{ __('Return') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">
    $(function() {
        function populateSelect() {
            var id = $('#department option:selected').val();
            var requestUrl = '{{ route('delivery.getInventoryRequest', ':id') }}';
            requestUrl = requestUrl.replace(':id', id);
            var employeeUrl = '{{ route('delivery.getEmployee', ':id') }}';
            employeeUrl = employeeUrl.replace(':id', id);

            $.ajax({
                url: requestUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $("#inventory_request").find("option").remove();
                    var item = '';

                    for (var i = 0; i < data.length; i++) {
                        var date = new Date(data[i].request_date);
                        var tanggal = date.toLocaleDateString('fr-ca');

                        item += "<option value='"+data[i].id+"'>"+tanggal+" | "+data[i].request_number+" - "+data[i].requested_by+"</option>";
                    }

                    $("#inventory_request").append(item);
                    $('.selectpicker').selectpicker('refresh'); 
                }
            });

            $.ajax({
                url: employeeUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $("#employee").find("option").remove();
                    var item = '';

                    for (var i = 0; i < data.length; i++) {
                        item += "<option value='"+data[i].id+"'>"+data[i].employee_number+" - "+data[i].employee_name+"</option>";
                    }

                    $("#employee").append(item);
                    $('.selectpicker').selectpicker('refresh'); 
                }
            });
        }

        $('#delivery_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });           

        $('#item-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("delivery.getInventoryDeliveryDetailData", $delivery->id) }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [               
                {data: 'item_code', name: 'item_code'},
                {data: 'detail_code', name: 'detail_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'identification_number', name: 'identification_number'},
                {data: 'unit', name: 'unit'},
                {data: 'qty', name: 'qty'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 1, "asc"]]
        });            
    });
</script>
@endsection