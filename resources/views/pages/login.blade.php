@extends('layouts.login')

@section('title', __('Login'))
@section('meta_description', __('Login Page'))
@section('meta_keywords', __('Login'))
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group">
                <div class="card p-4">
                    <form action="{{ route('login.attempt') }}" method="post">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <h1>
                                {{ __('Login') }}
                            </h1>
                            <p class="text-muted">
                                {{ __('Sign In to your account') }}
                            </p>
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>
                                        {{ $error }}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if (session('message'))
                            <div class="alert alert-danger">
                                {{ session('message') }}
                            </div>
                            @endif
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="cil-user">
                                        </i>
                                    </span>
                                </div>
                                <input class="form-control" id="username" name="username" placeholder="{{ __('Username or Email Address') }}" type="text">
                                </input>
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="cil-lock-locked">
                                        </i>
                                    </span>
                                </div>
                                <input class="form-control" id="password" name="password" placeholder="{{ __('Password') }}" type="password">
                                </input>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary px-4" type="submit">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <span class="display-1">
                                <img src="{{ asset('css/images/tl-education.png') }}">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="col-md-8 mt-4">
            <h5>{{ __('Language') }} : </h5>
            <ul class="lang-link">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li>
                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" alt="{{ __($properties['native']) }}" title="{{ __($properties['native']) }}">
                            <i class="lang-icon cif-{{ $properties['region'] }}"></i>
                        </a>
                    </li>
                @endforeach
            </ul>          
        </div>
    </div>    
</div>
@endsection
