@extends('layouts.app')

@section('title', __('Show Users'))
@section('meta_description', __('Users Page'))
@section('meta_keywords', __('Users'))
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<script type="text/javascript">
	jQuery(function($) {
		var id = '{{ $user->id }}';
	 	var myUrl = '{{ route('users.getUserGroups', ':id') }}';
	 	myUrl = myUrl.replace(':id', id);

		$.ajax({
			url: myUrl,
			type: 'GET',
			dataType: 'json',
			data: {},
			success: function(data){
		  		$.each(data, function(index, val) {
		    		$("input[id='group[" + val.id + "]" + "']").prop('checked', true);
		  		});
			}
		});                         
	});    
</script>
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('users.index') }}" alt="{{ __('Users') }}" title="{{ __('Users List') }}">
                    {{ __('Users') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Users') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('User Data') }}
                                </div>
                            </div>
                        </div><hr>	
						<div class="form-group">
							<label class="col-form-label" for="name">{{ __('Full Name') }}</label>
							<input class="form-control" id="name" type="text" name="name" placeholder="{{ __('Full Name') }}" value="{{ $user->name }}">
						</div>																						
						<div class="row">
							<div class="form-group col-md-6">
								<label class="col-form-label" for="username">{{ __('Username') }}</label>
								<input class="form-control" id="username" type="text" name="username" placeholder="{{ __('Username') }}" value="{{ $user->username }}">
							</div>
							<div class="form-group col-md-6">
								<label class="col-form-label" for="email">{{ __('Email') }}</label>
								<input class="form-control" id="email" type="text" name="email" placeholder="{{ __('Email') }}" value="{{ $user->email }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-form-label" for="status">{{ __('Status') }}</label>
							<div class="form-check">
								<input class="form-check-input" id="active" type="radio" value="0" name="status" {{ $user->is_block == '0' ? 'checked' : '' }}>
								<label class="form-check-label" for="radio1">{{ __('Active') }}</label>
							</div>									
							<div class="form-check">
								<input class="form-check-input" id="inactive" type="radio" value="1" name="status" {{ $user->is_block == '1' ? 'checked' : '' }}>
								<label class="form-check-label" for="radio1">{{ __('Blocked') }}</label>
							</div>	
						</div>
						<div class="form-group">
							<label><b>{{ __('Users Group') }}</b></label>
							<div class="card">
								<div class="card-body">
									@foreach ($group as $item)
										<p>
											<input type="checkbox" name="group[]" value="{{ $item->id }}" id="group[{{ $item->id }}]"><label class="ml-2" for="group[{{ $item->id }}]"> {{ __($item->group_name) }}</label>
										</p>
									@endforeach 		
								</div>
							</div>	
						</div>																
						<hr>								
						<div class="form-group">
							<a href="{{ route('users.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
								<i class="icon-action-undo"></i> {{ __('Return') }}
							</a>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection