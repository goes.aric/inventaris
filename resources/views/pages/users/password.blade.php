@extends('layouts.app')

@section('title', 'Update Password')
@section('meta_description', 'Update Password Page')
@section('meta_keywords', 'Update Password')
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>           
			<li class="breadcrumb-item active">{{ __('Update Password') }}</li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Users') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Update Password') }}
                                </div>
                            </div>
                        </div><hr>
						<form id="myForm" novalidate="novalidate" method="POST" action="{{ route('users.password.update') }}">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							{{ csrf_field() }}
							{{ method_field('PUT') }}
							<div class="form-group">
								<label class="col-form-label" for="old_password">{{ __('Old Password') }}</label>
								<input class="form-control" type="password" id="old_password" name="old_password" placeholder="{{ __('Old Password') }}" value="{{ old('old_password') }}">	
							</div>	
							<div class="row">
								<div class="form-group col-md-6">
									<label class="col-form-label" for="password">{{ __('New Password') }}</label>
									<input class="form-control" type="password" id="password" name="password" placeholder="{{ __('New Password') }}" value="{{ old('password') }}">	
								</div>									
								<div class="form-group col-md-6">
									<label class="col-form-label" for="password_confirmation">{{ __('Retype New Password') }}</label>
									<input class="form-control" type="password" id="password_confirmation" name="password_confirmation" placeholder="{{ __('Retype New Password') }}" value="{{ old('password_confirmation') }}">
								</div>									
							</div>																
							<hr>								
							<div class="form-group">
								<button class="btn btn-primary" type="submit" name="save" alt="{{ __('Update & Close') }}" title="{{ __('Update & Close') }}">
									<i class="cil-save"></i> {{ __('Update & Close') }}
								</button>
								<a href="{{ route('users.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
									<i class="icon-action-undo"></i> {{ __('Cancel') }}
								</a>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection