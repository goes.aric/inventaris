@extends('layouts.app')

@section('title', 'Profile')
@section('meta_description', 'Profile Page')
@section('meta_keywords', 'Profile')
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>           
			<li class="breadcrumb-item active">{{ __('Show Profile') }}</li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Users') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Profile') }}
                                </div>
                            </div>
                        </div><hr>
						<div class="row">								
							<div class="form-group col-md-12">
								<label class="col-form-label" for="name">{{ __('Full Name') }}</label>
								<input class="form-control" id="name" type="text" name="name" placeholder="{{ __('Full Name') }}" value="{{ $user->name }}">
							</div>
						</div>																						
						<div class="row">
							<div class="form-group col-md-6">
								<label class="col-form-label" for="username">{{ __('Username') }}</label>
								<input class="form-control" id="username" type="text" name="username" placeholder="{{ __('Username') }}" value="{{ $user->username }}">
							</div>
							<div class="form-group col-md-6">
								<label class="col-form-label" for="email">{{ __('Email') }}</label>
								<input class="form-control" id="email" type="text" name="email" placeholder="{{ __('Email') }}" value="{{ $user->email }}">
							</div>
						</div>																
						<hr>								
						<div class="form-group">
							<a href="{{ route('dashboard') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return') }}">
								<i class="icon-action-undo"></i> {{ __('Return') }}
							</a>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection