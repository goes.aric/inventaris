@extends('layouts.app')

@section('title', __('Users'))
@section('meta_description', __('Users Page'))
@section('meta_keywords', __('Users'))
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item active">
                {{ __('Users') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Users') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Users List') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <button alt="{{ __('Delete') }}" class="btn btn-danger float-right" data-target="#deleteDialog" data-toggle="modal" title="{{ __('Delete Selected Items') }}" type="button">
                                    <i class="icon-trash">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Delete') }}
                                    </span>
                                </button>
                                <a alt="{{ __('Create') }}" class="btn btn-primary float-right mr-2" href="{{ route('users.create') }}" title="{{ __('Create New Record') }}">
                                    <i class="icon-plus">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Create') }}
                                    </span>
                                </a>
                            </div>
                        </div><hr>                        
                        <div id="table" class="table-responsive">
                            <table cellspacing="0" class="table table-bordered datatable dataTable" id="users-table" style="width: 100%" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            <input id="check-all" name="check-all" type="checkbox">
                                            </input>
                                        </th>
                                        <th>
                                            {{ __('Full Name') }}
                                        </th>
                                        <th>
                                            {{ __('Email') }}
                                        </th>
                                        <th class="text-center">
                                            {{ __('Status') }}
                                        </th>
                                        <th class="text-center">
                                            {{ __('Last Login') }}
                                        </th>
                                        <th class="text-center">
                                            {{ __('Action') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Delete -->
<div class="modal fade" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="deleteDialogLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteDialogLabel">{{ __('Confirmation') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{ __('Are you sure want to delete selected data ?') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('No') }}</button>
                <button type="button" class="btn btn-primary" id="btnContinueDelete">{{ __('Yes') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("users.getUserData") }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [
                {data: 'checkbox_column', name: 'checkbox_column', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'is_block', name: 'is_block', searchable: false, class: 'text-center'},
                {data: 'last_visit', name: 'last_visit', class: 'text-center'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 1, "asc"]]
        }); 

        $('#users-table').on('click', '.btn-reset[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("users.reset", ":id") }}';
            url = url.replace(':id', id);

            // confirm then
            if (confirm('{{ __('Are you sure want to reset the password for the selected user ?') }}')) {
                $.ajax({
                    url: url,
                    type: 'PUT',
                    dataType: 'json',
                    data: {method: '_PUT', submit: true,},
                    success: function(data){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };
                        if (data.type == 'error') {
                            toastr.error(data.msg);
                        }else {
                            toastr.success(data.msg);
                        }
                    }
                }).always(function (data) {
                    $('#users-table').DataTable().draw(false);
                });
            }else{
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.info('{{ __('You have canceled it!') }}');
            }
        });

        $('#users-table').on('click', '.btn-delete[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("users.destroy", ":id") }}';
            url = url.replace(':id', id);

            // confirm then
            if (confirm('{{ __('Are you sure want to delete it ?') }}')) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: 'json',
                    data: {method: '_DELETE', submit: true,},
                    success: function(data){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };                      
                        if (data.type == 'error') {
                            toastr.error(data.msg);
                        }else {
                            toastr.success(data.msg);
                        }
                    }
                }).always(function (data) {
                    $('#user-table').DataTable().draw(false);
                });
            }else{
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.info('{{ __('You have canceled it!') }}');
            }
        }); 

        $('#check-all').on('click', function() {
            if (this.checked) {
                $(':checkbox').each(function() {
                    this.checked = true;
                });
            }else{
                $(':checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

        $('#btnContinueDelete').on('click', function (e) { 
            e.preventDefault();
            $('#deleteDialog').modal('hide');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url = '{{ route("users.delete") }}';

            var ckbox = $("input[name='data[]']");
            var ids = [];
            if (ckbox.is(':checked')) {
                $("input[name='data[]']:checked").each(function (i) {
                    ids[i] = $(this).val();
                });
            }else{          
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.warning('{{ __('Please select the record you want to delete!') }}');
            }       

            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                data: {
                    data: ids,
                },
                success: function(data){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center"
                    };                      
                    if (data.type == 'error') {
                        toastr.error(data.msg);
                    }else {
                        toastr.success(data.msg);
                    }
                }
            }).always(function (data) {
                $('#user-table').DataTable().draw(false);
            });
        }); 
    });     
</script>
@endsection
