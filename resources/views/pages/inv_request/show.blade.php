@extends('layouts.app')

@section('title', __('Show Inventory Request'))
@section('meta_description', __('Inventory Request Page'))
@section('meta_keywords', __('Inventory Request'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('inventory.index') }}" alt="{{ __('Inventory Request') }}" title="{{ __('Inventory Request List') }}">
                    {{ __('Inventory Request') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Request') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Inventory Request') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <button id="btnViewOrderFile" class="btn btn-primary float-right mb-1" alt="View File" title="{{ __('View Request File') }}">
                                    <i class="cib-adobe-acrobat-reader"></i> </i> {{ __('View File') }}
                                </button>                               
                            </div>                            
                        </div><hr>                        
                        <div class="row">
                            <!-- Primary Field -->
                            <div class="col-md-5">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="request_number">{{ __('Request Number') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="request_number" type="text" name="request_number" placeholder="{{ __('Request Number') }}" value="{{ $request->request_number }}" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="request_date">{{ __('Request Date') }}</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input class="form-control no-border-right" id="request_date" type="text" name="request_date" placeholder="{{ __('Request Date') }}" value="{{ $request->request_date }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-transparent">
                                                    <i class="cil-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="department">{{ __('Department') }}</label>
                                    <div class="col-md-8">
                                        <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                            @foreach ($department as $item)
                                                <option value="{{ $item->id }}" {{ $request->department_id == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="requested_by">{{ __('Requested By') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="requested_by" type="text" name="requested_by" placeholder="{{ __('Requested By') }}" value="{{ $request->requested_by }}">
                                    </div>
                                </div>                                    
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="delivery_date">{{ __('Reason of Request') }}</label>
                                    <div class="col-md-8">
                                        <select id="reason" name="reason" class="form-control selectpicker show-tick" title="{{ __('Select Reason') }}" single>
                                            <option value="1" {{ $request->reason == '1' ? 'selected' : '' }}>{{ __('Low on Item') }}</option>
                                            <option value="2" {{ $request->reason == '2' ? 'selected' : '' }}>{{ __('Out of Stock') }}</option>
                                            <option value="3" {{ $request->reason == '3' ? 'selected' : '' }}>{{ __('Need/Purpose for this Item') }}</option>
                                        </select>
                                    </div>
                                </div>                                                 
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="status">{{ __('Status') }}</label>
                                    <div class="col-md-8">
                                        <select id="status" name="status" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" single>
                                            <option value="1" {{ $request->status == '1' ? 'selected' : '' }}>{{ __('Ordered') }}</option>
                                            <option value="2" {{ $request->status == '2' ? 'selected' : '' }}>{{ __('Approved') }}</option>
                                            <option value="3" {{ $request->status == '3' ? 'selected' : '' }}>{{ __('Received') }}</option>
                                            <option value="4" {{ $request->status == '4' ? 'selected' : '' }}>{{ __('Denied') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="approved" class="{{ $request->approved_by == '' ? 'd-none' : '' }}">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="approved_by">{{ __('Approved By') }}</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="approved_by" type="text" name="approved_by" placeholder="{{ __('Approved By') }}" value="{{ $request->approved_by }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="approved_date">{{ __('Approved Date') }}</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input class="form-control no-border-right" id="approved_date" type="text" name="approved_date" placeholder="{{ __('Approved Date') }}" value="{{ $request->approved_date }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text bg-transparent">
                                                        <i class="cil-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                         
                                </div>               
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Items') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('The items of request') }}
                                </div>
                            </div>                                                               
                        </div><hr>
                        <div class="form-group">                               
                            <div id="table" class="table-responsive">
                                <table cellspacing="0" class="table table-bordered datatable dataTable" id="item-table" style="width: 100%" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                {{ __('Item Code') }}
                                            </th>                                        
                                            <th>
                                                {{ __('Item Name') }}
                                            </th>                                    
                                            <th>
                                                {{ __('Qty') }}
                                            </th>
                                            <th>
                                                {{ __('Unit') }}
                                            </th>                                                
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label" for="description">{{ __('Description') }}</label>
                                    <textarea class="form-control" id="description" name="description" placeholder="{{ __('Description') }}" rows="5">{{ $request->description }}</textarea>
                                </div>                                      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">    
                                    <div class="col-md-6">
                                        <label class="col-form-label float-right" for="total_item">{{ __('Total Items') }}</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control text-right" type="text" id="total_item" name="total_item" placeholder="{{ __('Total Items') }}" value="{{ $request->total_item }}" readonly="readonly">
                                    </div>      
                                </div>                                      
                            </div>
                        </div>    
                        <hr>                                
                        <div class="form-group">
                            <a href="{{ route('request.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
                                <i class="icon-action-undo"></i> {{ __('Return') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Request File -->
<div class="modal fade" id="requestFileDialog" tabindex="-1" role="dialog" aria-labelledby="requestFileDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content file-height">
            <div class="modal-header">
                <h5 class="modal-title" id="requestFileDialogLabel">{{ __('View Request Document') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src="{{ asset('storage/files/'.$request->request_file.'') }}" type="application/pdf" width="100%" height="100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {     
        $('#btnViewOrderFile').on('click', function(e) {
            e.preventDefault();

            $('#requestFileDialog').modal('show');
        });        

        $('#item-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("request.getInventoryRequestDetailData", $request->id) }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [               
                {data: 'item_code', name: 'item_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'qty', name: 'qty'},
                {data: 'unit', name: 'unit'}
            ],
            order: [[ 1, "asc"]]
        });             
    });
</script>
@endsection