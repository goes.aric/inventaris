@extends('layouts.app')

@section('title', __('Create Inventory Request'))
@section('meta_description', __('Inventory Request Page'))
@section('meta_keywords', __('Inventory Request'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('request.index') }}" alt="{{ __('Inventory Request') }}" title="{{ __('Inventory Request List') }}">
                    {{ __('Inventory Request') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Create') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Request') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Create New Inventory Request') }}
                                </div>
                            </div>
                        </div><hr>                        
						<form id="myForm" novalidate="novalidate" method="POST" action="{{ route('request.store') }}" enctype="multipart/form-data">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							{{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="request_date">{{ __('Request Date') }}</label>
                                    <div class="input-group">
                                        <input class="form-control no-border-right" id="request_date" type="text" name="request_date" placeholder="{{ __('Request Date') }}" value="{{ date('Y/m/d') }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-transparent">
                                                <i class="cil-calendar"></i>
                                            </span>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="department">{{ __('Department') }}</label>
                                    <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                        @foreach ($department as $item)
                                            <option value="{{ $item->id }}" {{ old('department') == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                        @endforeach
                                    </select>
                                </div>                                                                 
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="requested_by">{{ __('Requested By') }}</label>
                                    <input class="form-control" id="requested_by" type="text" name="requested_by" placeholder="{{ __('Employee Name') }}" value="{{ old('requested_by') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="reason">{{ __('Reason of Request') }}</label>
                                    <select id="reason" name="reason" class="form-control selectpicker show-tick" title="{{ __('Select Reason') }}" single>
                                        <option value="1" {{ old('reason') == '1' ? 'selected' : '' }}>{{ __('Low on Item') }}</option>
                                        <option value="2" {{ old('reason') == '2' ? 'selected' : '' }}>{{ __('Out of Stock') }}</option>
                                        <option value="3" {{ old('reason') == '3' ? 'selected' : '' }}>{{ __('Need/Purpose for this Item') }}</option>
                                    </select>
                                </div>                                                             
                            </div>                         
                            <div class="form-group">
                                <label class="col-form-label" for="description">{{ __('Request Description') }}</label>
                                <textarea class="form-control" id="description" name="description" placeholder="{{ __('Request Description') }}" rows="10">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="request_file">{{ __('Request File') }}</label>
                                <input class="form-control" id="request_file" type="file" name="request_file" placeholder="{{ __('Request File') }}" value="{{ old('request_file') }}">
                            </div>                             		
							<hr>								
							<div class="form-group">
                                <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Save & Continue') }}" title="{{ __('Save & Continue') }}">
                                    <i class="cil-save"></i> {{ __('Save & Continue') }}
                                </button>
								<a href="{{ route('request.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
									<i class="icon-action-undo"></i> {{ __('Cancel') }}
								</a>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">
    $(function() {
        $('#request_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });
    });
</script>
@endsection