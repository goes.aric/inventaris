@extends('layouts.app')

@section('title', __('Update Inventory Request'))
@section('meta_description', __('Inventory Request Page'))
@section('meta_keywords', __('Inventory Request'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('request.index') }}" alt="{{ __('Inventory Request') }}" title="{{ __('Inventory Request List') }}">
                    {{ __('Inventory Request') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Update') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Request') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Update Inventory Request') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <button id="btnViewOrderFile" class="btn btn-primary float-right mb-1" alt="View File" title="{{ __('View Request File') }}">
                                    <i class="cib-adobe-acrobat-reader"></i> </i> {{ __('View File') }}
                                </button>                               
                            </div>                            
                        </div><hr>                        
                        <form id="myForm" novalidate="novalidate" method="POST" action="{{ route('request.update', $request->id) }}" enctype="multipart/form-data">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <!-- Primary Field -->
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="request_number">{{ __('Request Number') }}</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="request_number" type="text" name="request_number" placeholder="{{ __('Request Number') }}" value="{{ $request->request_number }}" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="request_date">{{ __('Request Date') }}</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input class="form-control no-border-right" id="request_date" type="text" name="request_date" placeholder="{{ __('Request Date') }}" value="{{ $request->request_date }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text bg-transparent">
                                                        <i class="cil-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="department">{{ __('Department') }}</label>
                                        <div class="col-md-8">
                                            <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                                @foreach ($department as $item)
                                                    <option value="{{ $item->id }}" {{ $request->department_id == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="requested_by">{{ __('Requested By') }}</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="requested_by" type="text" name="requested_by" placeholder="{{ __('Requested By') }}" value="{{ $request->requested_by }}">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="delivery_date">{{ __('Reason of Request') }}</label>
                                        <div class="col-md-8">
                                            <select id="reason" name="reason" class="form-control selectpicker show-tick" title="{{ __('Select Reason') }}" single>
                                                <option value="1" {{ $request->reason == '1' ? 'selected' : '' }}>{{ __('Low on Item') }}</option>
                                                <option value="2" {{ $request->reason == '2' ? 'selected' : '' }}>{{ __('Out of Stock') }}</option>
                                                <option value="3" {{ $request->reason == '3' ? 'selected' : '' }}>{{ __('Need/Purpose for this Item') }}</option>
                                            </select>
                                        </div>
                                    </div>                                                 
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="status">{{ __('Status') }}</label>
                                        <div class="col-md-8">
                                            <select id="status" name="status" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" single>
                                                <option value="1" {{ $request->status == '1' ? 'selected' : '' }}>{{ __('Ordered') }}</option>
                                                <option value="2" {{ $request->status == '2' ? 'selected' : '' }}>{{ __('Approved') }}</option>
                                                <option value="4" {{ $request->status == '4' ? 'selected' : '' }}>{{ __('Denied') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="approved" class="{{ $request->approved_by == '' ? 'd-none' : '' }}">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="approved_by">{{ __('Approved By') }}</label>
                                            <div class="col-md-8">
                                                <input class="form-control" id="approved_by" type="text" name="approved_by" placeholder="{{ __('Approved By') }}" value="{{ $request->approved_by }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="approved_date">{{ __('Approved Date') }}</label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input class="form-control no-border-right" id="approved_date" type="text" name="approved_date" placeholder="{{ __('Approved Date') }}" value="{{ $request->approved_date }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text bg-transparent">
                                                            <i class="cil-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                         
                                    </div>               
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <h4 class="card-title mb-0">
                                        {{ __('Items') }}
                                    </h4>
                                    <div class="small text-muted">
                                        {{ __('The items of request') }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                    @if ($request->status == '1')
                                        <button type="button" id="btnAddItem" name="btnAddItem" alt="{{ __('Add Item') }}" class="btn btn-success float-right" title="{{ __('Add Item') }}">
                                            <i class="icon-plus">
                                            </i>
                                            <span class="d-none d-sm-inline">
                                                {{ __('Add Item') }}
                                            </span>
                                        </button> 
                                    @endif                               
                                </div>                                                               
                            </div><hr>
                            <div class="form-group">                               
                                <div id="table" class="table-responsive">
                                    <table cellspacing="0" class="table table-bordered datatable dataTable" id="item-table" style="width: 100%" width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    {{ __('Item Code') }}
                                                </th>                                        
                                                <th>
                                                    {{ __('Item Name') }}
                                                </th>                                    
                                                <th>
                                                    {{ __('Qty') }}
                                                </th>
                                                <th>
                                                    {{ __('Unit') }}
                                                </th>                                                
                                                <th class="text-center">
                                                    {{ __('Action') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-form-label" for="description">{{ __('Description') }}</label>
                                        <textarea class="form-control" id="description" name="description" placeholder="{{ __('Description') }}" rows="5">{{ $request->description }}</textarea>
                                    </div>                                      
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">    
                                        <div class="col-md-6">
                                            <label class="col-form-label float-right" for="total_item">{{ __('Total Items') }}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control text-right" type="text" id="total_item" name="total_item" placeholder="{{ __('Total Items') }}" value="{{ $request->total_item }}" readonly="readonly">
                                        </div>      
                                    </div>                                      
                                </div>
                            </div>    
                            <hr>                                
                            <div class="form-group">
                                @if ($request->status == '1')
                                    <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Update & Close') }}" title="{{ __('Update & Close') }}">
                                        <i class="cil-save"></i> {{ __('Update & Close') }}
                                    </button>
                                @endif
                                <a href="{{ route('request.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
                                    <i class="icon-action-undo"></i> {{ __('Cancel') }}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Add Item -->
<div class="modal fade" id="itemDialog" tabindex="-1" role="dialog" aria-labelledby="itemDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemDialogLabel">{{ __('Add Item') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label" for="item_id">{{ __('Items') }}</label>
                    <select id="item_id" name="item_id" class="form-control selectpicker show-tick" data-live-search="true" title="{{ __('Select Items') }}">
                    </select>                    
                </div>
                <h5><b>{{ __('Details') }}</b></h5>
                <p class="text-muted">{{ __('Here detail about item') }}</p>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label class="col-form-label" for="item_code">{{ __('Item Code') }}</label>
                        <input class="form-control" id="item_code" type="text" name="item_code" placeholder="{{ __('Item Code') }}" readonly="readonly">
                    </div>
                    <div class="form-group col-md-8">
                        <label class="col-form-label" for="item_name">{{ __('Item Name') }}</label>
                        <input class="form-control" id="item_name" type="text" name="item_name" placeholder="{{ __('Item Name') }}" readonly="readonly">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="col-form-label" for="qty">{{ __('Qty') }}</label>
                        <input class="form-control text-right" id="qty" type="number" name="qty">
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-form-label" for="unit">{{ __('Unit') }}</label>
                        <input class="form-control text-right" id="unit" type="text" name="unit" placeholder="{{ __('Unit') }}" readonly="readonly">
                    </div>                    
                </div>                               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                <button type="button" class="btn btn-primary" id="btnInsertItem">{{ __('Insert') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Dialog Request File -->
<div class="modal fade" id="requestFileDialog" tabindex="-1" role="dialog" aria-labelledby="requestFileDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content file-height">
            <div class="modal-header">
                <h5 class="modal-title" id="requestFileDialogLabel">{{ __('View Request Document') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src="{{ asset('storage/files/'.$request->request_file.'') }}" type="application/pdf" width="100%" height="100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('#request_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });

        $('#approved_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });        

        $('#btnViewOrderFile').on('click', function(e) {
            e.preventDefault();

            $('#requestFileDialog').modal('show');
        });        

        $('#item-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("request.getInventoryRequestDetailData", $request->id) }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [               
                {data: 'item_code', name: 'item_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'qty', name: 'qty'},
                {data: 'unit', name: 'unit'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 1, "asc"]]
        });

        $('#item-table').on('click', '.btn-delete[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("request.requestDetailDestroy", ":id") }}';
            url = url.replace(':id', id);

            // confirm then
            if (confirm('{{ __('Are you sure want to delete it ?') }}')) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: 'json',
                    data: {method: '_DELETE', submit: true,},
                    success: function(data){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };                      
                        if (data.type == 'error') {
                            $('#total_item').val(data.value);
                            toastr.error(data.msg);
                        }else {
                            $('#total_item').val(data.value);
                            toastr.success(data.msg);
                        }
                    }
                }).always(function (data) {
                    $('#item-table').DataTable().draw(false);
                });
            }else{
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.info('{{ __('You have canceled it!') }}');
            }
        });

        function clearItem(){
            $('#item_id').val('');
            $('#item_code').val('');
            $('#item_name').val('');
            $('#qty').val('');
            $('#unit').val('');
            $('.selectpicker').selectpicker('refresh');         
        }

        function populateItem(){
            var myUrl = '{{ route('request.populateItem') }}';

            $.ajax({
                type: 'GET',
                url: myUrl,             
                data: {
                    'id'    : {{ $request->id }}
                },
                dataType: 'json',
                success: function(data){
                    $("#item_id").find("option").remove();

                    var data_item = data.item;
                    var item = '';
                    for (var i in data_item)
                    {
                        item        = item + "<option value='"+data_item[i].id+"'>"+data_item[i].item_code+" - "+data_item[i].item_name+"</option>";
                    }

                    $("#item_id").append(item); 
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center"
                    };           

                    toastr.error(errorThrown);                      
                }
            });            
        }

        $('#btnAddItem').click(function(){
            clearItem();
            populateItem();
            $('#itemDialog').modal('show');
        });        

        //Event on Material ID Change
        $('#item_id').change(function(){
            var type = 'GET';
            var id = $('#item_id option:selected').val();
            var myUrl = '{{ route("request.autoFillItem", ":id") }}';
            myUrl = myUrl.replace(':id', id);

            $.ajax({
                type: type,
                url: myUrl,             
                data: {},
                dataType: 'json',
                success: function(data){
                    $('#item_code').val(data.item_code);
                    $('#item_name').val(data.item_name);
                    $('#unit').val(data.unit);
                    $('#qty').focus();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $('#item_code').val('');
                    $('#item_name').val('');
                    $('#unit').val('');
                    $('#qty').val('');                       
                }
            });
        });        

        //Save Material
        $('#btnInsertItem').click(function(e){          
            e.preventDefault(); 

            var formData = {
                '_token'    : $('input[name=_token]').val(),
                'request_id': '{{ $request->id }}',
                'item_id'   : $('#item_id option:selected').val(),
                'qty'       : $('#qty').val(),           
            }

            var type = 'POST';
            var myUrl = '{{ route('request.storeRequestDetail') }}';

            $.ajax({
                type: type,
                url: myUrl,             
                data: formData,
                dataType: 'json',
                success: function(data){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center"
                    };           
                    
                    if (data.type == 'error') {
                        $('#total_item').val(data.value);
                        toastr.error(data.msg);
                    }else {
                        $('#total_item').val(data.value);
                        toastr.success(data.msg);
                        $('#itemDialog').modal('hide');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center"
                    };           

                    toastr.error(errorThrown);                      
                }
            }).always(function (data) {
                $('#item-table').DataTable().draw(false);
            });             
        });

        $('#status').on('change', function(e) {
            e.preventDefault();

            var status = $('#status option:selected').val();

            if (status == '2' || status == '3') {
                $('#approved').removeClass('d-none');
            } else {
                $('#approved').addClass('d-none');
            }
        });                 
    });
</script>
@endsection