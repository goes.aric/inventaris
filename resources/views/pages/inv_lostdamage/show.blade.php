@extends('layouts.app')

@section('title', __('Show Inventory Return'))
@section('meta_description', __('Inventory Return Page'))
@section('meta_keywords', __('Inventory Return'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('return.index') }}" alt="{{ __('Inventory Return') }}" title="{{ __('Inventory Return List') }}">
                    {{ __('Inventory Return') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Return') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Inventory Return') }}
                                </div>
                            </div>                           
                        </div><hr>                        
                        <div class="row">
                            <!-- Primary Field -->
                            <div class="col-md-5">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="return_number">{{ __('Return Number') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="return_number" type="text" name="return_number" placeholder="{{ __('Return Number') }}" value="{{ $return->return_number }}" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="return_date">{{ __('Return Date') }}</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input class="form-control no-border-right" id="return_date" type="text" name="return_date" placeholder="{{ __('Return Date') }}" value="{{ $return->return_date }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-transparent">
                                                    <i class="cil-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="department">{{ __('Department') }}</label>
                                    <div class="col-md-8">
                                        <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                            @foreach ($department as $item)
                                                <option value="{{ $item->id }}" {{ $return->department_id == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="employee">{{ __('Return By') }}</label>
                                    <div class="col-md-8">
                                        <select id="employee" name="employee" class="form-control selectpicker show-tick" title="{{ __('Select Employee') }}" data-live-search="true" single>
                                            <option value="{{ $return->employee_id }}" selected="selected">{{ $return->employee->employee_number }} - {{ $return->employee->employee_name }}</option>
                                        </select>
                                    </div>
                                </div>                                                                                     
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="inventory_delivery">{{ __('Inventory Delivery') }}</label>
                                    <div class="col-md-8">
                                        <select id="inventory_delivery" name="inventory_delivery" class="form-control selectpicker show-tick" title="{{ __('Select Inventory Delivery') }}" data-live-search="true" single>
                                            <option value="{{ $return->inventory_delivery_id }}" selected="selected">{{ $return->delivery->delivery_number }}</option>
                                        </select>
                                    </div>
                                </div>                                    
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="status">{{ __('') }}Status</label>
                                    <div class="col-md-8">
                                        <select id="status" name="status" class="form-control selectpicker show-tick" title="Select Status" single>
                                            <option value="1" {{ $return->status == '1' ? 'selected' : '' }}>{{ __('Pending') }}</option>
                                            <option value="2" {{ $return->status == '2' ? 'selected' : '' }}>{{ __('Valid') }}</option>
                                        </select>
                                    </div>
                                </div>              
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Items') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('The items of delivery') }}
                                </div>
                            </div>                                                              
                        </div><hr>
                        <div class="form-group">                               
                            <div id="table" class="table-responsive">
                                <table cellspacing="0" class="table table-bordered datatable dataTable" id="item-table" style="width: 100%" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                {{ __('Item Code') }}
                                            </th>
                                            <th>
                                                {{ __('Detail Code') }}
                                            </th>    
                                            <th>
                                                {{ __('Item Name') }}
                                            </th>
                                            <th>
                                                {{ __('Identification Number') }}
                                            </th>
                                            <th>
                                                {{ __('Unit') }}
                                            </th> 
                                            <th>
                                                {{ __('Qty') }}
                                            </th>                                                
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" id="note" name="note" placeholder="{{ __('Note') }}" rows="5">{{ $return->note }}</textarea>
                                </div>                                      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">    
                                    <div class="col-md-6">
                                        <label class="col-form-label float-right" for="total_item">{{ __('Total Items') }}</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control text-right" type="text" id="total_item" name="total_item" placeholder="{{ __('Total Items') }}" value="{{ $return->total_item }}" readonly="readonly">
                                    </div>      
                                </div>                                      
                            </div>
                        </div>    
                        <hr>                                
                        <div class="form-group">
                            <a href="{{ route('return.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
                                <i class="icon-action-undo"></i> {{ __('Return') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">
    $(function() {        
        $('#item-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("return.getInventoryReturnDetailData", $return->id) }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [               
                {data: 'item_code', name: 'item_code'},
                {data: 'detail_code', name: 'detail_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'identification_number', name: 'identification_number'},
                {data: 'unit', name: 'unit'},
                {data: 'qty', name: 'qty'}
            ],
            order: [[ 1, "asc"]]
        });               
    });
</script>
@endsection