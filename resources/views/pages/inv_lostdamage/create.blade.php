@extends('layouts.app')

@section('title', __('Create Inventory Return'))
@section('meta_description', __('Inventory Return Page'))
@section('meta_keywords', __('Inventory Return'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('return.index') }}" alt="{{ __('Inventory Return') }}" title="{{ __('Inventory Return List') }}">
                    {{ __('Inventory Return') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Create') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Return') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Create New Inventory Return') }}
                                </div>
                            </div>
                        </div><hr>                        
						<form id="myForm" novalidate="novalidate" method="POST" action="{{ route('return.store') }}" enctype="multipart/form-data">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							{{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="return_date">{{ __('Return Date') }}</label>
                                    <div class="input-group">
                                        <input class="form-control no-border-right" id="return_date" type="text" name="return_date" placeholder="{{ __('Return Date') }}" value="{{ date('Y/m/d') }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-transparent">
                                                <i class="cil-calendar"></i>
                                            </span>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="department">{{ __('Department') }}</label>
                                    <select id="department" name="department" class="form-control selectpicker show-tick" title="{{ __('Select Department') }}" data-live-search="true" single>
                                        @foreach ($department as $item)
                                            <option value="{{ $item->id }}" {{ old('department') == $item->id ? 'selected' : '' }}>{{ $item->code }} - {{ $item->department_name }}</option>
                                        @endforeach
                                    </select>
                                </div>                                                                 
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="inventory_delivery">{{ __('Inventory Delivery') }}</label>
                                    <select id="inventory_delivery" name="inventory_delivery" class="form-control selectpicker show-tick" title="{{ __('Select Inventory Delivery') }}" data-live-search="true" single>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="employee">{{ __('Return By') }}</label>
                                    <select id="employee" name="employee" class="form-control selectpicker show-tick" title="{{ __('Select Employee') }}" data-live-search="true" single>
                                    </select>
                                </div>                                                             
                            </div>                         
                            <div class="form-group">
                                <label class="col-form-label" for="note">{{ __('Request Description') }}</label>
                                <textarea class="form-control" id="note" name="note" placeholder="{{ __('Request Description') }}" rows="7">{{ old('note') }}</textarea>
                            </div>                           		
							<hr>								
							<div class="form-group">
                                <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Save & Continue') }}" title="{{ __('Save & Continue') }}">
                                    <i class="cil-save"></i> {{ __('Save & Continue') }}
                                </button>
								<a href="{{ route('return.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
									<i class="icon-action-undo"></i> {{ __('Cancel') }}
								</a>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">
    $(function() {
        populateSelect();

        $('#return_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });

        function populateSelect() {
            var id = $('#department option:selected').val();
            var deliveryUrl = '{{ route('return.getInventoryDelivery', ':id') }}';
            deliveryUrl = deliveryUrl.replace(':id', id);
            var employeeUrl = '{{ route('return.getEmployee', ':id') }}';
            employeeUrl = employeeUrl.replace(':id', id);

            $.ajax({
                url: deliveryUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $("#inventory_delivery").find("option").remove();
                    var item = '';

                    for (var i = 0; i < data.length; i++) {
                        var date = new Date(data[i].delivery_date);
                        var tanggal = date.toLocaleDateString('fr-ca');

                        item += "<option value='"+data[i].id+"'>"+tanggal+" | "+data[i].delivery_number+" - "+data[i].employee.employee_name+"</option>";
                    }

                    $("#inventory_delivery").append(item);
                    $('.selectpicker').selectpicker('refresh'); 
                }
            });

            $.ajax({
                url: employeeUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $("#employee").find("option").remove();
                    var item = '';

                    for (var i = 0; i < data.length; i++) {
                        item += "<option value='"+data[i].id+"'>"+data[i].employee_number+" - "+data[i].employee_name+"</option>";
                    }

                    $("#employee").append(item);
                    $('.selectpicker').selectpicker('refresh'); 
                }
            });
        }

        $('#department').on('change blur select', function() {
            populateSelect();
        });
    });
</script>
@endsection