@extends('layouts.app')

@section('title', __('Show Inventory Procurement'))
@section('meta_description', __('Inventory Procurement Page'))
@section('meta_keywords', __('Inventory Procurement'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('procurement.index') }}" alt="{{ __('Inventory Procurement') }}" title="{{ __('Inventory Procurement List') }}">
                    {{ __('Inventory Procurement') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Procurement') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Inventory Procurement') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <button id="btnViewProcurementFile" class="btn btn-primary float-right mb-1" alt="View File" title="{{ __('View Procurement File') }}">
                                    <i class="cib-adobe-acrobat-reader"></i> </i> {{ __('View File') }}
                                </button>                               
                            </div>                            
                        </div><hr>                        
                        <form id="myForm" novalidate="novalidate" method="POST" action="{{ route('procurement.update', $procurement->id) }}" enctype="multipart/form-data">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <!-- Primary Field -->
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <label class="col-md-5 col-form-label" for="procurement_number">{{ __('Procurement Number') }}</label>
                                        <div class="col-md-7">
                                            <input class="form-control" id="procurement_number" type="text" name="Procurement_number" placeholder="{{ __('procurement Number') }}" value="{{ $procurement->procurement_number }}" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-5 col-form-label" for="procurement_date">{{ __('Procurement Date') }}</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <input class="form-control no-border-right" id="procurement_date" type="text" name="procurement_date" placeholder="{{ __('Procurement Date') }}" value="{{ $procurement->procurement_date }}">
                                                <div class="input-group-append">
                                                    <span class="input-group-text bg-transparent">
                                                        <i class="cil-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-5 col-form-label" for="decree_number">{{ __('Decree Number') }}</label>
                                        <div class="col-md-7">
                                            <input class="form-control" id="decree_number" type="text" name="decree_number" placeholder="{{ __('Decree Number') }}" value="{{ $procurement->decree_number }}"> 
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-5 col-form-label" for="reff_number">{{ __('Reff Number') }}</label>
                                        <div class="col-md-7">
                                            <input class="form-control" id="reff_number" type="text" name="reff_number" placeholder="{{ __('Reff Number') }}" value="{{ $procurement->reff_number }}"> 
                                        </div>
                                    </div>                                                                                     
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="origin">{{ __('Origin') }}</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" id="origin" name="origin" placeholder="{{ __('Origin') }}">{{ $procurement->origin }}</textarea>
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label" for="status">{{ __('Status') }}</label>
                                        <div class="col-md-8">
                                            <select id="status" name="status" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" single>
                                                <option value="1" {{ $procurement->status == '1' ? 'selected' : '' }}>{{ __('Draff') }}</option>
                                                <option value="2" {{ $procurement->status == '2' ? 'selected' : '' }}>{{ __('Valid') }}</option>
                                            </select>
                                        </div>
                                    </div>               
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <h4 class="card-title mb-0">
                                        {{ __('Items') }}
                                    </h4>
                                    <div class="small text-muted">
                                        {{ __('The items of procurement') }}
                                    </div>
                                </div>                                                              
                            </div><hr>
                            <div class="form-group">                               
                                <div id="table" class="table-responsive">
                                    <table cellspacing="0" class="table table-bordered datatable dataTable" id="item-table" style="width: 100%" width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    {{ __('Item Code') }}
                                                </th>                                        
                                                <th>
                                                    {{ __('Item Name') }}
                                                </th>                                    
                                                <th>
                                                    {{ __('Qty') }}
                                                </th>
                                                <th>
                                                    {{ __('Unit') }}
                                                </th>
                                                <th>
                                                    {{ __('Price') }}
                                                </th>       
                                                <th class="text-center">
                                                    {{ __('Action') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="information" name="information" placeholder="{{ __('Information') }}" rows="5">{{ $procurement->information }}</textarea>
                                    </div>                                      
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">    
                                        <div class="col-md-6">
                                            <label class="col-form-label float-right" for="total_item">{{ __('Total Items') }}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control text-right" type="text" id="total_item" name="total_item" placeholder="{{ __('Total Items') }}" value="{{ $procurement->total_item }}" readonly="readonly">
                                        </div>      
                                    </div>
                                    <div class="form-group row">    
                                        <div class="col-md-6">
                                            <label class="col-form-label float-right" for="total">{{ __('Total') }}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control text-right" type="text" id="total" name="total" placeholder="{{ __('Total') }}" value="{{ $procurement->total }}" readonly="readonly">
                                        </div>      
                                    </div>                                                                          
                                </div>
                            </div>    
                            <hr>                                
                            <div class="form-group">
                                <a href="{{ route('procurement.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
                                    <i class="icon-action-undo"></i> {{ __('Return') }}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Add Item -->
<div class="modal fade" id="itemDialog" tabindex="-1" role="dialog" aria-labelledby="itemDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemDialogLabel">{{ __('Show Item') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="item_form">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-form-label" for="item_id">{{ __('Items') }}</label>
                            <select id="item_id" name="item_id" class="form-control selectpicker show-tick" data-live-search="true" title="{{ __('Select Items') }}">
                            </select>
                            <input class="form-control" id="item_code" type="hidden" name="item_code" placeholder="{{ __('Item Code') }}" readonly="readonly">
                            <input class="form-control" id="item_name" type="hidden" name="item_name" placeholder="{{ __('Item Name') }}" readonly="readonly">
                            <input class="form-control text-right" id="unit" type="hidden" name="unit" placeholder="{{ __('Unit') }}" readonly="readonly">
                            <input class="form-control text-right" id="type" type="hidden" name="type" placeholder="{{ __('Type') }}" readonly="readonly"> 
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="qty">{{ __('Qty') }}</label>
                            <input class="form-control" id="qty" type="text" name="qty" placeholder="{{ __('Qty') }}">
                        </div>                    
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="price">{{ __('Price') }}</label>
                            <input class="form-control" id="price" type="text" name="price" placeholder="{{ __('Price') }}">
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="sub_total">{{ __('Sub Total') }}</label>
                            <input class="form-control" id="sub_total" type="text" name="sub_total" placeholder="{{ __('Sub Total') }}" readonly="readonly">
                        </div>
                    </div>                
                    <h5><b>{{ __('Details') }}</b></h5>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label" for="detail_code">{{ __('Detail Code') }}</label>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label" for="identification_number">{{ __('Identification Number') }}</label>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="condition">{{ __('Condition') }}</label>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label" for="description">{{ __('Information') }}</label>
                        </div>                                        
                    </div>                
                    <div id="detail">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <input class="form-control" id="detail_code" type="text" name="detail_code" readonly="readonly">
                            </div>
                            <div class="form-group col-md-4">
                                <input class="form-control" id="identification_number" type="text" name="identification_number">
                            </div>
                            <div class="form-group col-md-2">
                                <select id="condition" name="condition" class="form-control selectpicker show-tick" single title="{{ __('Select Condition') }}">
                                    <option value="1">{{ __('Good') }}</option>
                                    <option value="2">{{ __('Lightly Damaged') }}</option>
                                    <option value="3">{{ __('Heavily Damaged') }}</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <input class="form-control" id="description" type="text" name="description">
                            </div>                                        
                        </div>                    
                    </div>                    
                </form>                              
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="btnCancel" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Dialog Procurement File -->
<div class="modal fade" id="procurementFileDialog" tabindex="-1" role="dialog" aria-labelledby="procurementFileDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content file-height">
            <div class="modal-header">
                <h5 class="modal-title" id="procurementFileDialogLabel">{{ __('View Procurement Document') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src="{{ asset('storage/files/'.$procurement->procurement_file.'') }}" type="application/pdf" width="100%" height="100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        var proses = false;

        $('#procurement_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });       

        $('#btnViewProcurementFile').on('click', function(e) {
            e.preventDefault();

            $('#procurementFileDialog').modal('show');
        });      

        $('#item-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{{ route("procurement.getInventoryProcurementDetailData", $procurement->id) }}',
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [               
                {data: 'item_code', name: 'item_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'qty', name: 'qty'},
                {data: 'unit', name: 'unit'},
                {data: 'price', name: 'price'},
                {data: 'view', name: 'view', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 1, "asc"]]
        });

        $('#item-table').on('click', '.btn-view[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("procurement.getProcurementDetailItem", ":id") }}';
            url = url.replace(':id', id);

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(data){
                    $("#item_id").find("option").remove();
                    var item = "<option value='"+data.inventory_id+"' selected>"+data.item_code+" - "+data.item_name+"</option>";
                    $("#item_id").append(item); 

                    $('#type').val(data.inventory.type);
                    $('#item_code').val(data.item_code);
                    $('#item_name').val(data.item_name);
                    $('#qty').val(data.qty);
                    $('#unit').val(data.unit);
                    $('#price').val(data.price);
                    $('#sub_total').val(data.sub_total);

                    $('#detail div').remove();
                    $('#itemDialogLabel').text('Show Item');
                    $('#btnCancel').text('Close');

                    //Populate Sub Detail Data
                    var detail_id = data.id;
                    var myUrl = '{{ route("procurement.getSubDetailItem", ":id") }}';
                    myUrl = myUrl.replace(':id', id);

                    $.ajax({
                        url: myUrl,
                        type: 'GET',
                        dataType: 'json',
                        success: function(data){
                            var component = '';
                            for (var i = 0; i < data.length; i++) {
                                var condition = '';
                                if (data[i].condition == '1') {
                                    condition = '{{ __('Good') }}';
                                } else if (data[i].condition == '2') {
                                    condition = '{{ __('Lightly Damaged') }}';
                                } else {
                                    condition = '{{ __('Heavily Damaged') }}';
                                }

                                component += '<div class="row"><div class="form-group col-md-3"><input class="form-control" id="detail_code'+i+'" type="text" name="detail_code'+i+'" value="'+data[i].detail_code+'" readonly="readonly"></div><div class="form-group col-md-4"><input class="form-control" id="identification_number'+i+'" type="text" name="identification_number'+i+'" value="'+data[i].identification_number+'"></div><div class="form-group col-md-2"><select id="condition'+i+'" name="condition'+i+'" class="form-control selectpicker show-tick" single title="{{ __('Select Condition') }}"><option value="'+data[i].condition+'" selected>'+condition+'</option></select></div><div class="form-group col-md-3"><input class="form-control" id="description'+i+'" type="text" name="description'+i+'" value="'+data[i].information+'"></div></div>';
                            }

                            $('#detail').append(component);
                            $('.selectpicker').selectpicker('refresh');
                        }
                    });

                    //Show
                    $('.selectpicker').selectpicker('refresh');                    
                    $('#itemDialog').modal('show');                     
                }
            });
        });                              
    });
</script>
@endsection