@extends('layouts.app')

@section('title', __('Inventory Procurement'))
@section('meta_description', __('Inventory Procurement Page'))
@section('meta_keywords', __('Inventory Procurement'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/highlight.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/handlebars.js') }}"></script>
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item active">
                {{ __('Inventory Procurement') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Procurement') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Inventory Procurement List') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <a alt="{{ __('Create') }}" class="btn btn-primary float-right" href="{{ route('procurement.create') }}" title="{{ __('Create New Record') }}">
                                    <i class="icon-plus">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Create') }}
                                    </span>
                                </a>
                            </div>
                        </div><hr>                        
                        <div id="table" class="table-responsive">
                            <table cellspacing="0" class="table table-bordered datatable dataTable" id="procurement-table" style="width: 100%" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="3">
                                            <label class="col-form-label" for="filter_date">{{ __('Request Date') }}</label>
                                            <div class="input-group request" id="request">
                                                <input type="text" class="form-control" id="procurement_start" name="procurement_start">
                                                <div class="input-group-addon mr-1 ml-1 col-form-label">{{ __('to') }}</div>
                                                <input type="text" class="form-control" id="procurement_end" name="procurement_end">
                                            </div>                                             
                                        </th>                                       
                                        <th colspan="2">
                                            <label class="col-form-label" for="filter_status">{{ __('Status') }}</label>
                                            <select id="filter_status" name="filter_status" class="form-control selectpicker show-tick" title="{{ __('Select Status Filter') }}" single>
                                                <option value="%%" selected>{{ __('All Status') }}</option>
                                                <option data-divider="true"></option>
                                                <option value="1">{{ __('Pending') }}</option>
                                                <option value="2">{{ __('Valid') }}</option>
                                            </select>                                             
                                        </th>
                                        <th colspan="4"></th>
                                    </tr>                                    
                                    <tr>
                                        <th></th>
                                        <th>
                                            {{ __('Procurement Number') }}
                                        </th>                                         
                                        <th>
                                            {{ __('Procurement Date') }}
                                        </th>                                        
                                        <th>
                                            {{ __('Decree Number') }}
                                        </th>
                                        <th>
                                            {{ __('Reff Number') }}
                                        </th>
                                        <th>
                                            {{ __('Origin') }}
                                        </th>
                                        <th>
                                            {{ __('Item') }}
                                        </th>                                                                                      
                                        <th>
                                            {{ __('Total') }}
                                        </th>                                                                            
                                        <th class="text-center">
                                            {{ __('Action') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Show Item -->
<div class="modal fade" id="itemDialog" tabindex="-1" role="dialog" aria-labelledby="itemDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemDialogLabel">{{ __('Show Item') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="item_form">               
                    <h5><b>{{ __('Details') }}</b></h5>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label" for="detail_code">{{ __('Detail Code') }}</label>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label" for="identification_number">{{ __('Identification Number') }}</label>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="condition">{{ __('Condition') }}</label>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label" for="description">{{ __('Information') }}</label>
                        </div>                                        
                    </div>                
                    <div id="detail">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <input class="form-control" id="detail_code" type="text" name="detail_code" readonly="readonly">
                            </div>
                            <div class="form-group col-md-4">
                                <input class="form-control" id="identification_number" type="text" name="identification_number">
                            </div>
                            <div class="form-group col-md-2">
                                <select id="condition" name="condition" class="form-control selectpicker show-tick" single title="{{ __('Select Condition') }}">
                                    <option value="1">Good</option>
                                    <option value="2">Lightly Damaged</option>
                                    <option value="3">Heavily Damaged</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <input class="form-control" id="description" type="text" name="description">
                            </div>                                        
                        </div>                    
                    </div>                    
                </form>                              
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="btnCancel" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        var template = Handlebars.compile($("#details-template").html());

        var table = $('#procurement-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '{{ route("procurement.getInventoryProcurementData") }}',
                data: {
                    procurement_start: function() { return $('#procurement_start').val() },
                    procurement_end: function() { return $('#procurement_end').val() },
                    filter_status: function() { return $('#filter_status option:selected').val() }
                } 
            },
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [
                {
                    "className"     : 'details-control',
                    "orderable"     : false,
                    "searchable"    : false,
                    "data"          : null,
                    "defaultContent": ''
                },                
                {data: 'procurement_number', name: 'procurement_number'},
                {data: 'procurement_date', name: 'procurement_date'},
                {data: 'decree_number', name: 'decree_number'},
                {data: 'reff_number', name: 'reff_number'},
                {data: 'origin', name: 'origin'},
                {data: 'total_item', name: 'total_item'},
                {data: 'total', name: 'total'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 2, "asc"]]
        });

        $('#procurement_start').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        }).on('changeDate', function (selected) {
            if ($('#procurement_end').val() == '' || $('#procurement_end').val() < $('#procurement_start').val()) {
                $('#procurement_end').val($('#procurement_start').val());
            }

            var minDate = new Date(selected.date.valueOf());
            $('#procurement_end').datepicker('setStartDate', minDate);
        });

        $('#procurement_end').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,               
        }).on('changeDate', function (selected) {
            if ($('#procurement_start').val() == '' || $('#procurement_start').val() > $('#procurement_end').val()) {
                $('#procurement_start').val($('#procurement_end').val());
            }
        });

        // Add event listener for opening and closing details
        $('#procurement-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'posts-' + row.data().id;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                ajax: data.details_url,
                columns: [
                    {data: 'item_code', name: 'item_code'},
                    {data: 'item_name', name: 'item_name'},                             
                    {data: 'qty', name: 'qty', class: 'text-center'},
                    {data: 'unit', name: 'unit', class: 'text-center'},
                    {data: 'view', name: 'view', class: 'text-center'}
                ]
            });

            $('#posts').DataTable().draw(false);
        }

        $('#procurement_start').on('change', function () {
            table.ajax.reload();
        });

        $('#procurement_end').on('change', function () {
            table.ajax.reload();
        });

        $('#filter_department').on('change', function () {
            table.ajax.reload();
        });

        $('#filter_status').on('change', function () {
            table.ajax.reload();
        });

        $(document).on('click', '.btn-view[data-remote]', function (e) { 
            e.preventDefault();


            console.log('test');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#detail div').remove();
            $('#itemDialogLabel').text('Show Item');
            $('#btnCancel').text('Close');

            //Populate Sub Detail Data
            var detail_id = $(this).data('remote');
            var myUrl = '{{ route("procurement.getSubDetailItem", ":id") }}';
            myUrl = myUrl.replace(':id', detail_id);

            $.ajax({
                url: myUrl,
                type: 'GET',
                dataType: 'json',
                success: function(data){
                    var component = '';
                    for (var i = 0; i < data.length; i++) {
                        var condition = '';
                        if (data[i].condition == '1') {
                            condition = '{{ __('Good') }}';
                        } else if (data[i].condition == '2') {
                            condition = '{{ __('Lightly Damaged') }}';
                        } else {
                            condition = '{{ __('Heavily Damaged') }}';
                        }

                        component += '<div class="row"><div class="form-group col-md-3"><input class="form-control" id="detail_code'+i+'" type="text" name="detail_code'+i+'" value="'+data[i].detail_code+'" readonly="readonly"></div><div class="form-group col-md-4"><input class="form-control" id="identification_number'+i+'" type="text" name="identification_number'+i+'" value="'+data[i].identification_number+'"></div><div class="form-group col-md-2"><select id="condition'+i+'" name="condition'+i+'" class="form-control selectpicker show-tick" single title="{{ __('Select Condition') }}"><option value="'+data[i].condition+'" selected>'+condition+'</option></select></div><div class="form-group col-md-3"><input class="form-control" id="description'+i+'" type="text" name="description'+i+'" value="'+data[i].information+'"></div></div>';
                    }

                    $('#detail').append(component);
                    $('.selectpicker').selectpicker('refresh');
                }
            });

            //Show
            $('.selectpicker').selectpicker('refresh');                    
            $('#itemDialog').modal('show');                     
        });                                  
    });     
</script>
<script id="details-template" type="text/x-handlebars-template">
    <h6>Detail : @{{procurement_number}}</h6>
    <table class="table details-table table-bordered datatable dataTable" id="posts-@{{id}}" style="width: 100%" width="100%">
        <thead>
            <tr>
                <th>
                    Item Code
                </th>                                        
                <th>
                    Item Name
                </th>                                       
                <th>
                    Qty
                </th>
                <th>
                    Unit
                </th>
                <th>
                    Action
                </th>                
            </tr>
        </thead>
    </table>
</script>
@endsection
