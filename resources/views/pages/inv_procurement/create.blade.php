@extends('layouts.app')

@section('title', __('Create Inventory Procurement'))
@section('meta_description', __('Inventory Procurement Page'))
@section('meta_keywords', __('Inventory Procurement'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('procurement.index') }}" alt="{{ __('Inventory Procurement') }}" title="{{ __('Inventory Procurement List') }}">
                    {{ __('Inventory Procurement') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Create') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Procurement') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Create New Inventory Procurement') }}
                                </div>
                            </div>
                        </div><hr>                        
						<form id="myForm" novalidate="novalidate" method="POST" action="{{ route('procurement.store') }}" enctype="multipart/form-data">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							{{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="procurement_date">{{ __('Request Date') }}</label>
                                    <div class="input-group">
                                        <input class="form-control no-border-right" id="procurement_date" type="text" name="procurement_date" placeholder="{{ __('Request Date') }}" value="{{ date('Y/m/d') }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-transparent">
                                                <i class="cil-calendar"></i>
                                            </span>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="decree_number">{{ __('Decree Number') }}</label>
                                    <input class="form-control" id="decree_number" type="text" name="decree_number" placeholder="{{ __('Decree Number') }}" value="{{ old('decree_number') }}">
                                </div>                                                                 
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="reff_number">{{ __('Reff Number') }}</label>
                                    <input class="form-control" id="reff_number" type="text" name="reff_number" placeholder="{{ __('Reff Number') }}" value="{{ old('reff_number') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="origin">{{ __('Origin') }}</label>
                                    <input class="form-control" id="origin" type="text" name="origin" placeholder="{{ __('Origin') }}" value="{{ old('origin') }}">
                                </div>                                                             
                            </div>                         
                            <div class="form-group">
                                <label class="col-form-label" for="information">{{ __('Information') }}</label>
                                <textarea class="form-control" id="information" name="information" placeholder="{{ __('Information') }}" rows="7">{{ old('information') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="procurement_file">{{ __('Procurement File') }}</label>
                                <input class="form-control" id="procurement_file" type="file" name="procurement_file" placeholder="{{ __('Procurement File') }}" value="{{ old('procurement_file') }}">
                            </div>                             		
							<hr>								
							<div class="form-group">
                                <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Save & Continue') }}" title="{{ __('Save & Continue') }}">
                                    <i class="cil-save"></i> {{ __('Save & Continue') }}
                                </button>
								<a href="{{ route('procurement.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
									<i class="icon-action-undo"></i> {{ __('Cancel') }}
								</a>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">
    $(function() {
        $('#procurement_date').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        });
    });
</script>
@endsection