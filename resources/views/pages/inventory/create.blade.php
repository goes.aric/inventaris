@extends('layouts.app')

@section('title', __('Create Inventory'))
@section('meta_description', __('Inventory Page'))
@section('meta_keywords', __('Inventory'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('inventory.index') }}" alt="{{ __('Inventory') }}" title="{{ __('Inventory List') }}">
                    {{ __('Inventory') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Create') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Create New Inventory') }}
                                </div>
                            </div>
                        </div><hr>                        
						<form id="myForm" novalidate="novalidate" method="POST" action="{{ route('inventory.store') }}">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							{{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="type">{{ __('Type') }}</label>
                                    <select id="type" name="type" class="form-control selectpicker show-tick" title="{{ __('Select Type') }}"single>
                                        <option value="1" {{ old('type') == '1' ? 'selected' : '' }}>{{ __('Office Supplies') }}</option>
                                        <option value="2" {{ old('type') == '2' ? 'selected' : '' }}>{{ __('Materials') }}</option>
                                        <option value="3" {{ old('type') == '3' ? 'selected' : '' }}>{{ __('Provisional Equity') }}</option>
                                    </select>
                                </div>                                                    
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="category">{{ __('Category') }}</label>
                                    <select id="category" name="category" class="form-control selectpicker show-tick" title="{{ __('Select Category') }}" data-live-search="true" single>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}" {{ old('category') == $item->id ? 'selected' : '' }}>{{ $item->category_name }}</option>
                                        @endforeach
                                    </select>
                                </div>                                                                  
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="item_code">{{ __('Item Code') }}</label>
                                    <input class="form-control" id="item_code" type="text" name="item_code" placeholder="{{ __('Item Code') }}" value="{{ old('item_code') }}">
                                </div>                                 
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="item_name">{{ __('Item Name') }}</label>
                                    <input class="form-control" id="item_name" type="text" name="item_name" placeholder="{{ __('Item Name') }}" value="{{ old('item_name') }}">
                                </div>                                
                            </div>                                                         
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="brand">{{ __('Brand') }}</label>
                                    <input class="form-control" id="brand" type="text" name="brand" placeholder="{{ __('Brand') }}" value="{{ old('brand') }}">
                                </div>                         
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="type_of_brand">{{ __('Type of Brand') }}</label>
                                    <input class="form-control" id="type_of_brand" type="text" name="type_of_brand" placeholder="{{ __('Type of Brand') }}" value="{{ old('type_of_brand') }}">
                                </div>                                
                            </div>
                            <div class="row">                        
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="materials">{{ __('Materials') }}</label>
                                    <input class="form-control" id="materials" type="text" name="materials" placeholder="{{ __('Materials') }}" value="{{ old('materials') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="size">{{ __('Size / Dimension') }}</label>
                                    <input class="form-control" id="size" type="text" name="size" placeholder="{{ __('Size / Dimension') }}" value="{{ old('size') }}">
                                </div>                                                                 
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label" for="unit">{{ __('Unit') }}</label>
                                    <input class="form-control" id="unit" type="text" name="unit" placeholder="{{ __('Unit') }}" value="{{ old('unit') }}">
                                </div>                                
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="specification">{{ __('Product Specification') }}</label>
                                <textarea class="form-control" id="specification" type="specification" name="specification" placeholder="{{ __('Product Specification') }}" rows="10">{{ old('specification') }}</textarea>
                            </div>                          
                            <div class="form-group">
                                <label class="col-form-label" for="information">{{ __('Additional Information') }}</label>
                                <textarea class="form-control" id="information" name="information" placeholder="{{ __('Additional Information') }}" rows="5">{{ old('information') }}</textarea>
                            </div>		
							<hr>								
							<div class="form-group">
                                <button class="btn btn-primary" type="submit" name="save" alt="{{ __('Save & Close') }}" title="{{ __('Save & Close') }}">
                                    <i class="cil-save"></i> {{ __('Save & Close') }}
                                </button>
								<a href="{{ route('inventory.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
									<i class="icon-action-undo"></i> {{ __('Cancel') }}
								</a>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection