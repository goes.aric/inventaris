@extends('layouts.app')

@section('title', __('Show Inventory'))
@section('meta_description', __('Inventory Page'))
@section('meta_keywords', __('Inventory'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('inventory.index') }}" alt="{{ __('Inventory') }}" title="{{ __('Inventory List') }}">
                    {{ __('Inventory') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Inventory') }}
                                </div>
                            </div>
                        </div><hr>                        
                        <div class="row">                        
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="category">{{ __('Category') }}</label>
                                <select id="category" name="category" class="form-control selectpicker show-tick" title="{{ __('Select Category') }}" data-live-search="true" single>
                                    @foreach ($category as $item)
                                        <option value="{{ $item->id }}" {{ $inventory->category_id == $item->id ? 'selected' : '' }}>{{ $item->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="item_code">{{ __('Item Code') }}</label>
                                <input class="form-control" id="item_code" type="text" name="item_code" placeholder="{{ __('Item Code') }}" value="{{ $inventory->item_code }}">
                            </div>                                                                  
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="item_name">{{ __('Item Name') }}</label>
                            <input class="form-control" id="item_name" type="text" name="item_name" placeholder="{{ __('Item Name') }}" value="{{ $inventory->item_name }}">
                        </div>                                                         
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="brand">{{ __('Brand') }}</label>
                                <input class="form-control" id="brand" type="text" name="brand" placeholder="{{ __('Brand') }}" value="{{ $inventory->brand }}">
                            </div>                         
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="type">{{ __('Type') }}</label>
                                <input class="form-control" id="type" type="text" name="type" placeholder="{{ __('Type') }}" value="{{ $inventory->type }}">
                            </div>                                
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="identification_number">{{ __('Identification Number') }}</label>
                                <input class="form-control" id="identification_number" type="text" name="identification_number" placeholder="{{ __('Identification Number') }}" value="{{ $inventory->identification_number }}">
                            </div>                         
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="materials">{{ __('Materials') }}</label>
                                <input class="form-control" id="materials" type="text" name="materials" placeholder="{{ __('Materials') }}" value="{{ $inventory->materials }}">
                            </div>                                
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="size">{{ __('Size / Dimension') }}</label>
                                <input class="form-control" id="size" type="text" name="size" placeholder="{{ __('Size / Dimension') }}" value="{{ $inventory->size }}">
                            </div>                         
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="unit">{{ __('Unit') }}</label>
                                <input class="form-control" id="unit" type="text" name="unit" placeholder="{{ __('Unit') }}" value="{{ $inventory->unit }}">
                            </div>                                
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="specification">{{ __('Specification') }}</label>
                            <textarea class="form-control" id="specification" type="specification" name="specification" placeholder="{{ __('Specification') }}" rows="5">{{ $inventory->specification }}</textarea>
                        </div>
                        <div class="row">                        
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="is_office_supplies">{{ __('Is Office Supplies ?') }}</label>
                                <select id="is_office_supplies" name="is_office_supplies" class="form-control selectpicker show-tick" title="{{ __('Select Status') }}" data-live-search="true" single>
                                    <option value="1" {{ $inventory->is_office_supplies == '1' ? 'selected' : '' }}>{{ __('Yes') }}</option>
                                    <option value="0" {{ $inventory->is_office_supplies == '0' ? 'selected' : '' }}>{{ __('No') }}</option>
                                </select>
                            </div>                                
                        </div>                            
                        <div class="form-group">
                            <label class="col-form-label" for="information">{{ __('Information') }}</label>
                            <textarea class="form-control" id="information" name="information" placeholder="{{ __('Information') }}" rows="5">{{ $inventory->information }}</textarea>
                        </div>     
                        <hr>                                
                        <div class="form-group">
                            <a href="{{ route('inventory.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
                                <i class="icon-action-undo"></i> {{ __('Return') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection