@extends('layouts.app')

@section('title', __('Inventory'))
@section('meta_description', __('Inventory Page'))
@section('meta_keywords', __('Inventory'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/highlight.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/handlebars.js') }}"></script>
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item active">
                {{ __('Inventory') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Inventory List') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <button alt="{{ __('Delete') }}" class="btn btn-danger float-right" data-target="#deleteDialog" data-toggle="modal" title="{{ __('Delete Selected Items') }}" type="button">
                                    <i class="icon-trash">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Delete') }}
                                    </span>
                                </button>
                                <a alt="{{ __('Create') }}" class="btn btn-primary float-right mr-2" href="{{ route('inventory.create') }}" title="{{ __('Create New Record') }}">
                                    <i class="icon-plus">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Create') }}
                                    </span>
                                </a>
                            </div>
                        </div><hr>                        
                        <div id="table" class="table-responsive">
                            <table cellspacing="0" class="table table-bordered datatable dataTable" id="inventory-table" style="width: 100%" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="4">
                                            <label class="col-form-label" for="filter_type">{{ __('Type') }} :</label>
                                            <select id="filter_type" name="filter_type" class="form-control selectpicker show-tick" title="{{ __('Select Type Filter') }}" single>
                                                <option value="%%" selected>{{ __('All Type') }}</option>
                                                <option data-divider="true"></option>
                                                <option value="1">{{ __('Office Supplies') }}</option>
                                                <option value="2">{{ __('Materials') }}</option>
                                                <option value="3">{{ __('Provisional Equity') }}</option>
                                            </select>                                             
                                        </th>
                                        <th colspan="3">
                                            <label class="col-form-label" for="filter_category">{{ __('Category') }} :</label>
                                            <select id="filter_category" name="filter_category" class="form-control selectpicker show-tick" title="{{ __('Select Category Filter') }}" data-live-search="true" single>
                                                <option value="%%" selected>{{ __('All Category') }}</option>
                                                <option data-divider="true"></option>
                                                @foreach ($category as $item)
                                                    <option value="{{ $item->id }}" {{ old('category') == $item->id ? 'selected' : '' }}>{{ $item->category_name }}</option>
                                                @endforeach
                                            </select>                                             
                                        </th>
                                        <th colspan="4"></th>
                                    </tr>                                    
                                    <tr>
                                        <th>
                                            <input id="check-all" name="check-all" type="checkbox">
                                            </input>
                                        </th>
                                        <th></th>
                                        <th>
                                            {{ __('Type') }}
                                        </th>                                         
                                        <th>
                                            {{ __('Category') }}
                                        </th>                                        
                                        <th>
                                            {{ __('Item Code') }}
                                        </th>
                                        <th>
                                            {{ __('Item Name') }}
                                        </th>
                                        <th>
                                            {{ __('Brand') }}
                                        </th>      
                                        <th>
                                            {{ __('Type of Brand') }}
                                        </th>
                                        <th>
                                            {{ __('Qty') }}
                                        </th>                                        
                                        <th>
                                            {{ __('Unit') }}
                                        </th>                                                                            
                                        <th class="text-center">
                                            {{ __('Action') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Modal Dialog Delete -->
<div class="modal fade" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="deleteDialogLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteDialogLabel">{{ __('Confirmation') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{ __('Are you sure want to delete selected data ?') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('No') }}</button>
                <button type="button" class="btn btn-primary" id="btnContinueDelete">{{ __('Yes') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Dialog Inventory Location -->
<div class="modal fade" id="locationDialog" tabindex="-1" role="dialog" aria-labelledby="locationDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="locationDialogLabel">{{ __('Inventory User Details') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5><b>{{ __('Delivery ID') }}</b></h5>
                <h5><label id="lblDeliveryID" name="lblDeliveryID"></label></h5>
                <h5 class="mt-3">{{ __('Department') }}</h5><hr>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label class="col-form-label" id="lblDepartmentCode" name="lblDepartmentCode"></label>
                    </div>
                    <div class="form-group col-md-8">
                        <label class="col-form-label" id="lblDepartmentName" name="lblDepartmentName"></label>
                    </div>                                       
                </div>
                <h5 class="mt-2">{{ __('Inventory User') }}</h5><hr>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="col-form-label">{{ __('Employee') }}</label>
                    </div>
                    <div class="form-group col-md-9">
                        <label class="col-form-label" id="lblEmployeeNumber" name="lblEmployeeNumber"></label>
                    </div>                                        
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="col-form-label">{{ __('Payroll Number') }}</label>
                    </div>
                    <div class="form-group col-md-9">
                        <label class="col-form-label" id="lblPayrollNumber" name="lblPayrollNumber"></label>
                    </div>                                        
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="col-form-label">{{ __('Group') }}</label>
                    </div>
                    <div class="form-group col-md-9">
                        <label class="col-form-label" id="lblGroup" name="lblGroup"></label>
                    </div>                                        
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="col-form-label">{{ __('Position') }}</label>
                    </div>
                    <div class="form-group col-md-9">
                        <label class="col-form-label" id="lblPosition" name="lblPosition"></label>
                    </div>                                        
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label class="col-form-label">{{ __('Education') }}</label>
                    </div>
                    <div class="form-group col-md-9">
                        <label class="col-form-label" id="lblLevel" name="lblLevel"></label>
                    </div>                                        
                </div>                                                                                                         
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Dialog Inventory Maintenance Records -->
<div class="modal fade" id="maintenanceDialog" tabindex="-1" role="dialog" aria-labelledby="maintenanceDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="maintenanceDialogLabel">{{ __('Inventory Maintenance Records') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table cellspacing="0" class="table table-bordered datatable dataTable" id="records-table" style="width: 100%" width="100%">
                        <thead>                                   
                            <tr>
                                <th>
                                    {{ __('Date') }}
                                </th>                                         
                                <th>
                                    {{ __('Reff Number') }}
                                </th>
                                <th>
                                    {{ __('Problem') }}
                                </th>                                                                        
                                <th>
                                    {{ __('Work Description') }}
                                </th>
                                <th>
                                    {{ __('Cost') }}
                                </th>
                                <th>
                                    {{ __('Details') }}
                                </th>                                                                
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>                                                                                                                        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Dialog Inventory Maintenance Details -->
<div class="modal fade" id="maintenanceDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="maintenanceDetailsDialogLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="maintenanceDetailsDialogLabel">{{ __('Inventory Maintenance Records') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h5>Work Details</h5>
                    </div>
                </div>
                <div class="table-responsive">
                    <table cellspacing="0" class="table table-bordered datatable dataTable" id="works-table" style="width: 100%" width="100%">
                        <thead>                                   
                            <tr>
                                <th>
                                    {{ __('Work Name') }}
                                </th>                                         
                                <th>
                                    {{ __('Description') }}
                                </th>
                                <th>
                                    {{ __('Technician') }}
                                </th>                                                                        
                                <th>
                                    {{ __('Cost') }}
                                </th>                                                               
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12 text-center">
                        <h5>Spare Parts Details</h5>
                    </div>
                </div>                
                <div class="table-responsive">
                    <table cellspacing="0" class="table table-bordered datatable dataTable" id="parts-table" style="width: 100%" width="100%">
                        <thead>                                   
                            <tr>
                                <th>
                                    {{ __('Part Name') }}
                                </th>                                         
                                <th>
                                    {{ __('Part Number') }}
                                </th>
                                <th>
                                    {{ __('Serial Number') }}
                                </th>                                                                        
                                <th>
                                    {{ __('Price') }}
                                </th>                                                              
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>                           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        var template = Handlebars.compile($("#details-template").html());

        var table = $('#inventory-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '{{ route("inventory.getInventoryData") }}',
                data: {
                    type: function() { return $('#filter_type option:selected').val() },
                    category: function() { return $('#filter_category option:selected').val() }
                } 
            },            
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [
                {data: 'checkbox_column', name: 'checkbox_column', orderable: false, searchable: false},
                {
                    "className"     : 'details-control',
                    "orderable"     : false,
                    "searchable"    : false,
                    "data"          : null,
                    "defaultContent": ''
                },                
                {data: 'type', name: 'type', searchable: false},
                {data: 'category_name', name: 'category.category_name'},
                {data: 'item_code', name: 'item_code'},
                {data: 'item_name', name: 'item_name'},
                {data: 'brand', name: 'brand'},
                {data: 'type_of_brand', name: 'type_of_brand'},
                {data: 'qty', name: 'qty', searchable: false},
                {data: 'unit', name: 'unit'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 2, "asc"]]
        });

        $('#inventory-table').on('click', '.btn-delete[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("inventory.destroy", ":id") }}';
            url = url.replace(':id', id);

            // confirm then
            if (confirm('{{ __('Are you sure want to delete it ?') }}')) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: 'json',
                    data: {method: '_DELETE', submit: true,},
                    success: function(data){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-top-center"
                        };                      
                        if (data.type == 'error') {
                            toastr.error(data.msg);
                        }else {
                            toastr.success(data.msg);
                        }
                    }
                }).always(function (data) {
                    $('#inventory-table').DataTable().draw(false);
                });
            }else{
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.info('{{ __('You have canceled it!') }}');
            }
        }); 

        $('#check-all').on('click', function() {
            if (this.checked) {
                $(':checkbox').each(function() {
                    this.checked = true;
                });
            }else{
                $(':checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

        $('#btnContinueDelete').on('click', function (e) { 
            e.preventDefault();
            $('#deleteDialog').modal('hide');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url = '{{ route("inventory.delete") }}';

            var ckbox = $("input[name='data[]']");
            var ids = [];
            if (ckbox.is(':checked')) {
                $("input[name='data[]']:checked").each(function (i) {
                    ids[i] = $(this).val();
                });
            }else{          
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center"
                };                      
                toastr.warning('{{ __('Please select the record you want to delete!') }}');
            }       

            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                data: {
                    data: ids,
                },
                success: function(data){
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-center"
                    };                      
                    if (data.type == 'error') {
                        toastr.error(data.msg);
                    }else {
                        toastr.success(data.msg);
                    }
                }
            }).always(function (data) {
                $('#inventory-table').DataTable().draw(false);
            });
        });

        $('#filter_type').on('change', function () {
            table.ajax.reload();
        });

        $('#filter_category').on('change', function () {
            table.ajax.reload();
        });

        // Add event listener for opening and closing details
        $('#inventory-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'posts-' + row.data().id;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                ajax: data.details_url,
                columns: [
                    {data: 'detail_code', name: 'detail_code'},
                    {data: 'item_name', name: 'item_name'},
                    {data: 'identification_number', name: 'identification_number'},                             
                    {data: 'decree_number', name: 'decree_number'},
                    {data: 'origin', name: 'origin'},
                    {data: 'acquisition_year', name: 'acquisition_year'},
                    {data: 'condition', name: 'condition', searchable: false, class: 'text-center'},
                    {data: 'status', name: 'status', searchable: false, class: 'text-center'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
                ]
            });

            $('#posts').DataTable().draw(false);
        }

        $(document).on('click', '.btn-show[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).data('remote');
            var url = '{{ route("inventory.inventoryUserDetails", ":id") }}';
            url = url.replace(':id', id);

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(data){
                    var date = new Date(data.delivery_date);
                    var tanggal = date.toLocaleDateString('fr-ca');

                    $('#lblDeliveryID').text(data.delivery_number+' | '+tanggal);
                    $('#lblDepartmentCode').text(data.department.code);
                    $('#lblDepartmentName').text(data.department.department_name);
                    $('#lblEmployeeNumber').text(': '+data.employee.employee_number+' - '+data.employee.employee_name);
                    $('#lblPayrollNumber').text(': '+data.employee.payroll_number);
                    $('#lblGroup').text(': '+data.employee.group);
                    $('#lblPosition').text(': '+data.employee.position);
                    $('#lblLevel').text(': '+data.employee.level_of_education+' - '+data.employee.majoring_in_education);

                    $('#locationDialog').modal('show');
                }
            });
        });

        $(document).on('click', '.btn-record[data-remote]', function (e) { 
            e.preventDefault();

            var id = $(this).data('remote');
            var maintenanceUrl = '{{ route("inventory.maintenanceRecords", ":id") }}';
            maintenanceUrl = maintenanceUrl.replace(':id', id);

            $('#records-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: maintenanceUrl,
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
                oLanguage: {
                    "sEmptyTable":     "{{ __('No data available in table') }}",
                    "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                    "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                    "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ",",
                    "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                    "sLoadingRecords": "{{ __('Loading') }} ...",
                    "sProcessing":     "{{ __('Processing') }} ...",
                    "sSearch":         "{{ __('Search') }} : ",
                    "sZeroRecords":    "{{ __('No matching records found') }}",                  
                    "oPaginate": {
                        "sFirst":    "<i class='cil-media-step-backward'></i>",
                        "sLast":     "<i class='cil-media-step-forward'></i>",
                        "sNext":     "<i class='cil-media-skip-forward'></i>",
                        "sPrevious": " <i class='cil-media-skip-backward'></i>"
                    }
                },            
                pagingType: "full_numbers",
                paginate: {
                    "previous": "Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First",
                    "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                    "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
                },                  
                columns: [               
                    {data: 'date', name: 'date'},
                    {data: 'reff_number', name: 'reff_number'},
                    {data: 'problem', name: 'problem'},
                    {data: 'work_description', name: 'work_description'},
                    {data: 'cost', name: 'cost', searchable: false, class: 'text-center'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
                ],
                order: [[ 1, "asc"]]
            });            

            $('#maintenanceDialog').modal('show');
        });

        $(document).on('click', '.btn-maintenance-detail[data-remote]', function (e) { 
            e.preventDefault();

            var id = $(this).data('remote');
            var workUrl = '{{ route("inventory.maintenanceWorkRecords", ":id") }}';
            var partUrl = '{{ route("inventory.maintenancePartRecords", ":id") }}';
            workUrl = workUrl.replace(':id', id);
            partUrl = partUrl.replace(':id', id);

            $('#works-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: workUrl,
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
                oLanguage: {
                    "sEmptyTable":     "{{ __('No data available in table') }}",
                    "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                    "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                    "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ",",
                    "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                    "sLoadingRecords": "{{ __('Loading') }} ...",
                    "sProcessing":     "{{ __('Processing') }} ...",
                    "sSearch":         "{{ __('Search') }} : ",
                    "sZeroRecords":    "{{ __('No matching records found') }}",                  
                    "oPaginate": {
                        "sFirst":    "<i class='cil-media-step-backward'></i>",
                        "sLast":     "<i class='cil-media-step-forward'></i>",
                        "sNext":     "<i class='cil-media-skip-forward'></i>",
                        "sPrevious": " <i class='cil-media-skip-backward'></i>"
                    }
                },            
                pagingType: "full_numbers",
                paginate: {
                    "previous": "Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First",
                    "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                    "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
                },                  
                columns: [               
                    {data: 'work_name', name: 'work_name'},
                    {data: 'work_description', name: 'work_description'},
                    {data: 'technician', name: 'technician'},
                    {data: 'price', name: 'price', searchable: false, class: 'text-center'}
                ],
                order: [[ 1, "asc"]]
            });

            $('#parts-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: partUrl,
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
                oLanguage: {
                    "sEmptyTable":     "{{ __('No data available in table') }}",
                    "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                    "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                    "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ",",
                    "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                    "sLoadingRecords": "{{ __('Loading') }} ...",
                    "sProcessing":     "{{ __('Processing') }} ...",
                    "sSearch":         "{{ __('Search') }} : ",
                    "sZeroRecords":    "{{ __('No matching records found') }}",                  
                    "oPaginate": {
                        "sFirst":    "<i class='cil-media-step-backward'></i>",
                        "sLast":     "<i class='cil-media-step-forward'></i>",
                        "sNext":     "<i class='cil-media-skip-forward'></i>",
                        "sPrevious": " <i class='cil-media-skip-backward'></i>"
                    }
                },            
                pagingType: "full_numbers",
                paginate: {
                    "previous": "Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First",
                    "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                    "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
                },                  
                columns: [               
                    {data: 'part_name', name: 'part_name'},
                    {data: 'part_number', name: 'part_number'},
                    {data: 'serial_number', name: 'serial_number'},
                    {data: 'price', name: 'price', searchable: false, class: 'text-center'}
                ],
                order: [[ 1, "asc"]]
            });                        

            $('#maintenanceDetailsDialog').modal('show');
        });                             
    });     
</script>
<script id="details-template" type="text/x-handlebars-template">
    <h6>Detail : @{{item_code}} - @{{item_name}}</h6>
    <div class="table-responsive">
        <table class="table details-table table-bordered datatable dataTable" id="posts-@{{id}}" style="width: 100%" width="100%">
            <thead>
                <tr>
                    <th>
                        Detail Code
                    </th>                                                        
                    <th>
                        Item Name
                    </th>
                    <th>
                        Identification Number
                    </th>
                    <th>
                        Decree Number
                    </th>                                                                                      
                    <th>
                        Origin
                    </th>
                    <th>
                        Year
                    </th>
                    <th>
                        Condition
                    </th>                    
                    <th>
                        Status
                    </th>
                    <th>
                        Action
                    </th>                                
                </tr>
            </thead>
        </table>
    </div>
</script>
@endsection
