<script type="text/javascript" src="{{ asset('bower_components/toastr/toastr.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/toastr/toastr.min.css') }}"/>
<script type="text/javascript">
	@if (Session::has('msg'))
		var type = "{{ Session::get('alert-type', 'info') }}"
		switch(type){
			case 'info':
				toastr.info("{{ Session::get('msg') }}", toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-center",
					"preventDuplicate": false,
					"onclick": false,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeout": "5000",
					"extendedTimeOut": "1000",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				});
				break;			

			case 'success':
				toastr.success("{{ Session::get('msg') }}", toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-center",
					"preventDuplicate": false,
					"onclick": false,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeout": "5000",
					"extendedTimeOut": "1000",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				});			
				break;

			case 'warning':
				toastr.warning("{{ Session::get('msg') }}", toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-center",
					"preventDuplicate": false,
					"onclick": false,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeout": "5000",
					"extendedTimeOut": "1000",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				});			
				break;

			case 'error':
				toastr.error("{{ Session::get('msg') }}", toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-center",
					"preventDuplicate": false,
					"onclick": false,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeout": "5000",
					"extendedTimeOut": "1000",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				});			
				break;
		}
	@endif
</script>