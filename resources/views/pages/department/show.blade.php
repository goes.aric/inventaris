@extends('layouts.app')

@section('title', __('Show Department'))
@section('meta_description', __('Department Page'))
@section('meta_keywords', __('Department'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('department.index') }}" alt="{{ __('Department') }}" title="{{ __('Department List') }}">
                    {{ __('Department') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Show') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Department') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Show Department') }}
                                </div>
                            </div>
                        </div><hr>                        
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="code">{{ __('Code') }}</label>
                                <input class="form-control" id="code" type="text" name="code" placeholder="{{ __('Code') }}" value="{{ $department->code }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-form-label" for="department_name">{{ __('Department Name') }}</label>
                                <input class="form-control" id="department_name" type="text" name="department_name" placeholder="{{ __('Department Name') }}" value="{{ $department->department_name }}">
                            </div>
                        </div>                            
                        <div class="form-group">
                            <label class="col-form-label" for="description">{{ __('Description') }}</label>
                            <textarea class="form-control" id="description" name="description" placeholder="{{ __('Description') }}" rows="5">{{ $department->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="status">{{ __('Status') }}</label>
                            <div class="form-check">
                                <input class="form-check-input" id="active" type="radio" value="1" name="status" {{ $department->status == '1' ? 'checked' : '' }}>
                                <label class="form-check-label" for="active">{{ __('Active') }}</label>
                            </div>                                  
                            <div class="form-check">
                                <input class="form-check-input" id="inactive" type="radio" value="0" name="status" {{ $department->status == '0' ? 'checked' : '' }}>
                                <label class="form-check-label" for="inactive">{{ __('Inactive') }}</label>
                            </div>  
                        </div>                                                                                                              
                        <hr>                                
                        <div class="form-group">
                            <a href="{{ route('department.index') }}" class="btn btn-light" alt="{{ __('Return') }}" title="{{ __('Return to List') }}">
                                <i class="icon-action-undo"></i> {{ __('Return') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection