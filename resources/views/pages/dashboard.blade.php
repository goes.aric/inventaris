@extends('layouts.app')

@section('title', __('Dashboard'))
@section('meta_description', __('Dashboard Page'))
@section('meta_keywords', __('Dashboard'))
@section('copyright', 'Copyright © 2020 ArikBali & Fastech')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                {{ __('Home') }}
            </li>
            <li class="breadcrumb-item active">
                {{ __('Dashboard') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="card text-white bg-gradient-primary">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg">
                                        9.823
                                    </div>
                                    <div>
                                        Members online
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-transparent dropdown-toggle p-0" data-toggle="dropdown" type="button">
                                        <svg class="c-icon">
                                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-settings">
                                            </use>
                                        </svg>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">
                                            Action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Another action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Something else here
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class="">
                                        </div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div class="">
                                        </div>
                                    </div>
                                </div>
                                <canvas class="chart chartjs-render-monitor" height="127" id="card-chart1" style="display: block; height: 70px; width: 261px;" width="474">
                                </canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card text-white bg-gradient-info">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg">
                                        9.823
                                    </div>
                                    <div>
                                        Members online
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-transparent dropdown-toggle p-0" data-toggle="dropdown" type="button">
                                        <svg class="c-icon">
                                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-settings">
                                            </use>
                                        </svg>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">
                                            Action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Another action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Something else here
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class="">
                                        </div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div class="">
                                        </div>
                                    </div>
                                </div>
                                <canvas class="chart chartjs-render-monitor" height="127" id="card-chart2" style="display: block; height: 70px; width: 261px;" width="474">
                                </canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card text-white bg-gradient-warning">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg">
                                        9.823
                                    </div>
                                    <div>
                                        Members online
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-transparent dropdown-toggle p-0" data-toggle="dropdown" type="button">
                                        <svg class="c-icon">
                                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-settings">
                                            </use>
                                        </svg>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">
                                            Action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Another action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Something else here
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3" style="height:70px;">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class="">
                                        </div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div class="">
                                        </div>
                                    </div>
                                </div>
                                <canvas class="chart chartjs-render-monitor" height="127" id="card-chart3" style="display: block; height: 70px; width: 293px;" width="532">
                                </canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card text-white bg-gradient-danger">
                            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg">
                                        9.823
                                    </div>
                                    <div>
                                        Members online
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-transparent dropdown-toggle p-0" data-toggle="dropdown" type="button">
                                        <svg class="c-icon">
                                            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-settings">
                                            </use>
                                        </svg>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">
                                            Action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Another action
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Something else here
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class="">
                                        </div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div class="">
                                        </div>
                                    </div>
                                </div>
                                <canvas class="chart chartjs-render-monitor" height="127" id="card-chart4" style="display: block; height: 70px; width: 261px;" width="474">
                                </canvas>
                                <div class="c-chartjs-tooltip top" id="card-chart4-tooltip" style="opacity: 0; left: 138.344px; top: 99.5px;">
                                    <div class="c-tooltip-header">
                                        <div class="c-tooltip-header-item">
                                            August
                                        </div>
                                    </div>
                                    <div class="c-tooltip-body">
                                        <div class="c-tooltip-body-item">
                                            <span class="c-tooltip-body-item-color" style="background-color: rgba(255, 255, 255, 0.2);">
                                            </span>
                                            <span class="c-tooltip-body-item-label">
                                                My First dataset
                                            </span>
                                            <span class="c-tooltip-body-item-value">
                                                85
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 class="card-title mb-0">
                                    Traffic
                                </h4>
                                <div class="small text-muted">
                                    September 2019
                                </div>
                            </div>
                            <div aria-label="Toolbar with buttons" class="btn-toolbar d-none d-md-block" role="toolbar">
                                <div class="btn-group btn-group-toggle mx-3" data-toggle="buttons">
                                    <label class="btn btn-outline-secondary">
                                        <input autocomplete="off" id="option1" name="options" type="radio">
                                            Day
                                        </input>
                                    </label>
                                    <label class="btn btn-outline-secondary active">
                                        <input autocomplete="off" checked="" id="option2" name="options" type="radio">
                                            Month
                                        </input>
                                    </label>
                                    <label class="btn btn-outline-secondary">
                                        <input autocomplete="off" id="option3" name="options" type="radio">
                                            Year
                                        </input>
                                    </label>
                                </div>
                                <button class="btn btn-primary" type="button">
                                    <svg class="c-icon">
                                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-cloud-download">
                                        </use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="c-chart-wrapper" style="height:300px;margin-top:40px;">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class="">
                                    </div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row text-center">
                            <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <div class="text-muted">
                                    Visits
                                </div>
                                <strong>
                                    29.703 Users (40%)
                                </strong>
                                <div class="progress progress-xs mt-2">
                                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" class="progress-bar bg-gradient-success" role="progressbar" style="width: 40%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <div class="text-muted">
                                    Unique
                                </div>
                                <strong>
                                    24.093 Users (20%)
                                </strong>
                                <div class="progress progress-xs mt-2">
                                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" class="progress-bar bg-gradient-info" role="progressbar" style="width: 20%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <div class="text-muted">
                                    Pageviews
                                </div>
                                <strong>
                                    78.706 Views (60%)
                                </strong>
                                <div class="progress progress-xs mt-2">
                                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" class="progress-bar bg-gradient-warning" role="progressbar" style="width: 60%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <div class="text-muted">
                                    New Users
                                </div>
                                <strong>
                                    22.123 Users (80%)
                                </strong>
                                <div class="progress progress-xs mt-2">
                                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" class="progress-bar bg-gradient-danger" role="progressbar" style="width: 80%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md mb-sm-2 mb-0">
                                <div class="text-muted">
                                    Bounce Rate
                                </div>
                                <strong>
                                    40.15%
                                </strong>
                                <div class="progress progress-xs mt-2">
                                    <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" class="progress-bar" role="progressbar" style="width: 40%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
