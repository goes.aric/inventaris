@extends('layouts.app')

@section('title', __('Inventory Return'))
@section('meta_description', __('Inventory Return Page'))
@section('meta_keywords', __('Inventory Return'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/highlight.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/handlebars.js') }}"></script>
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item active">
                {{ __('Inventory Return') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                <h4 class="card-title mb-0">
                                    {{ __('Inventory Return') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Inventory Return List') }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <a alt="{{ __('Create') }}" class="btn btn-primary float-right" href="{{ route('return.create') }}" title="{{ __('Create New Record') }}">
                                    <i class="icon-plus">
                                    </i>
                                    <span class="d-none d-sm-inline">
                                        {{ __('Create') }}
                                    </span>
                                </a>
                            </div>
                        </div><hr>                        
                        <div id="table" class="table-responsive">
                            <table cellspacing="0" class="table table-bordered datatable dataTable" id="return-table" style="width: 100%" width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="3">
                                            <label class="col-form-label" for="filter_date">{{ __('Return Date') }}</label>
                                            <div class="input-group request" id="request">
                                                <input type="text" class="form-control" id="return_start" name="return_start">
                                                <div class="input-group-addon mr-1 ml-1 col-form-label">{{ __('to') }}</div>
                                                <input type="text" class="form-control" id="return_end" name="return_end">
                                            </div>                                             
                                        </th>                                       
                                        <th colspan="3">
                                            <label class="col-form-label" for="filter_department">{{ __('Department') }}</label>
                                            <select id="filter_department" name="filter_department" class="form-control selectpicker show-tick" title="{{ __('Select Department Filter') }}" data-live-search="true" single>
                                                <option value="%%" selected>{{ __('All Department') }}</option>
                                                <option data-divider="true"></option>
                                                @foreach ($department as $item)
                                                    <option value="{{ $item->id }}" {{ old('filter_department') == $item->id ? 'selected' : '' }}>{{ $item->department_name }}</option>
                                                @endforeach
                                            </select>                                             
                                        </th>
                                        <th colspan="2">
                                            <label class="col-form-label" for="filter_status">{{ __('Status') }}</label>
                                            <select id="filter_status" name="filter_status" class="form-control selectpicker show-tick" title="{{ __('Select Status Filter') }}" single>
                                                <option value="%%" selected>{{ __('All Status') }}</option>
                                                <option data-divider="true"></option>
                                                <option value="1">{{ __('Pending') }}</option>
                                                <option value="2">{{ __('Valid') }}</option>
                                            </select>                                             
                                        </th>
                                    </tr>                                    
                                    <tr>
                                        <th></th>
                                        <th>
                                            {{ __('Return Number') }}
                                        </th>                                         
                                        <th>
                                            {{ __('Return Date') }}
                                        </th>                                        
                                        <th>
                                            {{ __('Department') }}
                                        </th>
                                        <th>
                                            {{ __('Received By') }}
                                        </th>
                                        <th>
                                            {{ __('Total Item') }}
                                        </th>                                                                                      
                                        <th>
                                            {{ __('Status') }}
                                        </th>                                                                            
                                        <th class="text-center">
                                            {{ __('Action') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">
    $(function() {
        var template = Handlebars.compile($("#details-template").html());

        var table = $('#return-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: '{{ route("return.getInventoryReturnData") }}',
                data: {
                    return_start: function() { return $('#return_start').val() },
                    return_end: function() { return $('#return_end').val() },
                    filter_department: function() { return $('#filter_department option:selected').val() },
                    filter_status: function() { return $('#filter_status option:selected').val() }
                } 
            },
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ __('All') }}"]],
            oLanguage: {
                "sEmptyTable":     "{{ __('No data available in table') }}",
                "sInfo":           "{{ __('Showing') }} _START_ {{ __('to') }} _END_ {{ __('of') }} _TOTAL_ {{ __('entries') }}",
                "sInfoEmpty":      "{{ __('Showing') }} 0 {{ __('to') }} 0 {{ __('of') }} 0 {{ __('entries') }}",
                "sInfoFiltered":   "( {{ __('filtered from') }} _MAX_ {{ __('total entries') }} )",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "{{ __('Show') }} _MENU_ {{ __('entries')}}",
                "sLoadingRecords": "{{ __('Loading') }} ...",
                "sProcessing":     "{{ __('Processing') }} ...",
                "sSearch":         "{{ __('Search') }} : ",
                "sZeroRecords":    "{{ __('No matching records found') }}",                  
                "oPaginate": {
                    "sFirst":    "<i class='cil-media-step-backward'></i>",
                    "sLast":     "<i class='cil-media-step-forward'></i>",
                    "sNext":     "<i class='cil-media-skip-forward'></i>",
                    "sPrevious": " <i class='cil-media-skip-backward'></i>"
                }
            },            
            pagingType: "full_numbers",
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First",
                "page": "<span class=' '><i class='fa fa-eye'></i> &nbsp;Page&nbsp;&nbsp;</span>",
                "pageOf": "<span class=' '>&nbsp;of&nbsp;</span>"
            },                  
            columns: [
                {
                    "className"     : 'details-control',
                    "orderable"     : false,
                    "searchable"    : false,
                    "data"          : null,
                    "defaultContent": ''
                },                
                {data: 'return_number', name: 'return_number'},
                {data: 'return_date', name: 'return_date'},
                {data: 'department_name', name: 'department.department_name'},
                {data: 'employee_name', name: 'employee.employee_name'},
                {data: 'total_item', name: 'total_item'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class: 'text-center'}
            ],
            order: [[ 2, "asc"]]
        });

        $('#return_start').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,              
        }).on('changeDate', function (selected) {
            if ($('#return_end').val() == '' || $('#return_end').val() < $('#return_start').val()) {
                $('#return_end').val($('#return_start').val());
            }

            var minDate = new Date(selected.date.valueOf());
            $('#return_end').datepicker('setStartDate', minDate);
        });

        $('#return_end').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            clearBtn: true,
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,               
        }).on('changeDate', function (selected) {
            if ($('#return_start').val() == '' || $('#return_start').val() > $('#return_end').val()) {
                $('#return_start').val($('#return_end').val());
            }
        });

        // Add event listener for opening and closing details
        $('#return-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var tableId = 'posts-' + row.data().id;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                ajax: data.details_url,
                columns: [
                    {data: 'item_code', name: 'item_code'},
                    {data: 'detail_code', name: 'detail_code'},
                    {data: 'item_name', name: 'item_name'},
                    {data: 'identification_number', name: 'identification_number'},                             
                    {data: 'qty', name: 'qty', class: 'text-center'},
                    {data: 'unit', name: 'unit', class: 'text-center'}
                ]
            });

            $('#posts').DataTable().draw(false);
        }

        $('#return_start').on('change', function () {
            table.ajax.reload();
        });

        $('#return_end').on('change', function () {
            table.ajax.reload();
        });

        $('#filter_department').on('change', function () {
            table.ajax.reload();
        });

        $('#filter_status').on('change', function () {
            table.ajax.reload();
        });                          
    });     
</script>
<script id="details-template" type="text/x-handlebars-template">
    <h6>Detail : @{{return_number}}</h6>
    <table class="table details-table table-bordered datatable dataTable" id="posts-@{{id}}" style="width: 100%" width="100%">
        <thead>
            <tr>
                <th>
                    Item Code
                </th>
                <th>
                    Detail Code
                </th>                                                        
                <th>
                    Item Name
                </th>
                <th>
                    Identification Number
                </th>                                                                      
                <th>
                    Qty
                </th>
                <th>
                    Unit
                </th>
            </tr>
        </thead>
    </table>
</script>
@endsection
