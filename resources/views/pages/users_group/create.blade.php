@extends('layouts.app')

@section('title', __('Create Users Group'))
@section('meta_description', __('Users Group Page'))
@section('meta_keywords', __('Users Group'))
@section('copyright', 'Copyright © 2020 ArikBali')
@section('author', 'Arik Bali')

@section('content')
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    @include('partials/navbar_menus')
    <div class="c-subheader justify-content-between px-3">
        <ol class="breadcrumb border-0 m-0 px-0 px-md-3">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}" alt="{{ __('Dashboard') }}" title="{{ __('Dashboard') }}">
                    {{ __('Home') }}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('users-group.index') }}" alt="{{ __('Users Group') }}" title="{{ __('Users Group List') }}">
                    {{ __('Users Group') }}
                </a>
            </li>            
            <li class="breadcrumb-item active">
                {{ __('Create') }}
            </li>
        </ol>
    </div>
</header>
<div class="c-body">
    <main class="c-main">
        <div class="container-fluid">
            <div id="ui-view">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h4 class="card-title mb-0">
                                    {{ __('Users Group') }}
                                </h4>
                                <div class="small text-muted">
                                    {{ __('Create New User Group') }}
                                </div>
                            </div>
                        </div><hr>                        
						<form id="myForm" novalidate="novalidate" method="POST" action="{{ route('users-group.store') }}">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							{{ csrf_field() }}	
							<div class="form-group">
								<label class="col-form-label" for="group_name">{{ __('Users Group') }}</label>
								<input class="form-control" id="group_name" type="text" name="group_name" placeholder="{{ __('Users Group') }}" value="{{ old('group_name') }}">
							</div>
							<div class="form-group">
								<label class="col-form-label" for="group_description">{{ __('Description') }}</label>
								<textarea class="form-control" id="group_description" type="group_description" name="group_description" placeholder="{{ __('User Group Description') }}">{{ old('group_description') }}</textarea>
							</div>						
							<h6 class="mt-5">{{ __('User Rights') }}</h6><hr>
							<!-- Hak Akses Disini -->
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								@foreach ($data['data'] as $menu_item)
									@if ($loop->index != 0)
										<hr>
									@endif
									<label><b>{{ $menu_item['groupName'] }}</b></label><br>
									<label>{{ $menu_item['description'] }}</label><br><br>
									<div class="row">
										@foreach ($menu_item['items'] as $item)			
											<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
												<div class="form-check checkbox">
													<input type="checkbox" class="form-check-input" name="permissions[]" value="{{ $item['itemKey'] }}" id="{{ $item['groupKey'] }}[{{ $item['itemKey'] }}]">
													<label class="form-check-label" for="{{ $item['groupKey'] }}[{{ $item['itemKey'] }}]"> {{ $item['itemName'] }}</label>
												</div>
											</div> 
										@endforeach             
									</div> 
								@endforeach                   
							</div><hr>																
							<div class="form-group">
								<button class="btn btn-primary" type="submit" name="save" alt="{{ __('Save & Close') }}" title="{{ __('Save & Close') }}">
									<i class="cil-save"></i> {{ __('Save & Close') }}
								</button>
								<a href="{{ route('users-group.index') }}" class="btn btn-light" alt="{{ __('Cancel') }}" title="{{ __('Cancel') }}">
									<i class="icon-action-undo"></i> {{ __('Cancel') }}
								</a>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection